# README #

### What is this repository for? ###
The general autograder makes it possible to create tests that will run on multiple files, determining if the code meets a variety of conditions, from declaration to implementation and output.

### How do I get set up? ###

* Clone, fork or download the project
* Place files you want to test in /src/java\_files\_to\_test
* Place any support files needed for test in /src/java\_files\_needed\_for\_test
* Define tests in /test\_defs
* Place rosters in /rosters (optional)
* Run /src/AutoTesterDriver.java
* Choose the folder where you want result files stored
* Choose a period (control click to add more periods or to deselect periods)
* Choose a tests to run.
* Your results will be saved in the folder you chose.
    * <YourTestName>fullReportsTabDelimited.txt  (a tab delimited full report - can be pasted into a spreadsheet)
    * <YourTestName>Summary.txt (A summary of the results)
    * <YourTestName>SummaryTabDelimited.txt (A tab delimited summary report, useful for just pasting scores into a spreadsheet)
    * A file for each class tested containing the full report for that class.
       
### General structure ###
You can define multiple tests in a single file to test different parts of an assignment. Each test can have any number of test cases.

Skeleton:

```json
    {
	    "tests":[
		    {
			    "testName":"Test 1",
			    "testCases": [
				    {
					    // define properties for test case 1
				    },
				    {
					    // define properties for test case 2
				    }
			    ]
		    },
		    {
			    "testName":"Test 2",
			    "testCases": [
				    {
					    // define properties for test case 1
				    },
				    {
					    // define properties for case 2
				    }
			    ]
		    }
	    ]
    }
```

### Properties of Tests ###
Tests have the following properties that can be set:

* **testName:** The name of the test (Reports will title results using this name)
* **testCases:** The list of test cases
* **points: (optional)** The total number of points this test is worth. If this is set, then the points property for test cases will be ignored as the points will be equally distributed among the test cases so the total is this number.

### Properties By Test Type ###
Below are the properties that can be used to define test cases. Most properties have reasonable defaults, but depending on the test case, some will be required. For example, it would not make sense to define a ReturnValueTestCase without defining which method should be called.

#### Common Properties ####
All test types can have these properties.

* **testCaseClass:** (required) which type of test is this? See below for the possible types of tests.
* **comment:** (Default: null) Use this field to write comments or a description of what is being tested (optional)
* **points**: (Default: 1) The number of points the test is worth. Will be ignored if the test containing this test case has a points property defined.
* **methodName:** (Default: null) The name of the method being tested (required for some types of tests)
* **parameterTypes:** (Default: []) Parameter types of the method being tested
* **returnType:** (Default: null) The return type of the method being tested (Only required for tests that check the return value or return type)
* **parameters:** (Default: []) The parameters to pass to the method being tested
* **constructorParameters:** (Default: []) The parameters to pass to the constructor when instantiating the class being tested
* **constructorParameterTypes:** (Default: []) The parameter types for the constructor to use when instantiating the class being tested
* **inputs:** (Default: null) the input buffer to populate System.in with (as a String)
* **timeout:** (Default: 200 for all cases except ManualTestCase, for which it is Long.MAX\_VALUE (effectively no timeout)) The number of milliseconds to wait for the method to finish executing before timing out. (For most tests the default should be long enough that only infinite loops will time out)

#### ClassImplementsInterfaceTestCase ####
This class is passed if the class being tested implements the expected interface.

* **expectedInterface:** The name of the expected interface (required)
* **alternativeInterfaces:** (default null) an array of alternate interface declarations (i.e. Comparable vs Comparable<(this)>)

#### ConstructorSignatureTestCase ####
This test is passed if the class being tested has a constructor with the correct signature.

* **expectedConstructorModifiers:** (DEFAULT ["public"]) Constructor must have these access modifiers to pass the test (optional)
* **forbiddenConstructorModifiers:** (DEFAULT []) Constructor fails the test if it has any of these access modifiers (optional)

#### ExceptionThrownTestCase ####
This test is passed if the class being tested throws the expected exception when running the test case

* **expectedException:** The class name of the exception that should be thrown (required)
	
#### ExceptionDeclaredThrownTestCase ####
This test is passed if the method being tested declares explicitly that it throws the expected exception

* **expectedException:** The class name of the exception that is declared to be thrown (required)

#### FieldTestCase ####
This test is passed if the class being tested has the expected field name with the expected field modifiers and expected field type.

* **expectedFieldName:** The name of the expected field (required)
* **expectedFieldModifiers:** (DEFAULT []) Field must have these access modifiers to pass the test (optional)
* **forbiddenFieldModifiers:** (DEFAULT []) Test fails if the field has any of these modifiers (optional)
* **expectedFieldType:** (DEFAULT null) The expected type of the field

#### FieldValuesChangedTestCase ####
This test is passed if all the fields listed in fieldsChanged have the values listed in expectedFieldValues after method call.

* **fieldsChanged:** The fields that should change (Required)
* **fieldTypes:** the types of the fields that should change (required)
* **expectedFieldValues:** The expected values after method call of the fields being tested as listed in the fieldsChanged parameter (Required)

#### ManualTestCase ####
This test case automatically passes because it is expected that the user will be checking the result manually. To make it easier to check all the test cases, the user will be prompted to press enter after each test case.

#### MethodNameTestCase ####
This test passes if the class has the method name given in the test case.

#### MethodSignatureTestCase ####
This test passes if the class has the method name and parameter types given in the test case.

* **expectedMethodModifiers:** (DEFAULT ["public"]) Method must have these access modifiers to pass the test (optional)
* **forbiddenMethodModifiers:** (DEFAULT []) Method fails the test if it has any of these access modifiers (optional)

#### OutputTestCase ####
This test passes if calling the method results in the expected output being printed using System.out

* **expectedOutput:** The expected output (required)
* **removeCarriageReturn** (Default true) Removes carriage returns from the expected and actual output.  True by default because the expected output will usually just use \n for new lines, but PrintStream will use \r\n (optional)
* **removeTrailingWhiteSpace:** (Default false) Removes the trailing white space from the output and expected output. If this is set to true, you should set trim to false since trim will also remove trailing white space in addition to white space at the beginning (optional)
* **trim** (Default true) Remove white space from the beginning and end of the expected output and actual output (optional)
* **regexReplacements":** (Default null) A map where each key is a regex string and each value is the string to replace it with. Example: {"\\s": ""} removes all white space (optional)
* **alternativeOutputs:** (Default null) an array of alternative outputs that are also acceptable for the purpose of passing this test (optional)
* **contains:** (Default false) If this is true, then the output must contain the expected output to pass, but the expected output no longer has to match the entire output (optional)
* **startsWith:** (Default false) If this is true, then output must start with the expected output to pass, but the expected output no longer has to match the entire output (optional)
* **endsWith:** (Default false) If this is true, then the output must end with the expected output to pass, but the expected output no longer has to match the entire output (optional)
* **ignoreCase:** (Default false) If this is true, strings will be compared ignoring case
* **ignoreAllExcept:** (default false) array of strings. Removes anything except these strings. Example: ["hi", "bye"] removes everything except "hi" and "bye".

#### OutputLinesTestCase ####
This test passes if the calling method results in the expected lines of output being printed. Lines are separated with \n. Note that all the properties of OutputTestCase are also valid for this test case, but are applied to each line.

* **containsLines:** (Default false) If this is true, then the output lines must contain the expected output lines to pass, but the expected output lines no longer have to all match (optional)
* **startsWithLines:** (Default false) If this is true, then output must start with the expected lines of output to pass, but the expected output no longer has to match all the expected lines (optional)
* **endsWithLines:**" (Default false) If this is true, then the output lines must end with the expected output lines to pass, but the expected output lines no longer have to match the entire output (optional)

#### ParametersModifiedTestCase ####
This test is passed if, after calling the method, the parameters have the values given by expectedResultParameters.

* **expectedResultParameters:** The expected value of the parameters after calling the method (Required)
* **alternativeExpectedResultParameters:** (DEFAULT null) An array of other acceptable values the parameters could have after calling the method (optional) 

#### ReturnTypeTestCase ####
This test is passed if the return type of the method being tested is correct.

#### ReturnValueTestCase ####
This test is passed if the return value matches the expected return value (or one of the alternative return values if there are any).

* **expectedReturnValue:** The expected return value (Required)
* **orderMatters:** (Default true) If this is set to false, arrays will be considered equal if they contain the same elements regardless of order (optional)
* **alternativeReturnValues:** (Default null) An array of alternative return values. If the return value matches either the expected return value or one of these return values, the test will be passed. (optional)

#### StaticOrInstanceMethodTestCase ####
This test is passed if whether of not the method is correctly a static or instance method.
Note: This type of test could now be done with the correct parameters set for a MethodSignatureTestCase instead, but it still exists for backward compatability with existing tests.

* **isStatic:** (required) whether of not the method should be static

### Custom Objects ###
When defining values that are of types other than primitive, String, array, or ArrayList, you can use the following format:

```json
    {
        "className": "MyClass",
        "constructorParameterTypes": ["int", "double", "String[]"],
        "constructorParameters": [5, 8.3, ["hello", "goodbye"]]
    }
```

For example, let's say we needed to test the method

```java
    public Point getRightPoint(Point p)
```

(where Point is java.awt.Point) to see if it it correctly returns a Point object containing the coordinates of the point one unit to the right. We could define the test like this:

```json
    {
        "testCaseClass": "ReturnValueTestCase",
        "methodName": "getRightPoint",
        "parameterTypes": ["java.awt.Point"],
        "parameters": [
            // Create a Point object for (5, 8)
            {
                "className": "java.awt.Point",
                "constructorParameterTypes": ["int", "int"],
                "constructorParameters": [5, 8]
            }
        ],
        "returnType": "java.awt.Point",
        "expectedReturnValue": { // A Point object for (6, 8)
                "className": "java.awt.Point",
                "constructorParameterTypes": ["int", "int"],
                "constructorParameters": [6, 8]
        }
    }
```

If you need to make a reference to the name of the class being tested, you can use (this). For example, when testing if the tested class implements Comparable such that it can be compared to other objects of its own class, you can write:

```json
    {
        "testCaseClass": "ClassImplementsInterfaceTestCase",
        "expectedInterface": "Comparable<(this)>"
    }
```

If you need to make a reference to the name of a class named using the format PX\_LastName\_FirstName\_ClassName, you can use the (this.prefix) to refer to PX\_LastName\_FirstName\_. Here is an example of testing for a method signature for a method named insertionSort3 that takes an ArrayList of YelpRating objects where the YelpRating class should be named PX\_LastName\_FirstName\_YelpRating:

```json
    {
        "testCaseClass": "MethodSignatureTestCase",
        "methodName": "insertionSort3",
        "parameterTypes": ["ArrayList<(this.prefix)YelpRating>"]
    }
```