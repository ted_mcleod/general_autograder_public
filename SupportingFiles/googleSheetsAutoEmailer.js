// edited to send email for all scores of re AND 'y' in sent column or 'y' in the resubmit allowed column
// can turn testing mode on to verify

function onOpen() {
  SpreadsheetApp.getUi() // Or DocumentApp or FormApp.
      .createAddonMenu()
      .addItem('Show sidebar', 'showSidebar')
      .addToUi();
}

function showSidebar() {
  var html = HtmlService.createHtmlOutputFromFile('Sidebar')
      .setSandboxMode(HtmlService.SandboxMode.IFRAME)
      .setTitle('Emails!')
      .setWidth(300);
  SpreadsheetApp.getUi()
      .showSidebar(html);
}

function processForm(formObject) {
  dealWithForm(formObject, false);
}


function processTestForm(formObject){
  dealWithForm(formObject, true);
}

function dealWithForm(formObject, testing){
  // an object holding the column for each piece of info
  var formData = {
    mailCol: formObject.email,
    message: formObject.msg,
    sent: formObject.sent,
    resubmit: formObject.resub,
    firstName: formObject.first,
    lastName: formObject.last,
    score: formObject.score,
    autoGraderResults: formObject.autogr.split(",")
  }
  
  // convert letters to column numbers if necessary
  for (var key in formData) {
    if (Array.isArray(formData[key])) {
      for (var i = 0; i < formData[key].length; i++) {
        if(isNaN(parseFloat(formData[key][i]))){
          formData[key][i] = formData[key][i].trim().toLowerCase().charCodeAt(0) - 96;
        }
      }
    } else 
      if(isNaN(parseFloat(formData[key]))){
      formData[key] = formData[key].toLowerCase().charCodeAt(0) - 96;
    }
  }
    
  var sumMailRecipient = formObject.sumemail;
  if(sumMailRecipient == ""){
    sumMailRecipient = Session.getActiveUser().getEmail();
  }

  sendEmails(formData, testing, sumMailRecipient);
}

// colDic: values from form (columns on spreadsheet to process)
function sendEmails(colDic, testing, sumRecipient) {
  Logger.log("Sending emails");
  var labName = SpreadsheetApp.getActiveSpreadsheet().getName();
  var subject = labName + " Feedback";

  // for summary email
  var allNames = [];
  
  var sheet = SpreadsheetApp.getActiveSheet();

  var startRow = 1;
  var startCol = Infinity;
  
  var endCol = -1;
  
  for(var key in colDic){
    if (Array.isArray(colDic[key])) {
      startCol = Math.min(startCol, Math.min.apply(null, colDic[key]));
      endCol = Math.max(endCol, Math.max.apply(null, colDic[key]));
    } else {
      startCol = Math.min(startCol, colDic[key]);
      endCol = Math.max(endCol, colDic[key]);
    }
  }
    
  var lastRow = sheet.getLastRow();
    
  var numRows = lastRow - startRow + 1; // Number of rows to process
  var numCols = endCol - startCol + 1;
  
  // Fetch the range of cells
  var dataRange = sheet.getRange(startRow, startCol, numRows, numCols);
  
  // Fetch values for each row in the Range.
  var data = dataRange.getValues();
  
  for (var i in data) {
    var row = data[i];
    var shouldEmail = row[colDic.sent - startCol];
    var resubmit = row[colDic.resubmit - startCol];
    var score = row[colDic.score - startCol];
    var results = [];
    
    for (var r = 0; r < colDic.autoGraderResults.length; r++) {
      results.push(row[colDic.autoGraderResults[r] - startCol]);
    }
    
    if (("" + score).toLowerCase() == 're') {
      // a score of "re" requires a resubmit so resubmit should be automatically set to yes
      resubmit = "y";
    }

      // send email if explicitly noted or one isn't sent and resubmit is allowed
    if(shouldEmail.toLowerCase() == 'y' || (shouldEmail.toLowerCase() != 'sent' && ("" + resubmit).toLowerCase() == 'y')) {
            
      var emailAddress = row[colDic.mailCol - startCol];  // mail column
      Logger.log("sending email to " + emailAddress);
      allNames.push(row[colDic.firstName - startCol] + " " + row[colDic.lastName - startCol] + " (" + emailAddress + ")");
      
      // split message into array of messages separated by ". "
      var message = row[colDic.message - startCol].split('. ');       // message column
      
      
      
      // html (create ul of issues)
      var emailBody = "Hello! This is to notify you of issues with your APCS lab:" + labName + "<ul>";
      for(var item in message) {
        emailBody += "<li>" + htmlSafeStr(message[item]) + "</li>";
      }
      
      emailBody += "</ul>";

      emailBody += "<br>";
      emailBody += "<pre>";
      for (var j = 0; j < results.length; j++) {
        var autoGraderLines = results[j].split(String.fromCharCode(7)); // lines are saved using char with ascii value 7 (bell) to represent line breaks
        var reportStr = restoreNewLines(results[j]);
        Logger.log("decompressing report");
        emailBody += decompress(reportStr);
        Logger.log("finished decompressing");
        emailBody += "\n\n\n\n"
      }
      emailBody += "<\pre>";
      if (("" + score).toLowerCase() == 're') {
      // clarity
        emailBody += "Please resubmit with corrections to the late/resubmit form in the downloads page to receive a score.";
      } else if (("" + resubmit).toLowerCase() == 'y') {
        emailBody += "If you fix the issue(s), you may resubmit the corrected lab to the late/resubmit form in the downloads page.";
      } else {
        emailBody += "Please do not resubmit. Your lab does not qualify for resubmission.";
      }
      
      emailBody += "\n<p style='font-size: 10px'>Disclaimer: This message was not sent by a human. Questions? Contact " + Session.getActiveUser().getEmail() + "</p>";
      
      if(testing){
        subject += " TEST EMAIL";
        emailBody += "send to: " + emailAddress + "\n";
        emailAddress = Session.getActiveUser().getEmail();
        emailBody += allNames.toString();
      }
      
      MailApp.sendEmail({
        to: emailAddress,
        subject: subject,
        htmlBody: emailBody
     });
      
     // scales by 10???
      sheet.getRange((i + 1)/10 + 1, colDic.sent).setValue("sent");
      Logger.log(emailAddress + ", " + subject + ", " + emailBody);
      
      if(testing){
        break;
      }
    }
  }// end for loop
      // send summary report
      
    var ccTeacher = "<ul><li>" + allNames.join('</li><li>') + "</ul>";
    var toTeacher = "Email sent to " + allNames.length + " students: <br>" + ccTeacher;
    
     MailApp.sendEmail({
        to: sumRecipient,
        subject: subject + " Emails Sent",
        htmlBody: toTeacher
     });
}

function htmlSafeStr(str) {
    return ("" + str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

function restoreNewLines(str) {
  var strLines = "";
  for (var i = 0; i < str.length; i++) {
    var nextNewLine = str.indexOf(String.fromCharCode(7), i);
    if (nextNewLine == -1) break;
    strLines += str.substring(i, nextNewLine) + "\n";
    i = nextNewLine;
  }
  return strLines;
}

function decompress(str) {
  var endOfLinesMap = indexOfEndOfMap(str);
  if (endOfLinesMap == -1) return str;
  var linesMap = parseToLinesMap(str.substring(0, endOfLinesMap + 1));
  var s = str.substring(endOfLinesMap + 1);
  s = insertLines(s, linesMap);
  return s;
}

function parseToLinesMap(str) {
  var map = {};
  for (var i = 0; i < str.length; i++) {
    var nextLineEnd = str.indexOf("\n", i);
    if (nextLineEnd == -1) {
      break;
    }
    var line = str.substring(i, nextLineEnd);
    i = nextLineEnd + 1;
    var indexesEnd = str.indexOf("\n", i);
    var indexesStr = str.substring(i, indexesEnd);
    i = indexesEnd;
    if (indexesEnd == -1) {
      break;
    }
    var indexes = [];
    for (var j = 0; j < indexesStr.length; j++) {
      var nextSeparator = indexesStr.indexOf(" ", j);
      if (nextSeparator == -1) {
          break;
      }
      var indexStr = indexesStr.substring(j, nextSeparator);
      indexes.push(parseInt(indexStr));
      j = nextSeparator;
    }
    map[line] = indexes;
  }
  return map;
}

function insertLines(str, map) {
    var nlIndexes = newLineIndexes(str);
    for (var line in map) {
        var lineNumbers = map[line];
        for (var i = 0; i < lineNumbers.length; i++) {
            var lineNum = lineNumbers[i];
        	var indexToInsert = nlIndexes[lineNum];
        	// don't need to add new line since it will be there already
        	str = str.substring(0, indexToInsert) + line + str.substring(indexToInsert);
        	updateNewLineIndexesForLineAdded(nlIndexes, line.length, lineNum);
        }
    }
    return str;
}

function updateNewLineIndexesForLineAdded(indexes, lineLength, index) {
    for (var i = index; i < indexes.length; i++) {
        indexes[i] += lineLength;
    }
}

function newLineIndexes(str) {
  var indexes = [];
  for(var i = 0; i < str.length; i++) {
    if (str.charAt(i) == "\n") {
      indexes.push(i);
    }
  }
  return indexes;
}

function indexOfEndOfMap(str) {
  if (str.indexOf(".\n") == -1) return -1;
  var endOfNextList = -1;
  do {
    endOfNextList = str.indexOf('\n', endOfNextList + 1);
    endOfNextList = str.indexOf('\n', endOfNextList + 1);
  } while (str.charAt(endOfNextList - 1) != '.');
  return endOfNextList;
}
