import java.awt.BorderLayout;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.tinocs.generalAutoGrader.FileUtil;
import com.tinocs.generalAutoGrader.TestRunner;

public class AutoTesterDriver {
	
	private static final String PREF_FILE = "gatChooserPrefs";
	private static final String LAST_CHOOSER_PATH_KEY = "lastChooserPath";
	private static final String LAST_ROSTER_NAMES_KEY = "lastRosterNames";
	private static final String LAST_TEST_RUN_KEY = "lastTestRun";
	
	private static final String TEST_DEFS_DIR_PATH = "test_defs";
	private static final String FILES_TO_TEST_DIR = "src/java_files_to_test";
	private static final String ROSTERS_DIR = "rosters";
	
	// set to true to get maximum information about test results
	// generally you want this true since once a test fails you want
	// to know as much as possible about why it failed
	private static final boolean VERBOSE = true;
	
	// set to true to only see failed tests in reports (makes it easier to read)
	private static final boolean PRINT_ONLY_FAILED_TESTS = true;
	
	// set to true to include speed results and false to hide them
	// Useful for running performance comparisons
	private static final boolean INCLUDE_RUN_TIMES = false;
	
	public static void main(String[] args) {
    	try {
	        File resultsDir = null;
	        JsonObject prefs = null;
	        File prefFile = new File(PREF_FILE);
	        if(prefFile.exists() && !prefFile.isDirectory()) { 
	        	prefs = FileUtil.getObj(PREF_FILE).getAsJsonObject();
	        }
	        	
	        JFileChooser mainChooser = null;
	        if (prefs == null || !prefs.has(LAST_CHOOSER_PATH_KEY)) {
	        	mainChooser = new JFileChooser();
	        	if (prefs == null) {
	        		prefs = new JsonObject();
	        	}
	        } else {
	        	mainChooser = new JFileChooser(prefs.get(LAST_CHOOSER_PATH_KEY).getAsString());
	        }
			
	        // choose a directory to save the results
			mainChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			mainChooser.setDialogTitle("Choose directory for reports.");
			int retVal = mainChooser.showOpenDialog(null);
		    if(retVal == JFileChooser.APPROVE_OPTION) {
		       resultsDir = mainChooser.getSelectedFile();
		    } else {
		    	System.out.println("retVal = " + retVal);
		    }
		    
			if (resultsDir == null) {
				abort();
			}
			
			prefs.addProperty(LAST_CHOOSER_PATH_KEY, resultsDir.getAbsolutePath());
			FileUtil.writeString(new File(PREF_FILE), prefs.toString());
			
			JsonArray lastRosterNames = prefs.has(LAST_ROSTER_NAMES_KEY) && prefs.get(LAST_ROSTER_NAMES_KEY).isJsonArray() ? prefs.get(LAST_ROSTER_NAMES_KEY).getAsJsonArray() : null;
			JPanel gui = new JPanel(new BorderLayout());
			
            File[] rosterFiles = new File(ROSTERS_DIR).listFiles();
            if (rosterFiles == null) {
            	rosterFiles = new File[0];
            	System.out.println("WARNING: Missing rosters directory: " + new File(ROSTERS_DIR).getAbsolutePath());
            }
            String[] rosterNames = new String[rosterFiles.length];
            for (int i = 0; i < rosterFiles.length; i++) {
            	rosterNames[i] = rosterFiles[i].getName();
            }
            JList<String> list = new JList<String>(rosterNames);
            List<Integer> preSelectedRosters = new ArrayList<>();
            if (lastRosterNames != null) {
				for (JsonElement el : lastRosterNames) {
					String rosName = el.getAsString();
					if (rosName != null) {
						for (int i = 0; i < list.getModel().getSize(); i++) {
							if (rosName.equals(list.getModel().getElementAt(i))) {
								preSelectedRosters.add(i);
							}
						}
					}
				}
			}
            int[] selectedIndices = new int[preSelectedRosters.size()];
            for (int i = 0; i < preSelectedRosters.size(); i++) {
            	selectedIndices[i] = preSelectedRosters.get(i);
            }
            System.out.println("Preselected Indices:" + Arrays.toString(selectedIndices));
            list.setSelectedIndices(selectedIndices);
            
            gui.add(new JScrollPane(list));

            JOptionPane.showMessageDialog(
                    null, 
                    gui,
                    "Which rosters would you like to use?",
                    JOptionPane.QUESTION_MESSAGE);
            
            List<String> chosenRosterNames = (List<String>)list.getSelectedValuesList();
            System.out.println("Selected Rosters: " + chosenRosterNames);
            JsonArray lastRosters = new JsonArray();
            for (String rosName : chosenRosterNames) {
            	lastRosters.add(rosName);
            }
            
            prefs.add(LAST_ROSTER_NAMES_KEY, lastRosters);
			FileUtil.writeString(new File(PREF_FILE), prefs.toString());
			
			File[] testDefFiles = new File(TEST_DEFS_DIR_PATH).listFiles();
			if (testDefFiles == null) {
				System.err.println("Fatal Error: Missing test_defs directory: " + new File(TEST_DEFS_DIR_PATH).getAbsolutePath());
				abort();
				return;
			}
			String[] availableTests = new String[testDefFiles.length];
			for (int i = 0; i < testDefFiles.length; i++) {
				availableTests[i] = testDefFiles[i].getName();
			}
			
			String lastTestRun = prefs.has(LAST_TEST_RUN_KEY) ? prefs.get(LAST_TEST_RUN_KEY).getAsString() : null;
		    String initialSelection = lastTestRun != null && Arrays.asList(availableTests).contains(lastTestRun) ? lastTestRun : availableTests[0];
		    String testSelected = (String)JOptionPane.showInputDialog(null, "Choose a test to run.",
		        "Choose a Test", JOptionPane.QUESTION_MESSAGE, null, availableTests, initialSelection);
			if (testSelected == null) {
				abort();
			}
			
			prefs.addProperty(LAST_TEST_RUN_KEY, testSelected);
			FileUtil.writeString(new File(PREF_FILE), prefs.toString());
			
			TestRunner.runTests(new File(TEST_DEFS_DIR_PATH + File.separator + testSelected), new File(FILES_TO_TEST_DIR), resultsDir, new File(ROSTERS_DIR), chosenRosterNames, VERBOSE, PRINT_ONLY_FAILED_TESTS, INCLUDE_RUN_TIMES);

    	} catch (Exception e) {
    		e.printStackTrace();
    	}
	}
	
	private static void abort() {
		System.out.println("Test Aborted.");
		System.exit(0);
	}
}