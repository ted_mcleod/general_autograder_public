package com.tinocs.generalAutoGrader.testCase;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import com.google.gson.JsonObject;

/**
 * This test passes if the class has the method name and parameter types given in the test case
 * @author Ted_McLeod
 *
 */
public class MethodSignatureTestCase extends DeclarationTestCase {
	
	public static final String EXPECTED_METHOD_MODIFIERS_KEY = "expectedMethodModifiers"; // (DEFAULT ["public"]) Method must have these access modifiers to pass the test (optional)
	public static final String FORBIDDEN_METHOD_MODIFIERS_KEY = "forbiddenMethodModifiers"; // (DEFAULT []) Method fails the test if it has any of these access modifiers (optional)
	
	private String[] expectedMethodModifiers;
	private String[] forbiddenMethodModifiers;
	
	public MethodSignatureTestCase(JsonObject testCaseDef, String classBeingTested) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, MalformedURLException {
		super(testCaseDef, classBeingTested);
		
		// modifiers
		try {
			this.expectedMethodModifiers = testCaseDef.has(EXPECTED_METHOD_MODIFIERS_KEY) ? stringArrayFromJsonArray(testCaseDef.get(EXPECTED_METHOD_MODIFIERS_KEY).getAsJsonArray()) : new String[]{"public"};
		} catch (IllegalStateException err) {
			printInvalidFormatError(PARAMETER_TYPES_KEY);
			throw err;
		}
		
		// modifiers
		try {
			this.forbiddenMethodModifiers = testCaseDef.has(FORBIDDEN_METHOD_MODIFIERS_KEY) ? stringArrayFromJsonArray(testCaseDef.get(FORBIDDEN_METHOD_MODIFIERS_KEY).getAsJsonArray()) : new String[0];
		} catch (IllegalStateException err) {
			printInvalidFormatError(PARAMETER_TYPES_KEY);
			throw err;
		}
	}
	
	@Override
	public LinkedHashMap<String, String> propertyDescriptions() {
		LinkedHashMap<String, String> props = super.propertyDescriptions();
		if (expectedMethodModifiers.length > 0) props.put("Expected Method Modifiers", Arrays.toString(expectedMethodModifiers));
		if (forbiddenMethodModifiers.length > 0) props.put("Forbidden Method Modifiers", Arrays.toString(forbiddenMethodModifiers));
		return props;
	}

	@Override
	protected void runCase() {
		try {
			Method m = getMethodWithCorrectSignature();
			if (m != null) {
				String declaration = getDeclaration(m);
				boolean failed = false;
				ArrayList<String> modifiers = new ArrayList<>(Arrays.asList(Modifier.toString(m.getModifiers()).split(" ")));
				if (!modifiers.containsAll(new ArrayList<String>(Arrays.asList(expectedMethodModifiers)))) {
					getResult().addConditionalNote(declaration + " does not have the required modifiers: " + Arrays.toString(expectedMethodModifiers));
					failed = true;
				}
				
				for (String mod : forbiddenMethodModifiers) {
					if (modifiers.contains(mod)) {
						getResult().addConditionalNote(declaration + " has forbidden modifier: " + mod);
						failed = true;
					}
				}
				getResult().setPassed(!failed);
			}
		} catch (Throwable e) {
			getResult().addException(e);
		}
	}

	@Override
	protected String attributesDescription() {
		return super.attributesDescription() + ", expectedMethodModifiers=" + Arrays.toString(expectedMethodModifiers) + ", forbiddenMethodModifiers=" + Arrays.toString(forbiddenMethodModifiers);
	}
	
	@Override
	protected Set<String> validTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> validParams = super.validTestCaseParameters();
		if (validParams == null) {
			validParams = new HashSet<>();
		}
		validParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				EXPECTED_METHOD_MODIFIERS_KEY, FORBIDDEN_METHOD_MODIFIERS_KEY})));
		return validParams;
	}

	@Override
	protected Set<String> requiredTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> reqParams = super.requiredTestCaseParameters();
		if (reqParams == null) {
			reqParams = new HashSet<>();
		}
		reqParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				METHOD_NAME_KEY, PARAMETER_TYPES_KEY})));
		return reqParams;
	}
}
