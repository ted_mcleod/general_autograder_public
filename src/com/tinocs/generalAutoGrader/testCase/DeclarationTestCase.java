package com.tinocs.generalAutoGrader.testCase;

import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.google.gson.JsonObject;

public abstract class DeclarationTestCase extends TestCase {

	public DeclarationTestCase(JsonObject testCaseDef, String classBeingTested)
			throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException, MalformedURLException {
		super(testCaseDef, classBeingTested);
	}

	@Override
	protected Set<String> invalidTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> invalidParams = super.invalidTestCaseParameters();
		if (invalidParams == null) {
			invalidParams = new HashSet<>();
		}
		invalidParams.addAll(new HashSet<>(Arrays.asList(new String[]{TIMEOUT_KEY,
				PARAMETERS_KEY, CONSTRUCTOR_PARAMETERS_KEY, INPUTS_KEY})));
		return invalidParams;
	}
}
