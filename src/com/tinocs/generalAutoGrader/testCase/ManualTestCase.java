package com.tinocs.generalAutoGrader.testCase;

import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;

import com.google.gson.JsonObject;
import com.tinocs.generalAutoGrader.StringUtil;

/**
 * This test case automatically passes because it is expected that the user will be checking the result manually.
 * To make it easier to check all the test cases, the user will be prompted to press enter after each test case.
 * @author Ted_McLeod
 *
 */
public class ManualTestCase extends ExecutionTestCase {

	// This type of test can be used to test programs where the output needs to be manually tested.
	// After each test case it wait for the user to press enter before running the next test case
	public ManualTestCase(JsonObject testCaseDef, String classBeingTested) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, MalformedURLException {
		super(testCaseDef, classBeingTested);
		setTimeout(Long.MAX_VALUE); // effectively no timeout for manual tests
	}
	
	@Override
	public boolean promptForNextCase() {
		return true;
	}

	@Override
	protected void runCase() {
		PrintStream prevSysOut = System.out;
		PrintStream prevSysErr = System.err;
		try {
			SYS_OUT.println("Running test case:");
			SYS_OUT.println(StringUtil.getNChars(40, '-'));
			printPropertyDescriptions(SYS_OUT);
			SYS_OUT.println(StringUtil.getNChars(40, '-'));
			// Since this is a manual test, the user will want to see the output
			System.setOut(SYS_OUT);
			System.setErr(SYS_ERR);
			Method method = getMethodWithCorrectSignature();
			if (method != null && getParameters() != null) {
				Object inst = !Modifier.isStatic(method.getModifiers()) ? getInstanceOfClass(getClassBeingTested()) : null;
				method.setAccessible(true);
				method.invoke(inst, getParameters());
				getResult().setPassed(true);
			}
		} catch (Throwable e) {
			getResult().addException(e);
		}
		System.setOut(prevSysOut);
		System.setErr(prevSysErr);
	}
}
