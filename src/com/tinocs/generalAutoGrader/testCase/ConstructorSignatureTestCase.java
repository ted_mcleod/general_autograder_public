package com.tinocs.generalAutoGrader.testCase;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import com.google.gson.JsonObject;
/**
 * This test is passed if the class being tested has a constructor with the correct signature
 * @author Ted_McLeod
 *
 */
public class ConstructorSignatureTestCase extends DeclarationTestCase {
	
	public static final String EXPECTED_CONSTRUCTOR_MODIFIERS_KEY = "expectedConstructorModifiers"; // (DEFAULT ["public"]) Constructor must have these access modifiers to pass the test (optional)
	public static final String FORBIDDEN_CONSTRUCTOR_MODIFIERS_KEY = "forbiddenConstructorModifiers"; // (DEFAULT []) Constructor fails the test if it has any of these access modifiers (optional)
	
	private String[] expectedConstructorModifiers;
	private String[] forbiddenConstructorModifiers;
	
	public ConstructorSignatureTestCase(JsonObject testCaseDef, String classBeingTested) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, MalformedURLException {
		super(testCaseDef, classBeingTested);
		
		// modifiers
		try {
			this.expectedConstructorModifiers = testCaseDef.has(EXPECTED_CONSTRUCTOR_MODIFIERS_KEY) ? stringArrayFromJsonArray(testCaseDef.get(EXPECTED_CONSTRUCTOR_MODIFIERS_KEY).getAsJsonArray()) : new String[]{"public"};
		} catch (IllegalStateException err) {
			printInvalidFormatError(EXPECTED_CONSTRUCTOR_MODIFIERS_KEY);
			throw err;
		}
		
		// forbidden modifiers
		try {
			this.forbiddenConstructorModifiers = testCaseDef.has(FORBIDDEN_CONSTRUCTOR_MODIFIERS_KEY) ? stringArrayFromJsonArray(testCaseDef.get(FORBIDDEN_CONSTRUCTOR_MODIFIERS_KEY).getAsJsonArray()) : new String[0];
		} catch (IllegalStateException err) {
			printInvalidFormatError(FORBIDDEN_CONSTRUCTOR_MODIFIERS_KEY);
			throw err;
		}
	}
	
	@Override
	public LinkedHashMap<String, String> propertyDescriptions() {
		LinkedHashMap<String, String> props = super.propertyDescriptions();
		if (expectedConstructorModifiers.length > 0) props.put("Expected Constructor Modifiers", Arrays.toString(expectedConstructorModifiers));
		return props;
	}

	@Override
	protected void runCase() {
		Class<?> cls;
		try {
			cls = Class.forName(getClassBeingTested());
			Constructor<?>[] constructors = cls.getDeclaredConstructors();
			String constructorSignatures = "Found Declared Constructor Signatures: ";
			ArrayList<String> possibleNotes = new ArrayList<>();
			for (Constructor<?> constr : constructors) {
				String sig = Modifier.toString(constr.getModifiers()) + " " + constr.getName() + "(";
				Type[] paramTypes = constr.getGenericParameterTypes();
				for (Type type : paramTypes) {
					sig += type.getTypeName() + ", ";
				}
				if (paramTypes.length > 0) sig = sig.substring(0, sig.lastIndexOf(','));
				sig += "), ";
				constructorSignatures += sig;
				// some parameters, such as an untyped ArrayList will have "class " preceding their name
				String paramsStr = Arrays.toString(paramTypes).replaceAll("class ", "");
				String[] paramsStrArr = paramsStr.substring(1, paramsStr.length() - 1).split(", ");
				for (int i = 0; i < paramsStrArr.length; i++) {
					if (paramsStrArr[i].startsWith("[")) {
						paramsStrArr[i] = paramTypes[i].getTypeName();
					}
				}
				paramsStr = Arrays.toString(paramsStrArr);
				String expectedParamsStr = Arrays.toString(getFullConstructorParameterTypes());
				if (paramsStr.equals(expectedParamsStr)) {
					boolean failed = false;
					ArrayList<String> modifiers = new ArrayList<>(Arrays.asList(Modifier.toString(constr.getModifiers()).split(" ")));
					if (!modifiers.containsAll(new ArrayList<String>(Arrays.asList(expectedConstructorModifiers)))) {
						possibleNotes.add(sig + " does not include required modifiers: " + Arrays.toString(expectedConstructorModifiers));
						failed = true;
					}
					
					for (String mod : forbiddenConstructorModifiers) {
						if (modifiers.contains(mod)) {
							possibleNotes.add(sig + " has forbidden modifier: " + mod);
							failed = true;
						}
					}
					
					getResult().setPassed(!failed);
				}
				else possibleNotes.add(paramsStr + " does not match " + expectedParamsStr);
			}
			if (!getResult().isPassed()) {
				for (String note : possibleNotes) {
					getResult().addNote(note);
				}
			}
			if (constructors.length > 0) constructorSignatures = constructorSignatures.substring(0, constructorSignatures.lastIndexOf(','));
			getResult().addNote(constructorSignatures);
		} catch (Throwable e) {
			getResult().addException(e);
		}
	}

	public String[] getExpectedConstructorModifiers() {
		return expectedConstructorModifiers;
	}

	public void setExpectedConstructorModifiers(String[] expectedConstructorModifiers) {
		this.expectedConstructorModifiers = expectedConstructorModifiers;
	}

	@Override
	protected String attributesDescription() {
		return super.attributesDescription() + ", expectedConstructorModifiers=" + Arrays.toString(expectedConstructorModifiers);
	}
	
	@Override
	protected Set<String> validTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> validParams = super.validTestCaseParameters();
		if (validParams == null) {
			validParams = new HashSet<>();
		}
		validParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				EXPECTED_CONSTRUCTOR_MODIFIERS_KEY,
				FORBIDDEN_CONSTRUCTOR_MODIFIERS_KEY})));
		return validParams;
	}

	@Override
	protected Set<String> requiredTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> reqParams = super.requiredTestCaseParameters();
		if (reqParams == null) {
			reqParams = new HashSet<>();
		}
		reqParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				CONSTRUCTOR_PARAMETER_TYPES_KEY})));
		return reqParams;
	}

	@Override
	protected Set<String> invalidTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> invalidParams = super.invalidTestCaseParameters();
		if (invalidParams == null) {
			invalidParams = new HashSet<>();
		}
		invalidParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				METHOD_NAME_KEY, PARAMETER_TYPES_KEY, RETURN_TYPE_KEY})));
		return invalidParams;
	}
	
}
