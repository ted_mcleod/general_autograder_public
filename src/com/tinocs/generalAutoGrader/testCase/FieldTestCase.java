package com.tinocs.generalAutoGrader.testCase;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import com.google.gson.JsonObject;

/**
 * This test is passed if the class being tested has a field with the expected
 *  properties (i.e. field name, modifiers and/or field type)
 * @author Ted_McLeod
 *
 */
public class FieldTestCase extends DeclarationTestCase {
	
	public static final String EXPECTED_FIELD_NAME_KEY = "expectedFieldName"; // (DEFAULT null) The name of the expected field (optional)
	public static final String EXPECTED_FIELD_MODIFIERS_KEY = "expectedFieldModifiers"; // (DEFAULT []) Field must have these access modifiers to pass the test (optional)
	public static final String FORBIDDEN_FIELD_MODIFIERS_KEY = "forbiddenFieldModifiers"; // (DEFAULT []) Test fails if the field has any of these modifiers (optional)
	public static final String EXPECTED_FIELD_TYPE_KEY = "expectedFieldType"; // (DEFAULT null) The expected type of the field
	
	private String expectedFieldName;
	private String[] expectedFieldModifiers;
	private String[] forbiddenFieldModifiers;
	private String expectedFieldType;
	
	public FieldTestCase(JsonObject testCaseDef, String classBeingTested) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, MalformedURLException {
		super(testCaseDef, classBeingTested);
		
		boolean hasFieldRequirement = false;
		
		// field name
		try {
			this.expectedFieldName = testCaseDef.has(EXPECTED_FIELD_NAME_KEY) ? testCaseDef.get(EXPECTED_FIELD_NAME_KEY).getAsString() : null;
			hasFieldRequirement = true;
		} catch (IllegalStateException err) {
			printInvalidFormatError(EXPECTED_FIELD_NAME_KEY);
			throw err;
		}
		
		// field type
		try {
			this.expectedFieldType = testCaseDef.has(EXPECTED_FIELD_TYPE_KEY) ? cleanUpClassName(testCaseDef.get(EXPECTED_FIELD_TYPE_KEY).getAsString()) : null;
			hasFieldRequirement = true;
		} catch (IllegalStateException err) {
			printInvalidFormatError(EXPECTED_FIELD_TYPE_KEY);
			throw err;
		}
		
		// modifiers
		try {
			this.expectedFieldModifiers = testCaseDef.has(EXPECTED_FIELD_MODIFIERS_KEY) ? stringArrayFromJsonArray(testCaseDef.get(EXPECTED_FIELD_MODIFIERS_KEY).getAsJsonArray()) : new String[0];
			hasFieldRequirement = true;
		} catch (IllegalStateException err) {
			printInvalidFormatError(EXPECTED_FIELD_MODIFIERS_KEY);
			throw err;
		}
		
		// modifiers
		try {
			this.forbiddenFieldModifiers = testCaseDef.has(FORBIDDEN_FIELD_MODIFIERS_KEY) ? stringArrayFromJsonArray(testCaseDef.get(FORBIDDEN_FIELD_MODIFIERS_KEY).getAsJsonArray()) : new String[0];
			hasFieldRequirement = true;
		} catch (IllegalStateException err) {
			printInvalidFormatError(FORBIDDEN_FIELD_MODIFIERS_KEY);
			throw err;
		}
		
		if (!hasFieldRequirement) {
			throw new IllegalArgumentException(getClass().getName() + " must have some requirements defined. What has to be true about the field?");
		}
	}
	
	
	
//	@Override
//	protected Set<String> validTestCaseParameters() {
//		return new HashSet<>(Arrays.asList(new String[]{
//				EXPECTED_FIELD_NAME_KEY, EXPECTED_FIELD_MODIFIERS_KEY,
//				FORBIDDEN_FIELD_MODIFIERS_KEY, EXPECTED_FIELD_TYPE_KEY}));
//	}
	
	@Override
	public LinkedHashMap<String, String> propertyDescriptions() {
		LinkedHashMap<String, String> props = super.propertyDescriptions();
		if (expectedFieldName != null) props.put("Expected Field Name", expectedFieldName);
		if (expectedFieldType != null) props.put("Expected Field Type", expectedFieldType);
		if (expectedFieldModifiers.length > 0) props.put("Expected Field Modifiers", Arrays.toString(expectedFieldModifiers));
		if (forbiddenFieldModifiers.length > 0) props.put("Forbidden Field Modifiers", Arrays.toString(forbiddenFieldModifiers));
		return props;
	}

	@Override
	protected void runCase() {
		try {//String fieldName, String expectedFieldType, String[] expectedFieldModifiers, String[] forbiddenFieldModifiers
			Field field = getField(expectedFieldName, expectedFieldType, expectedFieldModifiers, forbiddenFieldModifiers);
			getResult().setPassed(field != null);
		} catch (Throwable e) {
			getResult().addException(e);
		}
	}

	public String getExpectedFieldName() {
		return expectedFieldName;
	}

	public void setExpectedFieldName(String expectedFieldName) {
		this.expectedFieldName = expectedFieldName;
	}

	public String[] getExpectedFieldModifiers() {
		return expectedFieldModifiers;
	}

	public void setExpectedFieldModifiers(String[] expectedFieldModifiers) {
		this.expectedFieldModifiers = expectedFieldModifiers;
	}

	public String getExpectedFieldType() {
		return expectedFieldType;
	}

	public void setExpectedFieldType(String expectedFieldType) {
		this.expectedFieldType = expectedFieldType;
	}
	
	public String[] getForbiddenFieldModifiers() {
		return forbiddenFieldModifiers;
	}

	public void setForbiddenFieldModifiers(String[] forbiddenFieldModifiers) {
		this.forbiddenFieldModifiers = forbiddenFieldModifiers;
	}

	@Override
	protected String attributesDescription() {
		return super.attributesDescription() + ", expectedFieldName=" + expectedFieldName + ", expectedFieldType=" + expectedFieldType + ", expectedFieldModifiers=" + Arrays.toString(expectedFieldModifiers) + ", forbiddenFieldModifiers=" + Arrays.toString(forbiddenFieldModifiers);
	}
	
	@Override
	protected Set<String> validTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> validParams = super.validTestCaseParameters();
		if (validParams == null) {
			validParams = new HashSet<>();
		}
		validParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				EXPECTED_FIELD_NAME_KEY, EXPECTED_FIELD_MODIFIERS_KEY,
				FORBIDDEN_FIELD_MODIFIERS_KEY, EXPECTED_FIELD_TYPE_KEY})));
		return validParams;
	}

	@Override
	protected Set<String> invalidTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> invalidParams = super.invalidTestCaseParameters();
		if (invalidParams == null) {
			invalidParams = new HashSet<>();
		}
		invalidParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				METHOD_NAME_KEY, PARAMETER_TYPES_KEY, RETURN_TYPE_KEY,
				CONSTRUCTOR_PARAMETER_TYPES_KEY})));
		return invalidParams;
	}
}
