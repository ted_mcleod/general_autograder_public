package com.tinocs.generalAutoGrader.testCase;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import com.google.gson.JsonObject;
/**
 * This test is passed if whether of not the method is correctly a static or instance method.
 * Note: This type of test could be done with the correct parameters set for a MethodSignatureTestCase now instead, but
 * it still exists for backward compatibility with existing tests.
 * @author Ted_McLeod
 *
 */
public class StaticOrInstanceMethodTestCase extends DeclarationTestCase {
	
	private static final String IS_STATIC_KEY = "isStatic"; // (required) whether of not the method should be static
	
	private boolean isStatic;
	
	public StaticOrInstanceMethodTestCase(JsonObject testCaseDef, String classBeingTested) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, MalformedURLException {
		super(testCaseDef, classBeingTested);
		
		// isStatic
		try {
			this.isStatic = testCaseDef.get(IS_STATIC_KEY).getAsBoolean();
		} catch (IllegalStateException err) {
			printInvalidFormatError(IS_STATIC_KEY);
			throw err;
		}
	}
	
	@Override
	public LinkedHashMap<String, String> propertyDescriptions() {
		LinkedHashMap<String, String> props = super.propertyDescriptions();
		props.put("Is Static", "" + isStatic);
		return props;
	}

	@Override
	protected void runCase() {
		try {
			Method m = getMethodWithCorrectSignature();
			if (m != null) {
				ArrayList<String> modifiers = new ArrayList<>(Arrays.asList(Modifier.toString(m.getModifiers()).split(" ")));
				
				if (modifiers.contains("static") != isStatic) {
					getResult().addConditionalNote(getDeclaration(m) + " should " + (isStatic ? "contain " : "not contain ") + " the static modifier");
				} else {
					getResult().setPassed(true);
				}
			}
		} catch (Throwable e) {
			getResult().addException(e);
		}
	}

	@Override
	protected String attributesDescription() {
		return super.attributesDescription() + ", isStatic=" + isStatic;
	}
	
	@Override
	protected Set<String> validTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> validParams = super.validTestCaseParameters();
		if (validParams == null) {
			validParams = new HashSet<>();
		}
		validParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				IS_STATIC_KEY})));
		return validParams;
	}

	@Override
	protected Set<String> requiredTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> reqParams = super.requiredTestCaseParameters();
		if (reqParams == null) {
			reqParams = new HashSet<>();
		}
		reqParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				METHOD_NAME_KEY, PARAMETER_TYPES_KEY})));
		return reqParams;
	}

	@Override
	protected Set<String> invalidTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> invalidParams = super.invalidTestCaseParameters();
		if (invalidParams == null) {
			invalidParams = new HashSet<>();
		}
		invalidParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				RETURN_TYPE_KEY, CONSTRUCTOR_PARAMETER_TYPES_KEY})));
		return invalidParams;
	}
}
