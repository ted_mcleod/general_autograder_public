package com.tinocs.generalAutoGrader.testCase;

import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;

import com.google.gson.JsonObject;

public abstract class ExecutionTestCase extends TestCase {

	public ExecutionTestCase(JsonObject testCaseDef, String classBeingTested)
			throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException, MalformedURLException {
		super(testCaseDef, classBeingTested);
	}

	@Override
	protected boolean requiresMethodOrConstructorCall() {
		return true;
	}
}
