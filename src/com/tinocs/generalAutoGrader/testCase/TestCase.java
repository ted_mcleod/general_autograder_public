package com.tinocs.generalAutoGrader.testCase;
import java.io.ByteArrayInputStream;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.tinocs.generalAutoGrader.StringUtil;
import com.tinocs.generalAutoGrader.TestCaseResult;
import com.tinocs.generalAutoGrader.TestRunner;
import com.tinocs.generalAutoGrader.TimeoutThread;

public abstract class TestCase {
	public static final String TEST_CASE_CLASS_KEY = "testCaseClass";
	public static final String COMMENT_KEY = "comment"; // (Default: null) Use this field to write comments or a description of what is being tested (optional)
	public static final String POINTS_KEY = "points"; // (Default: 1) The number of points the test is worth (optional)
	public static final String TIMEOUT_KEY = "timeout"; // (Default: 200 for all cases except ManualTestCase, for which it is Long.MAX_VALUE (effectively no timeout)) The number of milliseconds to wait for the method to finish executing before timing out. (For most tests the default should be long enough that only infinite loops will time out)
	public static final String METHOD_NAME_KEY = "methodName"; // (Default: null) The name of the method being tested
	public static final String PARAMETER_TYPES_KEY = "parameterTypes"; // (Default: []) Parameter types of the method being tested
	public static final String RETURN_TYPE_KEY = "returnType"; // (Default: null) The return type of the method being tested (Only required for tests that check the return value or return type)
	public static final String PARAMETERS_KEY = "parameters"; // (Default: []) The parameters to pass to the method being tested
	public static final String CONSTRUCTOR_PARAMETERS_KEY = "constructorParameters"; // (Default: []) The parameters to pass to the constructor when instantiating the class being tested
	public static final String CONSTRUCTOR_PARAMETER_TYPES_KEY = "constructorParameterTypes"; // (Default: []) The parameter types for the constructor to use when instantiating the class being tested
	public static final String INPUTS_KEY = "inputs"; // (Default: null) the input buffer to populate System.in with.
	public static final String MAX_PRINT_CALLS_KEY = "maxPrintCalls"; // (Default: 100) The maximum number of print calls (i.e. System.out.println) that will be printed in the result. The default should be sufficient for most cases without allowing too much spamming from infinite loops.
	
	private final Set<String> validTestCaseParameters;
	private final Set<String> requiredTestCaseParameters;

	// This field should be used when defining parameters that are of types other than primitive, String, array, or ArrayList.
	/* You can define a custom class object like this:
	 *  {
	 *  	"className": "MyClass",
	 *  	"constructorParameterTypes": ["int", "double", "String[]"],
	 *  	"constructorParameters": [5, 8.3, ["hello", "goodbye"]]
	 *  }
	 */
	public static final String CLASS_NAME = "className";

	private static final Set<String> JAVA_LANG_CLASSES = new HashSet<String>(Arrays.asList(new String[]{"Integer", "Character", "Short", "Long", "Byte", "Boolean", "Double", "Float"}));

	// since the threads that time out will still be running future threads will be more laggy,
	// so to prevent timeouts of threads that are not actually in infinite loops, the timeout
	// limit will increase each time.
	private static final long DEFAULT_TIMEOUT = 200;
	private static volatile int numTimeouts = 0;
	private static volatile long timeoutIncreasePerTimeout = 50;
	public static final PrintStream SYS_OUT = System.out; // original System.out
	public static final PrintStream SYS_ERR = System.err; // original System.err
	
	private static final long DEFAULT_MAX_PRINT_CALLS = 100;

	private String classBeingTested;
	private String comment;
	private double points;
	private TestCaseResult result;
	private String methodName;
	private Class<?>[] parameterTypes;
	private String[] fullParameterTypes;
	private Class<?> returnType;
	private String fullReturnType;
	private Object[] parameters;
	private Object[] originalParameters;
	private Class<?>[] constructorParameterTypes;
	private String[] fullConstructorParameterTypes;
	private Object[] constructorParameters;
	private Object[] originalConstructorParameters;
	private String inputs;
	private long maxPrintCalls;
	private Thread runThread;
	private TimeoutThread testRunnable;
	private TimeoutThread timeoutRunnable;
	private long timeout;
	private TestCase nextCase;
	private Runnable callback;
	private String threadID;

	// This class loader ensures static variables get initialized for
	// each test case. This allows System.in to be captured before the class
	// is loaded. This could also have the effect of masking errors from having
	// static global variables that cause a method to fail if called more than
	// once, however this issue already exists for instance variables anyways.
	// Checking for that kind of none-sense may require another kind of test,
	// such as a test that calls the same method twice with the same parameters
	// and confirms it returns the same value, outputs the same thing...etc.
	private URLClassLoader classLoader;

	public TestCase(JsonObject testCaseDef, String classBeingTested) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, MalformedURLException {
		threadID = classBeingTested + "-" + UUID.randomUUID().toString();
		// All test cases can have these parameters
		Set<String> validParams = new HashSet<>(Arrays.asList(new String[]{
				COMMENT_KEY, POINTS_KEY, TIMEOUT_KEY, METHOD_NAME_KEY,
				PARAMETER_TYPES_KEY, RETURN_TYPE_KEY, PARAMETERS_KEY,
				CONSTRUCTOR_PARAMETERS_KEY, CONSTRUCTOR_PARAMETER_TYPES_KEY,
				INPUTS_KEY, MAX_PRINT_CALLS_KEY, TEST_CASE_CLASS_KEY}));
		// add the test case class specific valid parameters
		Set<String> addValid = validTestCaseParameters();
		if (addValid != null) validParams.addAll(addValid);
		Set<String> invalidParams = invalidTestCaseParameters();
		if (invalidParams != null) validParams.removeAll(invalidParams);
		validTestCaseParameters = validParams;

		Set<String> requiredParams = new HashSet<>();
		Set<String> addReq = requiredTestCaseParameters();
		if (addReq != null) requiredParams.addAll(addReq);
		Set<String> reqToRemove = testCaseParametersNotRequired();
		if (reqToRemove != null) requiredParams.removeAll(reqToRemove);
		requiredTestCaseParameters = requiredParams;

		verifyCaseParametersAreValid(testCaseDef);
		this.classBeingTested = classBeingTested;

		classLoader = new URLClassLoader(new URL[] {getClassPath(classBeingTested)}) {
			@Override
			public synchronized Class<?> loadClass(String name) throws ClassNotFoundException {
				Class<?> cls = findLoadedClass(name);
				if (cls != null) {
					//SYS_OUT.println("Class " + name + " already loaded");
					return cls;
				}
				if (classBeingTested.equals(name)) {
					//SYS_OUT.println("Loading class: " + name);
					return findClass(name);
				}
				//SYS_OUT.println("super loading class: " + name);
				return super.loadClass(name);
			}
		};

		// Confirming test case parameters are valid


		// comment
		try {
			this.comment = testCaseDef.has(COMMENT_KEY) ? testCaseDef.get(COMMENT_KEY).getAsString() : null;
		} catch (IllegalStateException err) {
			printInvalidFormatError(COMMENT_KEY);
			throw err;
		}

		// points
		try {
			this.points = testCaseDef.has(POINTS_KEY) ? testCaseDef.get(POINTS_KEY).getAsDouble() : 1.0;
		} catch (IllegalStateException err) {
			printInvalidFormatError(POINTS_KEY);
			throw err;
		}

		// timeout
		try {
			this.timeout = testCaseDef.has(TIMEOUT_KEY) ? testCaseDef.get(TIMEOUT_KEY).getAsLong() : DEFAULT_TIMEOUT;
		} catch (IllegalStateException err) {
			printInvalidFormatError(TIMEOUT_KEY);
			throw err;
		}
		
		// timeout
		try {
			this.maxPrintCalls = testCaseDef.has(MAX_PRINT_CALLS_KEY) ? testCaseDef.get(MAX_PRINT_CALLS_KEY).getAsLong() : DEFAULT_MAX_PRINT_CALLS;
		} catch (IllegalStateException err) {
			printInvalidFormatError(MAX_PRINT_CALLS_KEY);
			throw err;
		}

		// method name
		try {
			this.methodName = testCaseDef.has(METHOD_NAME_KEY) ? testCaseDef.get(METHOD_NAME_KEY).getAsString() : null;
		} catch (IllegalStateException err) {
			printInvalidFormatError(METHOD_NAME_KEY);
			throw err;
		}
		
		// must initialize result before processing parameters that might need to log errors
		this.result = new TestCaseResult(threadID, maxPrintCalls);

		// parameter types
		try {
			this.parameterTypes = testCaseDef.has(PARAMETER_TYPES_KEY) ? classArrayFromJsonArray(testCaseDef.get(PARAMETER_TYPES_KEY).getAsJsonArray()) : new Class<?>[0];
			this.fullParameterTypes = testCaseDef.has(PARAMETER_TYPES_KEY) ? stringArrayFromJsonArray(testCaseDef.get(PARAMETER_TYPES_KEY).getAsJsonArray()) : new String[0];
			cleanUpClassNames(fullParameterTypes);
		} catch (IllegalStateException err) {
			printInvalidFormatError(PARAMETER_TYPES_KEY);
			throw err;
		} catch (Throwable cnfe) {
			getResult().addException(cnfe);
			this.parameterTypes = null;
		}

		// constructor parameter types
		try {
			this.constructorParameterTypes = testCaseDef.has(CONSTRUCTOR_PARAMETER_TYPES_KEY) ? classArrayFromJsonArray(testCaseDef.get(CONSTRUCTOR_PARAMETER_TYPES_KEY).getAsJsonArray()) : new Class<?>[0];
			this.fullConstructorParameterTypes = testCaseDef.has(CONSTRUCTOR_PARAMETER_TYPES_KEY) ? stringArrayFromJsonArray(testCaseDef.get(CONSTRUCTOR_PARAMETER_TYPES_KEY).getAsJsonArray()) : new String[0];
			cleanUpClassNames(fullConstructorParameterTypes);
		} catch (IllegalStateException err) {
			printInvalidFormatError(CONSTRUCTOR_PARAMETER_TYPES_KEY);
			throw err;
		} catch (Throwable cnfe) {
			getResult().addException(cnfe);
			this.constructorParameterTypes = null;
		}

		// return type
		try {
			this.returnType = testCaseDef.has(RETURN_TYPE_KEY) ? classForName(testCaseDef.get(RETURN_TYPE_KEY).getAsString()) : null;
			this.fullReturnType = testCaseDef.has(RETURN_TYPE_KEY) ? cleanUpClassName(testCaseDef.get(RETURN_TYPE_KEY).getAsString()) : null;
		} catch (IllegalStateException err) {
			printInvalidFormatError(RETURN_TYPE_KEY);
			throw err;
		} catch (Throwable cnfe) {
			getResult().addException(cnfe);
			this.returnType = null;
		}

		// parameters
		try {
			this.parameters = testCaseDef.has(PARAMETERS_KEY) ? parameterArrayFromJsonArray(testCaseDef.get(PARAMETERS_KEY).getAsJsonArray(), parameterTypes, fullParameterTypes) : new Object[0];
			this.originalParameters = testCaseDef.has(PARAMETERS_KEY) ? parameterArrayFromJsonArray(testCaseDef.get(PARAMETERS_KEY).getAsJsonArray(), parameterTypes, fullParameterTypes) : new Object[0];
		} catch (IllegalStateException err) {
			printInvalidFormatError(PARAMETERS_KEY);
			throw err;
		} catch (Throwable cnfe) {
			getResult().addException(cnfe);
			this.parameters = null;
		}

		// constructors
		try {
			this.constructorParameters = testCaseDef.has(CONSTRUCTOR_PARAMETERS_KEY) ? parameterArrayFromJsonArray(testCaseDef.get(CONSTRUCTOR_PARAMETERS_KEY).getAsJsonArray(), constructorParameterTypes, fullConstructorParameterTypes) : new Object[0];
			this.originalConstructorParameters = testCaseDef.has(CONSTRUCTOR_PARAMETERS_KEY) ? parameterArrayFromJsonArray(testCaseDef.get(CONSTRUCTOR_PARAMETERS_KEY).getAsJsonArray(), constructorParameterTypes, fullConstructorParameterTypes) : new Object[0];
		} catch (IllegalStateException err) {
			printInvalidFormatError(CONSTRUCTOR_PARAMETERS_KEY);
			throw err;
		} catch (Throwable cnfe) {
			getResult().addException(cnfe);
			this.constructorParameters = null;
		}

		// inputs
		try {
			this.inputs = testCaseDef.has(INPUTS_KEY) ? testCaseDef.get(INPUTS_KEY).getAsString() : null;
		} catch (IllegalStateException err) {
			printInvalidFormatError(INPUTS_KEY);
			throw err;
		}
		
		this.runThread = null;
		this.nextCase = null;
		this.callback = null;
	}

	public boolean classIsCommonJavaLang(String className) {
		return JAVA_LANG_CLASSES.contains(className);
	}

	// subclasses should override and add their params to this
	protected Set<String> validTestCaseParameters() {
		return null;
	}
	
	// parameters that are invalid for a type of test
	protected Set<String> invalidTestCaseParameters() {
		return null;
	}

	// subclasses can define any additional required test cases
	protected Set<String> requiredTestCaseParameters() {
		return null;
	}

	// subclasses will define any test cases normally required that are not
	protected Set<String> testCaseParametersNotRequired() {
		return null;
	}
	
	protected boolean requiresMethodOrConstructorCall() {
		return false;
	}

	private boolean testCaseParameterIsValid(String parameter) {
		return validTestCaseParameters.contains(parameter);
	}

	private void verifyCaseParametersAreValid(JsonObject testCaseDef) {
		ArrayList<String> invalidParams = new ArrayList<>();
		for (Map.Entry<String, JsonElement> entry : testCaseDef.entrySet()) {
			if (!testCaseParameterIsValid(entry.getKey())) {
				invalidParams.add(entry.getKey());
			}
		}
		if (!invalidParams.isEmpty()) {
			throw new IllegalArgumentException("invalid parameters for test type " + getClass().getName() + ": " + invalidParams);
		}
		ArrayList<String> missingRequired = new ArrayList<>();
		for (String param : requiredTestCaseParameters) {
			if (!testCaseDef.has(param)) missingRequired.add(param);
		}
		if (!missingRequired.isEmpty()) {
			throw new IllegalArgumentException("missing required parameters for test type " + getClass().getName() + ": " + missingRequired);
		}
		if (requiresMethodOrConstructorCall() && !testCaseDef.has(METHOD_NAME_KEY) && !testCaseDef.has(CONSTRUCTOR_PARAMETERS_KEY)) {
			throw new IllegalArgumentException(getClass().getName() + " must either call a method or a constructor");
		}
	}

	protected static void printInvalidFormatError(String field) {
		SYS_ERR.println("Invalid format for field: " + field);
	}
	
	protected static void printMissingExpectedOutputFileError(String fileName) {
		SYS_ERR.println("Cannot find expected output file: " + fileName);
	}
	
	protected static void printExpectedOutputFileIOException(String fileName) {
		SYS_ERR.println("IOException when reading expected output file: " + fileName);
	}

	protected String getClassNameFromName(String name) {
		String[] nameSplit = getClassBeingTested().split("_");
		String namePrefix = "";
		for (int i = 0; i < nameSplit.length && i < 3; i++) {
			namePrefix += nameSplit[i] + "_";
		}
		return name.replaceAll("[(]this.prefix[)]", namePrefix).replaceAll("[(]this[)]", getClassBeingTested());
	}

	protected String cleanUpClassName(String name) {
		name = getClassNameFromName(name);
		if (classIsCommonJavaLang(name)) name = "java.lang." + name;
		String typeParam = typeParamForClassName(name);
		if (classIsCommonJavaLang(typeParam)) {
			typeParam = "java.lang." + typeParam;
			int startOfTypeParam = name.indexOf('<');
			if (startOfTypeParam != -1) {
				int endOfTypeParam = name.lastIndexOf('>');
				name = name.substring(0, startOfTypeParam + 1) + typeParam + name.substring(endOfTypeParam);
			}
		}
		String s = "";
		for (int j = 0; j < name.length(); j++) {
			if ((j == 0 || isClassNameSeparatorChar(name.charAt(j - 1))) && (j + 6 >= name.length() || isClassNameSeparatorChar(name.charAt(j + 6))) && j + 6 <= name.length() && name.substring(j, j + 6).equals("String")) {
				s += "java.lang.String";
				j += 5;
			} else if ((j == 0 || isClassNameSeparatorChar(name.charAt(j - 1))) && (j + 9 >= name.length() || isClassNameSeparatorChar(name.charAt(j + 9))) && j + 9 <= name.length() && name.substring(j, j + 9).equals("ArrayList")){
				s += "java.util.ArrayList";
				j += 8;
			} else if ((j == 0 || isClassNameSeparatorChar(name.charAt(j - 1))) && (j + 4 >= name.length() || isClassNameSeparatorChar(name.charAt(j + 4))) && j + 4 <= name.length() && name.substring(j, j + 4).equals("List")){
				s += "java.util.List";
				j += 3;
			} else {
				s += name.charAt(j);
			}
		}
		return s.replaceAll("\\s","");
	}

	protected void cleanUpClassNames(String[] classNames) {
		for (int i = 0; i < classNames.length; i++) {
			classNames[i] = cleanUpClassName(classNames[i]);
		}
	}

	private static boolean isClassNameSeparatorChar(char c) {
		return c == '[' || c == ']' || c == ' ' || c == ',' || c == '<' || c == '>' || c == '(' || c == ')';
	}

	private static URL getClassPath(String className) throws MalformedURLException {
		String classFileName = className.replace('.', '/') + ".class";
		String pathToClassFile = ClassLoader.getSystemClassLoader().getResource(classFileName).toExternalForm();    
		return new URL(pathToClassFile.substring(0, pathToClassFile.length() - classFileName.length()));
	}

	public Class<?> loadClass(String className) throws ClassNotFoundException {
		return classLoader.loadClass(className);
	}

	public Object getInstanceOfClass(String className, Class<?>[] constructorParameterTypes, Object[] constructorParameters) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		//SYS_OUT.println("getting instance of class: " + className);
		Class<?> cls = null;
		try {
			cls = loadClass(className);
		} catch (ClassNotFoundException cnfe) {
			throw new ClassNotFoundException("Cannot resolve constructor for class " + className + " because a required class does not exist.");
		}
		Object inst = null;
		Constructor<?> constr = null;
		for (Class<?> paramCls : constructorParameterTypes) {
			if (paramCls == null) {
				// one of the classes in the parameter types does not exist
				// (perhaps it was a class name given by (this.prefix)MyClass
				// but was named incorrectly, so this test should fail
				throw new ClassNotFoundException("Cannot resolve constructor parameters " + Arrays.toString(constructorParameters) + " for class " + className + " because a required class does not exist.");
			}
		}
		if (constructorParameterTypes == null || constructorParameterTypes.length == 0) {
			constr = cls.getDeclaredConstructor();
			constr.setAccessible(true);
			inst = constr.newInstance();
		} else {
			constr = cls.getDeclaredConstructor(constructorParameterTypes);
			constr.setAccessible(true);
			inst = constr.newInstance(constructorParameters);
		}
		return inst;
	}

	public String getDeclaration(Method m) {
		String declaration = "";
		declaration += Modifier.toString(m.getModifiers()) + " " + m.getName() + "(";
		Type[] paramTypes = m.getGenericParameterTypes();
		for (Type type : paramTypes) {
			declaration += type.getTypeName() + ", ";
		}
		if (paramTypes.length > 0) declaration = declaration.substring(0, declaration.lastIndexOf(','));
		declaration += "), ";
		return declaration;
	}

	public Method getMethodWithCorrectSignature() {
		Method matchingMethod = null;
		try {
			//SYS_OUT.println("getting method: " + getMethodName());
			Class<?> cls = loadClass(getClassBeingTested());
			String expectedParamsStr = Arrays.toString(getFullParameterTypes());
			Method[] methods = cls.getDeclaredMethods();
			String methodSignatures = "Found Declared Method Signatures: ";
			for (Method m : methods) {
				Type[] paramTypes = m.getGenericParameterTypes();
				methodSignatures += getDeclaration(m);
				if (m.getName().equals(getMethodName())) {
					// some parameters, such as an untyped ArrayList will have "class " preceding their name
					String paramsStr = Arrays.toString(paramTypes).replaceAll("class ", "");
					String[] paramsStrArr = paramsStr.substring(1, paramsStr.length() - 1).split(", ");
					for (int i = 0; i < paramsStrArr.length; i++) {
						if (paramsStrArr[i].startsWith("[")) {
							paramsStrArr[i] = paramTypes[i].getTypeName();
						}
					}
					paramsStr = Arrays.toString(paramsStrArr);
					if (!paramsStr.equals(expectedParamsStr)) {
						getResult().addConditionalNote(paramsStr + " does not match " + expectedParamsStr);
					} else {
						matchingMethod = m;
					}
				}
			}
			if (methods.length > 0) methodSignatures = methodSignatures.substring(0, methodSignatures.lastIndexOf(','));
			getResult().addNote(methodSignatures);

		} catch (Exception e) {
			getResult().addException(e);
		}
		if (matchingMethod == null) {
			getResult().addConditionalNote("Method not found!");
		}
		return matchingMethod;
	}

	public Field getField(String fieldName) {
		return getField(fieldName, null, new String[0], new String[0]);
	}

	public Field getField(String fieldName, String expectedFieldType, String[] expectedFieldModifiers, String[] forbiddenFieldModifiers) {
		Field fieldFound = null;
		try {
			//SYS_OUT.println("Getting field: " + fieldName + " with type: " + expectedFieldType + ", modifiers: " + expectedFieldModifiers + " and not modifiers: " + forbiddenFieldModifiers);
			Class<?> cls = loadClass(getClassBeingTested());
			Field[] fields = cls.getDeclaredFields();
			String fieldsDiscovered = "Found Declared Fields:";
			for (Field field : fields) {
				String fieldStr = field.toGenericString().replaceAll("\\s*" + getClassBeingTested() + ".", " ").trim();
				fieldsDiscovered += fieldStr + ", ";
			}
			for (Field field : fields) {
				String fieldStr = field.toGenericString().replaceAll("\\s*" + getClassBeingTested() + ".", " ").trim();
				if (fieldName == null || fieldName.equals(field.getName())) {
					boolean hasCorrectProps = true;
					if (expectedFieldType != null && !(" " + fieldStr + " ").contains(" " + expectedFieldType + " ")) {
						getResult().addNote(fieldStr + " does not have expected type " + expectedFieldType);
						hasCorrectProps = false;
					}
					List<String> missingModifiers = StringUtil.wordsInArrayNotContainedInString(fieldStr, expectedFieldModifiers);
					if (!missingModifiers.isEmpty()) {
						getResult().addNote(field.toGenericString().replaceAll(getClassBeingTested() + ".", "") + " does not have expected modifiers: " + missingModifiers);
						hasCorrectProps = false;
					}

					for (String mod : forbiddenFieldModifiers) {
						if ((" " + fieldStr + " ").contains(" " + mod + " ")) {
							getResult().addNote(field.toGenericString().replaceAll(getClassBeingTested() + ".", "") + " has forbidden modifier: " + mod);
							hasCorrectProps = false;
						}
					}
					if (hasCorrectProps) {
						fieldFound = field;
						break;
					}
				}
			}
			if (fields.length > 0) fieldsDiscovered = fieldsDiscovered.substring(0, fieldsDiscovered.lastIndexOf(','));
			getResult().addNote(fieldsDiscovered);
		} catch (Exception e) {
			getResult().addException(e);
		}
		if (fieldFound == null) {
			getResult().addConditionalNote("Could not find a field" + (fieldName == null ? " " : " named " + fieldName + " ") + "meeting all requirements!");
		}
		return fieldFound;
	}

	public static Map<String, String> mapStrStrFromJsonObject(JsonObject json) {
		Set<Map.Entry<String,JsonElement>> entrySet = json.entrySet();
		Map<String, String> map = new LinkedHashMap<String, String>();
		for (Map.Entry<String,JsonElement> entry : entrySet) {
			String key = entry.getKey();
			String val = entry.getValue().getAsString();
			map.put(key, val);
		}
		return map;
	}

	public static String typeParamForClassName(String className) {
		int startOfTypeParam = className.indexOf('<');
		if (startOfTypeParam != -1) {
			int endOfTypeParam = className.lastIndexOf('>');
			return className.substring(startOfTypeParam + 1, endOfTypeParam).trim();
		}
		return null;
	}

	public static ArrayList<Integer> integerArrayListFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		ArrayList<Integer> arr = new ArrayList<Integer>();
		for (JsonElement el : jsonArr) {
			arr.add(el.getAsInt());
		}
		return arr;
	}

	public static ArrayList<Double> doubleArrayListFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		ArrayList<Double> arr = new ArrayList<Double>();
		for (JsonElement el : jsonArr) {
			arr.add(el.getAsDouble());
		}
		return arr;
	}

	public static ArrayList<Character> characterArrayListFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		ArrayList<Character> arr = new ArrayList<Character>();
		for (JsonElement el : jsonArr) {
			arr.add(el.getAsCharacter());
		}
		return arr;
	}

	public static ArrayList<Boolean> booleanArrayListFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		ArrayList<Boolean> arr = new ArrayList<Boolean>();
		for (JsonElement el : jsonArr) {
			arr.add(el.getAsBoolean());
		}
		return arr;
	}

	public static ArrayList<Long> longArrayListFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		ArrayList<Long> arr = new ArrayList<Long>();
		for (JsonElement el : jsonArr) {
			arr.add(el.getAsLong());
		}
		return arr;
	}

	public static ArrayList<Short> shortArrayListFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		ArrayList<Short> arr = new ArrayList<Short>();
		for (JsonElement el : jsonArr) {
			arr.add(el.getAsShort());
		}
		return arr;
	}

	public static ArrayList<Float> floatArrayListFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		ArrayList<Float> arr = new ArrayList<Float>();
		for (JsonElement el : jsonArr) {
			arr.add(el.getAsFloat());
		}
		return arr;
	}

	public static ArrayList<Byte> byteArrayListFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		ArrayList<Byte> arr = new ArrayList<Byte>();
		for (JsonElement el : jsonArr) {
			arr.add(el.getAsByte());
		}
		return arr;
	}

	public static ArrayList<String> stringArrayListFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		ArrayList<String> arr = new ArrayList<String>();
		for (JsonElement el : jsonArr) {
			arr.add(el.getAsString());
		}
		return arr;
	}

	public static int[] intArrayFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		int[] arr = new int[jsonArr.size()];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = jsonArr.get(i).getAsInt();
		}
		return arr;
	}

	public static int[][] twoDIntArrayFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		int rows = jsonArr.size();
		int cols = 0;
		if (jsonArr.size() > 0) {
			cols = jsonArr.get(0).getAsJsonArray().size();
		}
		int[][] arr = new int[rows][cols];
		for (int i = 0; i < rows; i++) {
			arr[i] = intArrayFromJsonArray(jsonArr.get(i).getAsJsonArray());
		}
		return arr;
	}

	public static double[] doubleArrayFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		double[] arr = new double[jsonArr.size()];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = jsonArr.get(i).getAsDouble();
		}
		return arr;
	}

	public static double[][] twoDDoubleArrayFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		int rows = jsonArr.size();
		int cols = 0;
		if (jsonArr.size() > 0) {
			cols = jsonArr.get(0).getAsJsonArray().size();
		}
		double[][] arr = new double[rows][cols];
		for (int i = 0; i < rows; i++) {
			arr[i] = doubleArrayFromJsonArray(jsonArr.get(i).getAsJsonArray());
		}
		return arr;
	}


	public static char[] charArrayFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		char[] arr = new char[jsonArr.size()];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = jsonArr.get(i).getAsCharacter();
		}
		return arr;
	}

	public static char[][] twoDCharArrayFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		int rows = jsonArr.size();
		int cols = 0;
		if (jsonArr.size() > 0) {
			cols = jsonArr.get(0).getAsJsonArray().size();
		}
		char[][] arr = new char[rows][cols];
		for (int i = 0; i < rows; i++) {
			arr[i] = charArrayFromJsonArray(jsonArr.get(i).getAsJsonArray());
		}
		return arr;
	}

	public static boolean[] booleanArrayFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		boolean[] arr = new boolean[jsonArr.size()];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = jsonArr.get(i).getAsBoolean();
		}
		return arr;
	}

	public static boolean[][] twoDBooleanArrayFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		int rows = jsonArr.size();
		int cols = 0;
		if (jsonArr.size() > 0) {
			cols = jsonArr.get(0).getAsJsonArray().size();
		}
		boolean[][] arr = new boolean[rows][cols];
		for (int i = 0; i < rows; i++) {
			arr[i] = booleanArrayFromJsonArray(jsonArr.get(i).getAsJsonArray());
		}
		return arr;
	}

	public static long[] longArrayFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		long[] arr = new long[jsonArr.size()];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = jsonArr.get(i).getAsLong();
		}
		return arr;
	}

	public static long[][] twoDLongArrayFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		int rows = jsonArr.size();
		int cols = 0;
		if (jsonArr.size() > 0) {
			cols = jsonArr.get(0).getAsJsonArray().size();
		}
		long[][] arr = new long[rows][cols];
		for (int i = 0; i < rows; i++) {
			arr[i] = longArrayFromJsonArray(jsonArr.get(i).getAsJsonArray());
		}
		return arr;
	}

	public static short[] shortArrayFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		short[] arr = new short[jsonArr.size()];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = jsonArr.get(i).getAsShort();
		}
		return arr;
	}

	public static short[][] twoDShortArrayFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		int rows = jsonArr.size();
		int cols = 0;
		if (jsonArr.size() > 0) {
			cols = jsonArr.get(0).getAsJsonArray().size();
		}
		short[][] arr = new short[rows][cols];
		for (int i = 0; i < rows; i++) {
			arr[i] = shortArrayFromJsonArray(jsonArr.get(i).getAsJsonArray());
		}
		return arr;
	}

	public static float[] floatArrayFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		float[] arr = new float[jsonArr.size()];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = jsonArr.get(i).getAsFloat();
		}
		return arr;
	}

	public static float[][] twoDFloatArrayFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		int rows = jsonArr.size();
		int cols = 0;
		if (jsonArr.size() > 0) {
			cols = jsonArr.get(0).getAsJsonArray().size();
		}
		float[][] arr = new float[rows][cols];
		for (int i = 0; i < rows; i++) {
			arr[i] = floatArrayFromJsonArray(jsonArr.get(i).getAsJsonArray());
		}
		return arr;
	}

	public static byte[] byteArrayFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		byte[] arr = new byte[jsonArr.size()];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = jsonArr.get(i).getAsByte();
		}
		return arr;
	}

	public static byte[][] twoDByteArrayFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		int rows = jsonArr.size();
		int cols = 0;
		if (jsonArr.size() > 0) {
			cols = jsonArr.get(0).getAsJsonArray().size();
		}
		byte[][] arr = new byte[rows][cols];
		for (int i = 0; i < rows; i++) {
			arr[i] = byteArrayFromJsonArray(jsonArr.get(i).getAsJsonArray());
		}
		return arr;
	}

	public static String[] stringArrayFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		String[] arr = new String[jsonArr.size()];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = jsonArr.get(i).getAsString();
		}
		return arr;
	}

	public static String[][] twoDStringArrayFromJsonArray(JsonArray jsonArr) {
		if (jsonArr == null) return null;
		int rows = jsonArr.size();
		int cols = 0;
		if (jsonArr.size() > 0) {
			cols = jsonArr.get(0).getAsJsonArray().size();
		}
		String[][] arr = new String[rows][cols];
		for (int i = 0; i < rows; i++) {
			arr[i] = stringArrayFromJsonArray(jsonArr.get(i).getAsJsonArray());
		}
		return arr;
	}

	public Class<?>[] classArrayFromJsonArray(JsonArray jsonArr) throws ClassNotFoundException {
		Class<?>[] arr = new Class<?>[jsonArr == null ? 0 : jsonArr.size()];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = classForName(jsonArr.get(i).getAsString());
		}
		return arr;
	}

	public Class<?> classForName(String className) throws ClassNotFoundException {
		int indexOfTypeParam = className.indexOf('<');
		if (indexOfTypeParam != -1) {
			className = className.substring(0, indexOfTypeParam).trim();
		}
		className = cleanUpClassName(className);


		//if (className.contains("(this") || className.contains("(this")) return null;
		// int
		if (className.equals("int")) return int.class;
		else if (className.equals("int[]")) return int[].class;
		else if (className.equals("int[][]")) return int[][].class;

		// double
		else if (className.equals("double")) return double.class;
		else if (className.equals("double[]")) return double[].class;
		else if (className.equals("double[][]")) return double[][].class;

		// boolean
		else if (className.equals("boolean")) return boolean.class;
		else if (className.equals("boolean[]")) return boolean[].class;
		else if (className.equals("boolean[][]")) return boolean[][].class;

		// char
		else if (className.equals("char")) return char.class;
		else if (className.equals("char[]")) return char[].class;
		else if (className.equals("char[][]")) return char[][].class;

		// long
		else if (className.equals("long")) return long.class;
		else if (className.equals("long[]")) return long[].class;
		else if (className.equals("long[][]")) return long[][].class;

		// short
		else if (className.equals("short")) return short.class;
		else if (className.equals("short[]")) return short[].class;
		else if (className.equals("short[][]")) return short[][].class;

		// float
		else if (className.equals("float")) return float.class;
		else if (className.equals("float[]")) return float[].class;
		else if (className.equals("float[][]")) return float[][].class;

		// byte
		else if (className.equals("byte")) return byte.class;
		else if (className.equals("byte[]")) return byte[].class;
		else if (className.equals("byte[][]")) return byte[][].class;

		// String
		else if (className.equals("java.lang.String[]")) return String[].class;
		else if (className.equals("java.lang.String[][]")) return String[][].class;

		// void
		else if (className.equals("void")) return void.class;

		// Classes
		else {
			// Note: This must be the full name (i.e. "java.util.ArrayList")
			// except for the special cases in JAVA_LANG_CLASSES that will be conveniently supported by adding the java.lang. prefix:
			if (JAVA_LANG_CLASSES.contains(className)) className = "java.lang." + className;
			//SYS_OUT.println("loading class: " + className);
			return loadClass(className);
		}
	}

	public Object[] getObjectAsArray(Object obj) {
		if (!obj.getClass().isArray()) throw new IllegalArgumentException(obj.getClass().getName() + " is not an array");
		else if (Object[].class.isAssignableFrom(obj.getClass())) return (Object[]) obj;
		else if (int[].class.isAssignableFrom(obj.getClass())) return getObjectArrayFromArray((int[]) obj);
		else if (int[][].class.isAssignableFrom(obj.getClass())) return getObjectArrayFromArray((int[][]) obj);
		else if (short[].class.isAssignableFrom(obj.getClass())) return getObjectArrayFromArray((short[]) obj);
		else if (short[][].class.isAssignableFrom(obj.getClass())) return getObjectArrayFromArray((short[][]) obj);
		else if (long[].class.isAssignableFrom(obj.getClass())) return getObjectArrayFromArray((long[]) obj);
		else if (long[][].class.isAssignableFrom(obj.getClass())) return getObjectArrayFromArray((long[][]) obj);
		else if (char[].class.isAssignableFrom(obj.getClass())) return getObjectArrayFromArray((char[]) obj);
		else if (char[][].class.isAssignableFrom(obj.getClass())) return getObjectArrayFromArray((char[][]) obj);
		else if (byte[].class.isAssignableFrom(obj.getClass())) return getObjectArrayFromArray((byte[]) obj);
		else if (byte[][].class.isAssignableFrom(obj.getClass())) return getObjectArrayFromArray((byte[][]) obj);
		else if (boolean[].class.isAssignableFrom(obj.getClass())) return getObjectArrayFromArray((boolean[]) obj);
		else if (boolean[][].class.isAssignableFrom(obj.getClass())) return getObjectArrayFromArray((boolean[][]) obj);

		else if (double[].class.isAssignableFrom(obj.getClass())) return getObjectArrayFromArray((double[]) obj);
		else if (double[][].class.isAssignableFrom(obj.getClass())) return getObjectArrayFromArray((double[][]) obj);

		else if (float[].class.isAssignableFrom(obj.getClass())) return getObjectArrayFromArray((float[]) obj);
		else if (float[][].class.isAssignableFrom(obj.getClass())) return getObjectArrayFromArray((float[][]) obj);
		return null; // will happen if a 3D array is passed
	}

	public Object[] getObjectArrayFromArray(Object[] array) {
		return array;
	}

	public Object[] getObjectArrayFromArray(int[] array) {
		Object[] arr = new Object[array.length];
		for (int i = 0; i < array.length; i++) {
			arr[i] = array[i];
		}
		return arr;
	}

	public Object[] getObjectArrayFromArray(long[] array) {
		Object[] arr = new Object[array.length];
		for (int i = 0; i < array.length; i++) {
			arr[i] = array[i];
		}
		return arr;
	}

	public Object[] getObjectArrayFromArray(short[] array) {
		Object[] arr = new Object[array.length];
		for (int i = 0; i < array.length; i++) {
			arr[i] = array[i];
		}
		return arr;
	}

	public Object[] getObjectArrayFromArray(char[] array) {
		Object[] arr = new Object[array.length];
		for (int i = 0; i < array.length; i++) {
			arr[i] = array[i];
		}
		return arr;
	}

	public Object[] getObjectArrayFromArray(byte[] array) {
		Object[] arr = new Object[array.length];
		for (int i = 0; i < array.length; i++) {
			arr[i] = array[i];
		}
		return arr;
	}

	public Object[] getObjectArrayFromArray(double[] array) {
		Object[] arr = new Object[array.length];
		for (int i = 0; i < array.length; i++) {
			arr[i] = array[i];
		}
		return arr;
	}

	public Object[] getObjectArrayFromArray(float[] array) {
		Object[] arr = new Object[array.length];
		for (int i = 0; i < array.length; i++) {
			arr[i] = array[i];
		}
		return arr;
	}

	public Object[] getObjectArrayFromArray(boolean[] array) {
		Object[] arr = new Object[array.length];
		for (int i = 0; i < array.length; i++) {
			arr[i] = array[i];
		}
		return arr;
	}
	
	private Map<Object, Integer> getMapFromArray(Object[] arr) {
		Map<Object, Integer> map = new HashMap<Object, Integer>();
		for (Object obj : arr) {
			if (obj.getClass().isArray()) {
				obj = getMapFromArray(getObjectAsArray(obj));
			} else if (Iterable.class.isAssignableFrom(obj.getClass())) {
				obj = getMapFromIterable((Iterable<?>)obj);
			}
			Integer count = map.get(obj);
			if (count == null) {
				count = 0;
			}
			map.put(obj, count + 1);
		}
		return map;
	}
	
	private Map<Object, Integer> getMapFromIterable(Iterable<?> arr) {
		Map<Object, Integer> map = new HashMap<>();
		for (Object obj : arr) {
			if (Iterable.class.isAssignableFrom(obj.getClass())) {
				obj = getMapFromIterable((Iterable<?>)obj);
			} else if (obj.getClass().isArray()) {
				obj = getMapFromArray(getObjectAsArray(obj));
			}
			Integer count = map.get(obj);
			if (count == null) {
				count = 0;
			}
			map.put(obj, count + 1);
		}
		return map;
	}
	
	protected boolean resultMatches(Object expectedReturnValue,  Object res, boolean orderMatters) {
		// if order does matter or if the return type is not an array or array list type, just do the usual check
		if (orderMatters || !Iterable.class.isAssignableFrom(getReturnType()) && !getReturnType().isArray()) {
			return getReturnType().isArray() ? (Arrays.deepToString(getObjectAsArray(expectedReturnValue)).equals(Arrays.deepToString(getObjectAsArray(res)))) : (res.toString().equals(expectedReturnValue.toString()));
		} else { // order doesn't matter, so just see if they have the same items.
			Map<Object, Integer> expectedMap;
			Map<Object, Integer> resultMap;
			if (getReturnType().isArray()) {
				Object[] expectedArr = getObjectAsArray(expectedReturnValue);
				Object[] resArr = getObjectAsArray(res);
				expectedMap = getMapFromArray(expectedArr);
				resultMap = getMapFromArray(resArr);
			} else {
				expectedMap = getMapFromIterable((Iterable<?>) expectedReturnValue);
				resultMap = getMapFromIterable((Iterable<?>) res);
			}
			Set<Object> expectedKeySet = expectedMap.keySet();
			Set<Object> resultKeySet = resultMap.keySet();
			if (!expectedKeySet.equals(resultKeySet)) return false;
			for (Object key : expectedKeySet) {
				if (!expectedMap.get(key).equals(resultMap.get(key))) return false;
			}
			return true;
		}
	}

	public Object getInstanceOfClass(String className) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		return getInstanceOfClass(className, constructorParameterTypes, constructorParameters);
	}

	@SuppressWarnings("rawtypes")
	public Object getObjectFromJsonElement(JsonElement jsonEl, Class<?> cls, String fullType) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		// int
		if (cls.equals(int.class)) return jsonEl.getAsInt();
		else if (cls.equals(int[].class)) return intArrayFromJsonArray(jsonEl.getAsJsonArray());
		else if (cls.equals(int[][].class)) return twoDIntArrayFromJsonArray(jsonEl.getAsJsonArray());

		// double
		else if (cls.equals(double.class)) return jsonEl.getAsDouble();
		else if (cls.equals(double[].class)) return doubleArrayFromJsonArray(jsonEl.getAsJsonArray());
		else if (cls.equals(double[][].class)) return twoDDoubleArrayFromJsonArray(jsonEl.getAsJsonArray());

		// char
		else if (cls.equals(char.class)) return jsonEl.getAsCharacter();
		else if (cls.equals(char[].class)) return charArrayFromJsonArray(jsonEl.getAsJsonArray());
		else if (cls.equals(char[][].class)) return twoDCharArrayFromJsonArray(jsonEl.getAsJsonArray());

		// boolean
		else if (cls.equals(boolean.class)) return jsonEl.getAsBoolean();
		else if (cls.equals(boolean[].class)) return booleanArrayFromJsonArray(jsonEl.getAsJsonArray());
		else if (cls.equals(boolean[][].class)) return twoDBooleanArrayFromJsonArray(jsonEl.getAsJsonArray());

		// long
		else if (cls.equals(long.class)) return jsonEl.getAsLong();
		else if (cls.equals(long[].class)) return longArrayFromJsonArray(jsonEl.getAsJsonArray());
		else if (cls.equals(long[][].class)) return twoDLongArrayFromJsonArray(jsonEl.getAsJsonArray());

		// short
		else if (cls.equals(short.class)) return jsonEl.getAsShort();
		else if (cls.equals(short[].class)) return shortArrayFromJsonArray(jsonEl.getAsJsonArray());
		else if (cls.equals(short[][].class)) return twoDShortArrayFromJsonArray(jsonEl.getAsJsonArray());

		// float
		else if (cls.equals(float.class)) return jsonEl.getAsFloat();
		else if (cls.equals(float[].class)) return floatArrayFromJsonArray(jsonEl.getAsJsonArray());
		else if (cls.equals(float[][].class)) return twoDFloatArrayFromJsonArray(jsonEl.getAsJsonArray());

		// byte
		else if (cls.equals(byte.class)) return jsonEl.getAsByte();
		else if (cls.equals(byte[].class)) return byteArrayFromJsonArray(jsonEl.getAsJsonArray());
		else if (cls.equals(byte[][].class)) return twoDByteArrayFromJsonArray(jsonEl.getAsJsonArray());

		// String
		else if (cls.equals(String.class)) return jsonEl.getAsString();
		else if (cls.equals(String[].class)) return stringArrayFromJsonArray(jsonEl.getAsJsonArray());
		else if (cls.equals(String[][].class)) return twoDStringArrayFromJsonArray(jsonEl.getAsJsonArray());

		else if (cls.isAssignableFrom(ArrayList.class)) {
			String typeParam = typeParamForClassName(fullType);
			if (classIsCommonJavaLang(typeParam)) typeParam = "java.lang." + typeParam;
			if (typeParam.equals("java.lang.Integer")) return integerArrayListFromJsonArray(jsonEl.getAsJsonArray());
			else if (typeParam.equals("java.lang.Double")) return doubleArrayListFromJsonArray(jsonEl.getAsJsonArray());
			else if (typeParam.equals("java.lang.Character")) return characterArrayListFromJsonArray(jsonEl.getAsJsonArray());
			else if (typeParam.equals("java.lang.Boolean")) return booleanArrayListFromJsonArray(jsonEl.getAsJsonArray());
			else if (typeParam.equals("java.lang.Long")) return longArrayListFromJsonArray(jsonEl.getAsJsonArray());
			else if (typeParam.equals("java.lang.Short")) return shortArrayListFromJsonArray(jsonEl.getAsJsonArray());
			else if (typeParam.equals("java.lang.Float")) return floatArrayListFromJsonArray(jsonEl.getAsJsonArray());
			else if (typeParam.equals("java.lang.Byte")) return byteArrayListFromJsonArray(jsonEl.getAsJsonArray());
			else if (typeParam.equals("java.lang.String")) return stringArrayListFromJsonArray(jsonEl.getAsJsonArray());
			else if (typeParam.equals("java.lang.Comparable")) {
				try {
					return new ArrayList<Comparable>(stringArrayListFromJsonArray(jsonEl.getAsJsonArray()));
				} catch (Exception ccestr) {
					try {
						return new ArrayList<Comparable>(integerArrayListFromJsonArray(jsonEl.getAsJsonArray()));
					} catch (Exception cceint) {
						try {
							return new ArrayList<Comparable>(doubleArrayListFromJsonArray(jsonEl.getAsJsonArray()));
						} catch (Exception ccedou) {
							try {
								return new ArrayList<Comparable>(characterArrayListFromJsonArray(jsonEl.getAsJsonArray()));
							} catch (Exception ccecha) {
								try {
									return new ArrayList<Comparable>(booleanArrayListFromJsonArray(jsonEl.getAsJsonArray()));
								} catch (Exception cceboo) {
									try {
										return new ArrayList<Comparable>(longArrayListFromJsonArray(jsonEl.getAsJsonArray()));
									} catch (Exception ccelon) {
										try {
											return new ArrayList<Comparable>(shortArrayListFromJsonArray(jsonEl.getAsJsonArray()));
										} catch (Exception ccesho) {
											try {
												return new ArrayList<Comparable>(byteArrayListFromJsonArray(jsonEl.getAsJsonArray()));
											} catch (Exception ccebyt) {
												try {
													return new ArrayList<Comparable>(floatArrayListFromJsonArray(jsonEl.getAsJsonArray()));
												} catch (Exception cceflo) {
													ArrayList objList = objectArrayListFromJsonArray(jsonEl.getAsJsonArray(), fullType);
													ArrayList<Comparable> compList = new ArrayList<>();
													for (int i = 0; i < objList.size(); i++) {
														// assume each object will be assignable to Comparable - otherwise something is wrong with the test
														compList.add((Comparable) objList.get(i));
													}
													return compList;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			return objectArrayListFromJsonArray(jsonEl.getAsJsonArray(), fullType);
		} else if (jsonEl.isJsonObject()) {
			return getObjectFromJsonObject(jsonEl.getAsJsonObject(), fullType);
		}
		return null;
	}

	public Object getObjectFromJsonObject(JsonObject jsonObj, String className) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Class<?>[] paramTypes = jsonObj.has(PARAMETER_TYPES_KEY) ? classArrayFromJsonArray(jsonObj.get(PARAMETER_TYPES_KEY).getAsJsonArray()) : new Class<?>[0];
		String[] fullParamTypes = jsonObj.has(PARAMETER_TYPES_KEY) ? stringArrayFromJsonArray(jsonObj.get(PARAMETER_TYPES_KEY).getAsJsonArray()) : new String[0];
		cleanUpClassNames(fullParamTypes);
		Object[] params = jsonObj.has(PARAMETERS_KEY) ? parameterArrayFromJsonArray(jsonObj.get(PARAMETERS_KEY).getAsJsonArray(), paramTypes, fullParamTypes) : new Object[0];
		if (jsonObj.has(CLASS_NAME)) className = getClassNameFromName(jsonObj.get(CLASS_NAME).getAsString());
		return getInstanceOfClass(className, paramTypes, params);
	}

	public Object[] parameterArrayFromJsonArray(JsonArray jsonArr, Class<?>[] paramTypes, String[] fullParamTypes) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Object[] arr = new Object[jsonArr == null ? 0 : jsonArr.size()];
		if (paramTypes.length != arr.length) {
			throw new IllegalStateException("Parameters: " + jsonArr.toString() + " (length: " + jsonArr.size() + ")" + " and parameter types " + Arrays.toString(paramTypes) + " (length: " + paramTypes.length + ")" + " do not match in length");
		}
		for (int i = 0; i < arr.length; i++) {
			arr[i] = getObjectFromJsonElement(jsonArr.get(i), paramTypes[i], fullParamTypes[i]);
		}
		return arr;
	}

	public ArrayList<Object> objectArrayListFromJsonArray(JsonArray jsonArr, String fullType) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		ArrayList<Object> arr = new ArrayList<>();
		String typeClassName = "Object";
		int indexOfTypeParam = fullType.indexOf('<');
		if (indexOfTypeParam != -1) {
			typeClassName = fullType.substring(indexOfTypeParam + 1, fullType.indexOf('>')).trim();
		}
		for (JsonElement el : jsonArr) {
			arr.add(getObjectFromJsonObject(el.getAsJsonObject(), typeClassName));
		}
		return arr;
	}

	public Object[] getParameters() {
		return parameters;
	}

	public void setParameters(Object[] parameters) {
		this.parameters = parameters;
	}

	public Object[] getOriginalParameters() {
		return originalParameters;
	}

	public void setOriginalParameters(Object[] originalParameters) {
		this.originalParameters = originalParameters;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public Class<?>[] getParameterTypes() {
		return parameterTypes;
	}

	public void setParameterTypes(Class<?>[] parameterTypes) {
		this.parameterTypes = parameterTypes;
	}

	public String[] getFullParameterTypes() {
		return fullParameterTypes;
	}

	public void setFullParameterTypes(String[] fullParameterTypes) {
		this.fullParameterTypes = fullParameterTypes;
	}

	public Class<?> getReturnType() {
		return returnType;
	}

	public void setReturnType(Class<?> returnType) {
		this.returnType = returnType;
	}

	public String getInputs() {
		return inputs;
	}

	public void setInputs(String inputs) {
		this.inputs = inputs;
	}

	public Class<?>[] getConstructorParameterTypes() {
		return constructorParameterTypes;
	}

	public void setConstructorParameterTypes(Class<?>[] constructorParameterTypes) {
		this.constructorParameterTypes = constructorParameterTypes;
	}

	public String[] getFullConstructorParameterTypes() {
		return fullConstructorParameterTypes;
	}

	public void setFullConstructorParameterTypes(String[] fullConstructorParameterTypes) {
		this.fullConstructorParameterTypes = fullConstructorParameterTypes;
	}

	public Object[] getConstructorParameters() {
		return constructorParameters;
	}

	public void setConstructorParameters(Object[] constructorParameters) {
		this.constructorParameters = constructorParameters;
	}

	public Object[] getOriginalConstructorParameters() {
		return originalConstructorParameters;
	}

	public void setOriginalConstructorParameters(Object[] originalConstructorParameters) {
		this.originalConstructorParameters = originalConstructorParameters;
	}

	public String getFullReturnType() {
		return fullReturnType;
	}

	public void setFullReturnType(String fullReturnType) {
		this.fullReturnType = fullReturnType;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public double getPoints() {
		return points;
	}

	public void setPoints(double points) {
		this.points = points;
	}

	public TestCaseResult getResult() {
		return result;
	}

	public void setResult(TestCaseResult result) {
		this.result = result;
	}

	public TestCase getNextCase() {
		return nextCase;
	}

	public void setNextCase(TestCase nextCase) {
		this.nextCase = nextCase;
	}

	public Runnable getCallback() {
		return callback;
	}

	public void setCallback(Runnable callback) {
		this.callback = callback;
	}

	public long getTimeout() {
		return timeout;
	}

	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	public String getClassBeingTested() {
		return classBeingTested;
	}

	public void setClassBeingTested(String classBeingTested) {
		this.classBeingTested = classBeingTested;
	}

	public String getTestCaseTitle() {
		return getClass().getSimpleName();
	}

	public LinkedHashMap<String, String> propertyDescriptions() {
		LinkedHashMap<String, String> props = new LinkedHashMap<>();
		if (comment != null && comment.trim().length() > 0) props.put("Comment", comment);
		props.put("Points", "" + (Math.round(points * 10000) / 10000.0)); // rounded to 4 decimal places
		props.put("Constructor Parameter Types", Arrays.toString(fullConstructorParameterTypes));
		String constructorParamDescrip = null;
		try {
			constructorParamDescrip = Arrays.toString(originalConstructorParameters);
		} catch (Throwable err) {
			constructorParamDescrip = "Error: Cannot print due to the following exception:\n" + err.toString();
		}
		props.put("Constructor Parameters", constructorParamDescrip);
		if (methodName != null) {
			props.put("Method Name", methodName);
			props.put("Parameter Types", Arrays.toString(fullParameterTypes));
			String paramDescrip = null;
			try {
				paramDescrip = Arrays.deepToString(originalParameters);
			} catch (Throwable err) {
				paramDescrip = "Error: Cannot print due to the following exception:\n" + err.toString();
			}
			props.put("Parameters", paramDescrip);
			if (returnType != null) {
				props.put("Return Type", fullReturnType);
			}
		}
		if (inputs != null) props.put("Inputs", inputs.replaceAll("\n", "\\\\n"));
		return props;
	}

	public void printPropertyDescriptions(PrintStream out) {
		LinkedHashMap<String, String> props = propertyDescriptions();
		int longestKey = longestKeyInMap(props);
		for (Map.Entry<String, String> entry : props.entrySet()) {
			if (entry.getKey().endsWith(System.lineSeparator())) {
				out.print(entry.getKey());
				out.println(entry.getValue());
			} else {
				out.printf("%-" + longestKey + "s %s", entry.getKey(), entry.getValue());
				out.println();
			}
		}
	}

	private int longestKeyInMap(Map<String, String> map) {
		int longest = 0;
		for (String key : map.keySet()) {
			if (key.length() > longest) {
				longest = key.length();
			}
		}
		return longest;
	}

	public void printTestCaseDescription(boolean verbose, PrintStream out) {
		int separatorLength = 40;
		char resultsSeparator = '-';
		StringUtil.printTitleWithBorder(getTestCaseTitle(), '*', ' ', separatorLength, out);
		printPropertyDescriptions(out);
		StringUtil.printTitleWithBorder("Results", '-', ' ', separatorLength, out);
		if (result != null) result.printResult(verbose, out);
		else out.println("Test Case Not Run.");
		out.println(StringUtil.getNChars(separatorLength, resultsSeparator));
		out.println();
	}

	protected String attributesDescription() {
		String descrip = "points=" + points + ", methodName=" + methodName + ", parameterTypes=";
		try {
			descrip += Arrays.toString(parameterTypes);
		} catch (Throwable err) {
			descrip += "Could not print due to error: " + err.toString();
		}
		descrip += ", fullParameterTypes=";
		descrip += Arrays.toString(fullParameterTypes);
		descrip += ", returnType=";
		try {
			descrip += returnType;
		} catch (Throwable err) {
			descrip += "Could not print due to error: " + err.toString();
		}
		descrip += ", parameters=";
		try {
			descrip += Arrays.deepToString(parameters);
		} catch (Throwable err) {
			descrip += "Could not print due to error: " + err.toString();
		}
		descrip += ", constructorParameterTypes=";
		try {
			descrip += Arrays.toString(constructorParameterTypes);
		} catch (Throwable err) {
			descrip += "Could not print due to error: " + err.toString();
		}
		descrip += ", fullConstructorParameterTypes=";
		descrip += Arrays.toString(fullConstructorParameterTypes);
		descrip += ", constructorParameters=";
		try {
			descrip += Arrays.deepToString(constructorParameters);
		} catch (Throwable err) {
			descrip += "Could not print due to error: " + err.toString();
		}
		return descrip;
//		return "points=" + points + ", methodName=" + methodName + ", parameterTypes="
//				+ Arrays.toString(parameterTypes) + ", fullParameterTypes=" + Arrays.toString(fullParameterTypes)
//				+ ", returnType=" + returnType + ", parameters=" + Arrays.deepToString(parameters)
//				+ ", constructorParameterTypes=" + Arrays.toString(constructorParameterTypes)
//				+ ", fullConstructorParameterTypes=" + Arrays.toString(fullConstructorParameterTypes)
//				+ ", constructorParameters=" + Arrays.deepToString(constructorParameters);
	}

	@Override
	public String toString() {
		return getClass().getName() + "[" + attributesDescription() + "]";
	}

	public void runTestCase() {
		if (promptForNextCase()) {
			// before running the next test, wait for the user to press enter
			SYS_OUT.println("Press Enter to Run next Test");
			TestRunner.SCANNER.nextLine();
		}
		if (runThread == null) {
			final TestCaseResult result = this.result;
			final long timeout = this.timeout + numTimeouts * timeoutIncreasePerTimeout;
			final TestCase nextCase = getNextCase();
			final String inputs = this.inputs;

			testRunnable = new TimeoutThread() {

				@Override
				public void run() {
					System.setOut(result.getOutput());
					System.setErr(result.getErrOutput());
					System.setIn(new ByteArrayInputStream(inputs != null ? inputs.getBytes() : new byte[0]));
					long startTime = System.currentTimeMillis();
					runCase();
					result.setRunTime(System.currentTimeMillis() - startTime);
					if (!isTimedOut()) {
						if (timeoutRunnable != null) timeoutRunnable.timeout();
						//SYS_OUT.println("Test Case completed");
						if (nextCase != null) {
							nextCase.runTestCase();
						} else if (callback != null) {
							callback.run();
						}
						runThread = null;
					}
				}
			};

			timeoutRunnable = new TimeoutThread() {

				@Override
				public void run() {
					try {
						synchronized(this) {
							wait(timeout);
						}
						if (!isTimedOut()) {
							testRunnable.timeout(); // this way is safer but lots of timeouts will slow things down a lot
							runThread.setPriority(Thread.MIN_PRIORITY);
							//runThread.stop(); // deprecated as this causes unexpected behavior on other threads
							// Not using stop causes the thread to keep running until the program ends with System.exit(0)
							SYS_OUT.println("Method call timed out after " + timeout + "ms. Possible infinite loop.");
							result.setPassed(false);
							result.setRunTime(10 * timeout);
							result.addNote("Method call timed out after " + timeout + "ms. Possible infinite loop.");
							numTimeouts++;
							if (nextCase != null) {
								nextCase.runTestCase();
							} else if (callback != null) {
								callback.run();
							}
							runThread = null;
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
						e.printStackTrace(SYS_ERR);
					}
				}
			};

			runThread = new Thread(testRunnable);
			runThread.setName(threadID);
			runThread.start();

			Thread timeoutThread = new Thread(timeoutRunnable);
			timeoutThread.start();
		}

	}

	public boolean promptForNextCase() {
		return false;
	}

	protected abstract void runCase();
}
