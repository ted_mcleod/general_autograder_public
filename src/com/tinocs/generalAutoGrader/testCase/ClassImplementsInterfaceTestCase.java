package com.tinocs.generalAutoGrader.testCase;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import com.google.gson.JsonObject;

/**
 * This class is passed if the class being tested implements the expected interface.
 * Basic Example usage to check if the class being tested implements the Comparable interface with
 * type parameter String, so it can be compared to String objects.
 * {
 *
 *		"testName":"Interface Test",
 *		"testCases": [
 *			{
 *				"testCaseClass": "ClassImplementsInterfaceTestCase",
 *				"expectedInterface": "Comparable<String>"
 *			}
 *		]
 *	}
 * 
 * If the interface takes type parameters and a type parameter needs to be the name of the class being tested,
 * you can use (this) to represent the class Example usage (Testing if the tested class implements
 * Comparable so it can be compared to other objects of its own class):
 * {
 *
 *		"testName":"Interface Test",
 *		"testCases": [
 *			{
 *				"testCaseClass": "ClassImplementsInterfaceTestCase",
 *				"expectedInterface": "Comparable<(this)>"
 *			}
 *		]
 *	}
 *	
 * @author Ted_McLeod
 *
 */
public class ClassImplementsInterfaceTestCase extends DeclarationTestCase {
	public static final String EXPECTED_INTERFACE_KEY = "expectedInterface"; // The name of the expected interface (required)
	public static final String ALTERNATIVE_INTERFACES_KEY = "alternativeInterfaces"; // (default null) an array of alternate interface declarations (i.e. Comparable vs Comparable<(this)>)

	private String expectedInterface;
	private String[] alternativeInterfaces;

	public ClassImplementsInterfaceTestCase(JsonObject testCaseDef, String classBeingTested) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, MalformedURLException {
		super(testCaseDef, classBeingTested);

		// expectedInterface
		try {
			this.expectedInterface = cleanUpClassName(testCaseDef.get(EXPECTED_INTERFACE_KEY).getAsString());	
		} catch (IllegalStateException err) {
			printInvalidFormatError(EXPECTED_INTERFACE_KEY);
			throw err;
		}

		// alternativeInterfaces
		try {
			this.alternativeInterfaces = testCaseDef.has(ALTERNATIVE_INTERFACES_KEY) ? stringArrayFromJsonArray(testCaseDef.get(ALTERNATIVE_INTERFACES_KEY).getAsJsonArray()) : null;
		} catch (IllegalStateException err) {
			printInvalidFormatError(ALTERNATIVE_INTERFACES_KEY);
			throw err;
		}
	}

	@Override
	public LinkedHashMap<String, String> propertyDescriptions() {
		LinkedHashMap<String, String> props = super.propertyDescriptions();
		props.put("Expected Interface", getExpectedInterface());
		if (alternativeInterfaces != null && alternativeInterfaces.length > 0) props.put("Alternative Interfaces", Arrays.toString(alternativeInterfaces));
		return props;
	}

	private boolean interfacesContainsInterface(Type[] interfaces, String interf) {
		try {
			for (Type type : interfaces) {
				String typeStr = type.toString().replaceAll("interface ", "");
				if (typeStr.equals(interf)) {
					getResult().addNote(typeStr + " equals " + interf);
					return true;
				} else {
					if (typeStr.lastIndexOf('.') != -1) {
						typeStr = typeStr.substring(typeStr.lastIndexOf('.') + 1);
						if (typeStr.equals(interf)) {
							getResult().addNote(typeStr + " equals " + interf);
							return true;
						} else {
							getResult().addNote(typeStr + " does not equal " + interf);
						}
					} else {
						getResult().addNote(typeStr + " does not equal " + interf);
					}
				}
			}
		} catch (Throwable e) {
			getResult().addException(e);
		}
		return false;
	}

	@Override
	protected void runCase() {
		try {
			Class<?> cls = Class.forName(getClassBeingTested());
			Type[] interfaces = cls.getGenericInterfaces();
			getResult().addNote("Found interfaces: " + Arrays.toString(interfaces));
			if (interfacesContainsInterface(interfaces, getExpectedInterface())) {
				getResult().setPassed(true);
			} else if (alternativeInterfaces != null && alternativeInterfaces.length > 0) {
				for (String interf : alternativeInterfaces) {
					if (interfacesContainsInterface(interfaces, interf)) {
						getResult().setPassed(true);
						break;
					}
				}
			}
		} catch (Exception e) {
			getResult().addException(e);
		}
	}

	public String getExpectedInterface() {
		return expectedInterface;
	}

	public void setExpectedInterface(String expectedInterface) {
		this.expectedInterface = expectedInterface;
	}

	@Override
	protected String attributesDescription() {
		return super.attributesDescription() + ", expectedInterface=" + expectedInterface;
	}
	
	@Override
	protected Set<String> validTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> validParams = super.validTestCaseParameters();
		if (validParams == null) {
			validParams = new HashSet<>();
		}
		validParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				EXPECTED_INTERFACE_KEY, ALTERNATIVE_INTERFACES_KEY})));
		return validParams;
	}

	@Override
	protected Set<String> requiredTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> reqParams = super.requiredTestCaseParameters();
		if (reqParams == null) {
			reqParams = new HashSet<>();
		}
		reqParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				EXPECTED_INTERFACE_KEY})));
		return reqParams;
	}

	@Override
	protected Set<String> invalidTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> invalidParams = super.invalidTestCaseParameters();
		if (invalidParams == null) {
			invalidParams = new HashSet<>();
		}
		invalidParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				METHOD_NAME_KEY,PARAMETER_TYPES_KEY, RETURN_TYPE_KEY,
				CONSTRUCTOR_PARAMETER_TYPES_KEY})));
		return invalidParams;
	}
}
