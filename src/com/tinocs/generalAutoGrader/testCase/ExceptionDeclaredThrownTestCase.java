package com.tinocs.generalAutoGrader.testCase;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import com.google.gson.JsonObject;

/**
 * This test is passed if the method being tested declares explicitly that it throws the expected exception
 * @author Ted_McLeod
 *
 */
public class ExceptionDeclaredThrownTestCase extends DeclarationTestCase {
	public static final String EXPECTED_EXCEPTION_KEY = "expectedException"; // The class name of the exception that is declared to be thrown (required)
	
	private String expectedException;

	public ExceptionDeclaredThrownTestCase(JsonObject testCaseDef, String classBeingTested) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, MalformedURLException {
		super(testCaseDef, classBeingTested);
		
		// expectedException
		try {
			this.expectedException = cleanUpClassName(testCaseDef.get(EXPECTED_EXCEPTION_KEY).getAsString());
		} catch (IllegalStateException err) {
			printInvalidFormatError(EXPECTED_EXCEPTION_KEY);
			throw err;
		}
	}
	
	@Override
	public LinkedHashMap<String, String> propertyDescriptions() {
		LinkedHashMap<String, String> props = super.propertyDescriptions();
		props.put("Expected Exception", expectedException);
		return props;
	}

	@Override
	protected void runCase() {
		try {
			Method m = getMethodWithCorrectSignature();
			if (m != null) {
				Type[] thrownTypes = m.getGenericExceptionTypes();
				String msg = "Exceptions declared to be thrown: ";
				for (Type type : thrownTypes) {
					String typeStr = type.toString().replaceAll("class ", "");
					msg += typeStr;
					if (type != thrownTypes[thrownTypes.length - 1]) msg += ", ";
					if (typeStr.equals(getExpectedException())) {
						getResult().setPassed(true);
					}
				}
				getResult().addNote(msg);
				if (!getResult().isPassed()) {
					getResult().addNote("Does not explicitly declare " + getExpectedException() + " thrown.");
				}
			}
		} catch (Throwable e) {
			getResult().addException(e);
		}
	}

	public String getExpectedException() {
		return expectedException;
	}

	public void setExpectedException(String expectedException) {
		this.expectedException = cleanUpClassName(expectedException);
	}

	@Override
	protected String attributesDescription() {
		return super.attributesDescription() + ", expectedException=" + expectedException;
	}
	
	@Override
	protected Set<String> validTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> validParams = super.validTestCaseParameters();
		if (validParams == null) {
			validParams = new HashSet<>();
		}
		validParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				EXPECTED_EXCEPTION_KEY})));
		return validParams;
	}

	@Override
	protected Set<String> requiredTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> reqParams = super.requiredTestCaseParameters();
		if (reqParams == null) {
			reqParams = new HashSet<>();
		}
		reqParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				EXPECTED_EXCEPTION_KEY})));
		return reqParams;
	}
}
