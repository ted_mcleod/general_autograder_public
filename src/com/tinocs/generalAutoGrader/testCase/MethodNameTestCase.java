package com.tinocs.generalAutoGrader.testCase;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.google.gson.JsonObject;

/**
 * This test passes if the class has the method name given in the test case
 * @author Ted_McLeod
 *
 */
public class MethodNameTestCase extends DeclarationTestCase {

	public MethodNameTestCase(JsonObject testCaseDef, String classBeingTested) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, MalformedURLException {
		super(testCaseDef, classBeingTested);
	}

	@Override
	protected void runCase() {
		Class<?> cls;
		try {
			cls = Class.forName(getClassBeingTested());
			Method[] methods = cls.getDeclaredMethods();
			String methodNames = "Found Declared Method Names: ";
			for (Method m : methods) {
				methodNames += m.getName() + ", ";
				if (m.getName().equals(getMethodName())) {
					getResult().setPassed(true);
				}
			}
			if (methods.length > 0) methodNames = methodNames.substring(0, methodNames.lastIndexOf(','));
			getResult().addNote(methodNames);
		} catch (Throwable e) {
			getResult().addException(e);
		}
	}

	@Override
	protected Set<String> requiredTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> reqParams = super.requiredTestCaseParameters();
		if (reqParams == null) {
			reqParams = new HashSet<>();
		}
		reqParams.addAll(new HashSet<>(Arrays.asList(new String[]{METHOD_NAME_KEY})));
		return reqParams;
	}
}
