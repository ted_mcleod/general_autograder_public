package com.tinocs.generalAutoGrader.testCase;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * This test is passed if, after calling the method, the parameters have the values given by expectedResultParameters
 * @author Ted_McLeod
 *
 */
public class ParametersModifiedTestCase extends ExecutionTestCase {
	public static final String EXPECTED_RESULT_PARAMETERS_KEY = "expectedResultParameters"; // The expected value of the parameters after calling the method (Required)
	public static final String ALTERNATIVE_EXPECTED_RESULT_PARAMETERS_KEY = "alternativeExpectedResultParameters";  // (DEFAULT null) An array of other acceptable values the parameters could have after calling the method (optional) 
	
	private Object[] expectedResultParameters;
	private Object[][] alternativeExpectedResultParameters;
	
	public ParametersModifiedTestCase(JsonObject testCaseDef, String classBeingTested) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, MalformedURLException {
		super(testCaseDef, classBeingTested);
		
		// expectedResultParameters
		try {
			JsonArray expectedParams = testCaseDef.get(EXPECTED_RESULT_PARAMETERS_KEY).getAsJsonArray();
			this.expectedResultParameters = parameterArrayFromJsonArray(expectedParams, getParameterTypes(), getFullParameterTypes());
		} catch (IllegalStateException err) {
			printInvalidFormatError(EXPECTED_RESULT_PARAMETERS_KEY);
			throw err;
		} catch (NoSuchMethodException | ClassNotFoundException | InvocationTargetException | InstantiationException cnfe) {
			getResult().addException(cnfe);
			this.expectedResultParameters = null;
		}
		
		// alternativeExpectedResultParameters
		if (testCaseDef.has(ALTERNATIVE_EXPECTED_RESULT_PARAMETERS_KEY)) {
			try {
				JsonArray altExpectedParams = testCaseDef.get(ALTERNATIVE_EXPECTED_RESULT_PARAMETERS_KEY).getAsJsonArray();
				this.alternativeExpectedResultParameters = new Object[altExpectedParams.size()][];
				for (int i = 0; i < altExpectedParams.size(); i++) {
					this.alternativeExpectedResultParameters[i] = parameterArrayFromJsonArray(altExpectedParams.get(i).getAsJsonArray(), getParameterTypes(), getFullParameterTypes());
				}
			} catch (IllegalStateException err) {
				printInvalidFormatError(ALTERNATIVE_EXPECTED_RESULT_PARAMETERS_KEY);
				throw err;
			} catch (NoSuchMethodException | ClassNotFoundException | InvocationTargetException | InstantiationException cnfe) {
				getResult().addException(cnfe);
				this.alternativeExpectedResultParameters = null;
			}
		}
	}
	
	@Override
	public LinkedHashMap<String, String> propertyDescriptions() {
		LinkedHashMap<String, String> props = super.propertyDescriptions();
		String paramDescrip = null;
		try {
			paramDescrip = Arrays.deepToString(expectedResultParameters);
		} catch (Throwable err) {
			paramDescrip = "Error: Cannot print due to the following exception:\n" + err.toString();
		}
		props.put("Expected Result Parameters", paramDescrip);
		if (alternativeExpectedResultParameters != null) {
			String altParamDescrip = null;
			try {
				altParamDescrip = Arrays.deepToString(alternativeExpectedResultParameters);
			} catch (Throwable err) {
				altParamDescrip = "Error: Cannot print due to the following exception:\n" + err.toString();
			}
			props.put("Alternative ExpectedResult Parameters", altParamDescrip);
		}
		return props;
	}

	@Override
	protected void runCase() {
		if (expectedResultParameters != null) {
			try {
				Method method = getMethodWithCorrectSignature();
				if (method != null && getParameters() != null) {
					Object inst = !Modifier.isStatic(method.getModifiers()) ? getInstanceOfClass(getClassBeingTested()) : null;
					method.setAccessible(true);
					method.invoke(inst, getParameters());
					getResult().addNote("Parameters After Method Call: " + Arrays.deepToString(getParameters()));
					getResult().setPassed(Arrays.deepToString(expectedResultParameters).equals(Arrays.deepToString(getParameters())));
					if (!getResult().isPassed() && alternativeExpectedResultParameters != null) {
						for (Object[] altParam : alternativeExpectedResultParameters) {
							if (Arrays.deepToString(altParam).equals(Arrays.deepToString(getParameters()))) {
								getResult().setPassed(true);
								getResult().addNote("Match found with alternative result: " + Arrays.deepToString(altParam));
								break;
							}
						}
					}
				}
			} catch (Throwable e) {
				getResult().addException(e);
			}
		}
	}

	@Override
	protected String attributesDescription() {
		return super.attributesDescription() + ", expectedResultParameters=" + Arrays.deepToString(expectedResultParameters);
	}
	
	@Override
	protected Set<String> validTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> validParams = super.validTestCaseParameters();
		if (validParams == null) {
			validParams = new HashSet<>();
		}
		validParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				EXPECTED_RESULT_PARAMETERS_KEY,
				ALTERNATIVE_EXPECTED_RESULT_PARAMETERS_KEY})));
		return validParams;
	}

	@Override
	protected Set<String> requiredTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> reqParams = super.requiredTestCaseParameters();
		if (reqParams == null) {
			reqParams = new HashSet<>();
		}
		reqParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				EXPECTED_RESULT_PARAMETERS_KEY})));
		return reqParams;
	}
}
