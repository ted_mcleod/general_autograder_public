package com.tinocs.generalAutoGrader.testCase;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * This test is passed if all the fields listed in fieldsChanged have the values listed in expectedFieldValues after method call
 * @author Ted_McLeod
 *
 */
public class FieldValuesChangedTestCase extends ExecutionTestCase {
	public static final String FIELDS_CHANGED_KEY = "fieldsChanged"; // The fields that should change (Required)
	public static final String FIELD_TYPES_KEY = "fieldTypes"; // the types of the fields that should change (required)
	public static final String EXPECTED_FIELD_VALUES_KEY = "expectedFieldValues"; // The expected values after method call of the fields being tested as listed in the fieldsChanged parameter (Required)
	
	private String[] fieldsChanged;
	private Class<?>[] fieldTypes;
	private String[] fullFieldTypes;
	private Object[] expectedFieldValues;
	
	public FieldValuesChangedTestCase(JsonObject testCaseDef, String classBeingTested) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, MalformedURLException {
		super(testCaseDef, classBeingTested);
		
		// fields changed
		try {
			this.fieldsChanged = testCaseDef.has(FIELDS_CHANGED_KEY) ? stringArrayFromJsonArray(testCaseDef.get(FIELDS_CHANGED_KEY).getAsJsonArray()) : new String[0];
		} catch (IllegalStateException err) {
			printInvalidFormatError(FIELDS_CHANGED_KEY);
			throw err;
		}
		
		// Field types
		try {
			this.fieldTypes = testCaseDef.has(FIELD_TYPES_KEY) ? classArrayFromJsonArray(testCaseDef.get(FIELD_TYPES_KEY).getAsJsonArray()) : new Class<?>[0];
			this.fullFieldTypes = testCaseDef.has(FIELD_TYPES_KEY) ? stringArrayFromJsonArray(testCaseDef.get(FIELD_TYPES_KEY).getAsJsonArray()) : new String[0];
			cleanUpClassNames(fullFieldTypes);
		} catch (IllegalStateException err) {
			printInvalidFormatError(FIELD_TYPES_KEY);
			throw err;
		} catch (ClassNotFoundException cnfe) {
			getResult().addException(cnfe);
			this.fieldTypes = null;
		}
		
		
		// expectedResultParameters
		try {
			JsonArray expectedFieldVals = testCaseDef.get(EXPECTED_FIELD_VALUES_KEY).getAsJsonArray();
			this.expectedFieldValues = parameterArrayFromJsonArray(expectedFieldVals, fieldTypes, fullFieldTypes);
		} catch (IllegalStateException err) {
			printInvalidFormatError(EXPECTED_FIELD_VALUES_KEY);
			throw err;
		} catch (NoSuchMethodException | ClassNotFoundException cnfe) {
			getResult().addException(cnfe);
			this.expectedFieldValues = null;
		}
	}
	
	@Override
	public LinkedHashMap<String, String> propertyDescriptions() {
		LinkedHashMap<String, String> props = super.propertyDescriptions();
		props.put("Fields Changed", Arrays.deepToString(fieldsChanged));
		props.put("Field Types", Arrays.deepToString(fullFieldTypes));
		String expectedFieldValDescrip = null;
		try {
			expectedFieldValDescrip = Arrays.deepToString(expectedFieldValues);
		} catch (Throwable err) {
			expectedFieldValDescrip = "Error: Cannot print due to the following exception:\n" + err.toString();
		}
		props.put("Expected Field Values", expectedFieldValDescrip);
		return props;
	}

	@Override
	protected void runCase() {
		if (fieldTypes != null && expectedFieldValues != null) {
			try {
				Method method = getMethodWithCorrectSignature();
				if (method != null && getParameters() != null) {
					Object inst = !Modifier.isStatic(method.getModifiers()) ? getInstanceOfClass(getClassBeingTested()) : null;
					method.setAccessible(true);
					method.invoke(inst, getParameters());
					Object[] fieldValues = new Object[fieldsChanged.length];
					for (int i = 0; i < fieldsChanged.length; i++) {
						Field field = getField(fieldsChanged[i]);
						field.setAccessible(true);
						fieldValues[i] = field.get(inst);
					}
					getResult().addNote("Field values after method call: " + Arrays.deepToString(fieldValues));
					getResult().setPassed(Arrays.deepToString(expectedFieldValues).equals(Arrays.deepToString(fieldValues)));
				}
			} catch (Throwable e) {
				getResult().addException(e);
			}
		}
	}

	@Override
	protected String attributesDescription() {
		return super.attributesDescription()+ ", fieldsChanged=" + Arrays.toString(fieldsChanged)+ ", fullFieldTypes=" + Arrays.toString(fullFieldTypes)  + ", expectedFieldValues=" + Arrays.deepToString(expectedFieldValues);
	}
	
	@Override
	protected Set<String> validTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> validParams = super.validTestCaseParameters();
		if (validParams == null) {
			validParams = new HashSet<>();
		}
		validParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				FIELDS_CHANGED_KEY,
				FIELD_TYPES_KEY, EXPECTED_FIELD_VALUES_KEY})));
		return validParams;
	}

	@Override
	protected Set<String> requiredTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> reqParams = super.requiredTestCaseParameters();
		if (reqParams == null) {
			reqParams = new HashSet<>();
		}
		reqParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				FIELDS_CHANGED_KEY,
				FIELD_TYPES_KEY, EXPECTED_FIELD_VALUES_KEY})));
		return reqParams;
	}
}
