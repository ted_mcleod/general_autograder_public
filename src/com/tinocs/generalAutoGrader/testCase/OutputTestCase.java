package com.tinocs.generalAutoGrader.testCase;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import com.google.gson.JsonObject;
import com.tinocs.generalAutoGrader.StringUtil;

/**
 * This test passes if calling the method results in the expected output being printed using System.out
 * @author Ted_McLeod
 *
 */
public class OutputTestCase extends ExecutionTestCase {
	public static final String EXPECTED_OUTPUT_KEY = "expectedOutput"; // The expected output (required)
	public static final String EXPECTED_OUTPUT_IS_FILE_NAME_KEY = "ExpectedOutputIsFileName"; // (default false) If this is true, the expectedOutput property is interpreted as a file name and the contents of the file will be treated as the expected output
	public static final String REMOVE_CARRIAGE_RETURN_KEY = "removeCarriageReturn"; // (Default true) Removes carriage returns from the expected and actual output.  True by default because the expected output will usually just use \n for new lines, but PrintStream will use \r\n (optional)
	public static final String REMOVE_TRAILING_WHITE_SPACE_KEY = "removeTrailingWhiteSpace"; // (Default false) Removes the trailing white space from the output and expected output. If this is set to true, you should set trim to false since trim will also remove trailing white space in addition to white space at the beginning (optional)
	public static final String TRIM_KEY = "trim"; // (Default true) Remove white space from the beginning and end of the expected output and actual output (optional)
	public static final String REGEX_REPLACEMENTS_KEY = "regexReplacements"; // (Default null) A map where each key is a regex string and each value is the string to replace it with. Example: {"\\s": ""} removes all white space (optional)
	public static final String ALTERNATIVE_OUTPUTS_KEY = "alternativeOutputs"; // (Default null) an array of alternative outputs that are also acceptable for the purpose of passing this test (optional)
	public static final String CONTAINS_KEY = "contains"; // (Default false) If this is true, then the output must contain the expected output to pass, but the expected output no longer has to match the entire output (optional)
	public static final String STARTS_WITH_KEY = "startsWith"; // (Default false) If this is true, then output must start with the expected output to pass, but the expected output no longer has to match the entire output (optional)
	public static final String ENDS_WITH_KEY = "endsWith"; // (Default false) If this is true, then the output must end with the expected output to pass, but the expected output no longer has to match the entire output (optional)
	public static final String IGNORE_CASE_KEY = "ignoreCase"; // (Default false) If this is true, strings will be compared ignoring case
	public static final String IGNORE_ALL_EXCEPT_KEY = "ignoreAllExcept"; // (default false) array of strings. Removes anything except these strings. Example: ["hi", "bye"] removes everything except "hi" and "bye".
	
	private String expectedOutput;
	private boolean expectedOutputIsFileName;
	private boolean removeCarriageReturn;
	private boolean removeTrailingWhiteSpace;
	private boolean trim;
	private Map<String, String> regexReplacements;
	private String[] alternativeOutputs;
	private String[] ignoreAllExcept;
	private boolean contains;
	private boolean startsWith;
	private boolean endsWith;
	private boolean ignoreCase;

	public OutputTestCase(JsonObject testCaseDef, String classBeingTested) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException {
		super(testCaseDef, classBeingTested);
		
		// expectedOutput
		try {
			this.expectedOutput = testCaseDef.get(EXPECTED_OUTPUT_KEY).getAsString();
		} catch (IllegalStateException err) {
			printInvalidFormatError(EXPECTED_OUTPUT_KEY);
			throw err;
		}
		
		// expectedOutputIsFileName
		try {
			this.expectedOutputIsFileName = testCaseDef.has(EXPECTED_OUTPUT_IS_FILE_NAME_KEY) ? testCaseDef.get(EXPECTED_OUTPUT_IS_FILE_NAME_KEY).getAsBoolean() : false;
		} catch (IllegalStateException err) {
			printInvalidFormatError(EXPECTED_OUTPUT_IS_FILE_NAME_KEY);
			throw err;
		}
		
		if (this.expectedOutputIsFileName) {
			try (BufferedReader reader = new BufferedReader(new FileReader(this.expectedOutput))) {
				StringBuilder sb = new StringBuilder();
				int c = reader.read();
				while (c != -1) {
					sb.append((char)c);
					c = reader.read();
				}
				this.expectedOutput = sb.toString();
			} catch (FileNotFoundException err) {
				printMissingExpectedOutputFileError(this.expectedOutput);
				throw err;
			} catch (IOException err) {
				printExpectedOutputFileIOException(this.expectedOutput);
				throw err;
			}
		}
		
		// removeCarriageReturn
		try {
			this.removeCarriageReturn = testCaseDef.has(REMOVE_CARRIAGE_RETURN_KEY) ? testCaseDef.get(REMOVE_CARRIAGE_RETURN_KEY).getAsBoolean() : true;
		} catch (IllegalStateException err) {
			printInvalidFormatError(REMOVE_CARRIAGE_RETURN_KEY);
			throw err;
		}
		
		// removeTrailingWhiteSpace
		try {
			this.removeTrailingWhiteSpace = testCaseDef.has(REMOVE_TRAILING_WHITE_SPACE_KEY) ? testCaseDef.get(REMOVE_TRAILING_WHITE_SPACE_KEY).getAsBoolean() : false;
		} catch (IllegalStateException err) {
			printInvalidFormatError(REMOVE_TRAILING_WHITE_SPACE_KEY);
			throw err;
		}
		
		// trim
		try {
			this.trim = testCaseDef.has(TRIM_KEY) ? testCaseDef.get(TRIM_KEY).getAsBoolean() : true;
		} catch (IllegalStateException err) {
			printInvalidFormatError(TRIM_KEY);
			throw err;
		}
		
		// regexReplacements
		try {
			this.regexReplacements = testCaseDef.has(REGEX_REPLACEMENTS_KEY) ? mapStrStrFromJsonObject(testCaseDef.get(REGEX_REPLACEMENTS_KEY).getAsJsonObject()) : null;
		} catch (IllegalStateException err) {
			printInvalidFormatError(REGEX_REPLACEMENTS_KEY);
			throw err;
		}
		
		// alternativeOutputs
		try {
			this.alternativeOutputs = testCaseDef.has(ALTERNATIVE_OUTPUTS_KEY) ? stringArrayFromJsonArray(testCaseDef.get(ALTERNATIVE_OUTPUTS_KEY).getAsJsonArray()) : null;
		} catch (IllegalStateException err) {
			printInvalidFormatError(ALTERNATIVE_OUTPUTS_KEY);
			throw err;
		}
		
		// contains
		try {
			this.contains = testCaseDef.has(CONTAINS_KEY) ? testCaseDef.get(CONTAINS_KEY).getAsBoolean() : false;
		} catch (IllegalStateException err) {
			printInvalidFormatError(CONTAINS_KEY);
			throw err;
		}
		
		// startsWith
		try {
			this.startsWith = testCaseDef.has(STARTS_WITH_KEY) ? testCaseDef.get(STARTS_WITH_KEY).getAsBoolean() : false;
		} catch (IllegalStateException err) {
			printInvalidFormatError(STARTS_WITH_KEY);
			throw err;
		}
		
		// endsWith
		try {
			this.endsWith = testCaseDef.has(ENDS_WITH_KEY) ? testCaseDef.get(ENDS_WITH_KEY).getAsBoolean() : false;
		} catch (IllegalStateException err) {
			printInvalidFormatError(ENDS_WITH_KEY);
			throw err;
		}
		
		// caseSensitive
		try {
			this.ignoreCase = testCaseDef.has(IGNORE_CASE_KEY) ? testCaseDef.get(IGNORE_CASE_KEY).getAsBoolean() : false;
		} catch (IllegalStateException err) {
			printInvalidFormatError(IGNORE_CASE_KEY);
			throw err;
		}
		
		// ignoreAllExcept
		try {
			this.ignoreAllExcept = testCaseDef.has(IGNORE_ALL_EXCEPT_KEY) ? stringArrayFromJsonArray(testCaseDef.get(IGNORE_ALL_EXCEPT_KEY).getAsJsonArray()) : null;
		} catch (IllegalStateException err) {
			printInvalidFormatError(IGNORE_ALL_EXCEPT_KEY);
			throw err;
		}
		
	}
	
	@Override
	public LinkedHashMap<String, String> propertyDescriptions() {
		LinkedHashMap<String, String> props = super.propertyDescriptions();
		if (!removeCarriageReturn) props.put("Remove Carriage Return (\\r)", "" + removeCarriageReturn);
		if (removeTrailingWhiteSpace) props.put("Remove Trailing White Space", "" + removeTrailingWhiteSpace);
		if (!trim) props.put("Trim", "" + trim);
		if (regexReplacements != null) {
			for (Map.Entry<String, String> entry : regexReplacements.entrySet()) {
				props.put("Replace \"" + entry.getKey() + "\" with", "\"" + entry.getValue() + "\"");
			}
		}
		if (contains) props.put("contains", "" + contains);
		if (startsWith) props.put("starts with", "" + startsWith);
		if (endsWith) props.put("ends with", "" + endsWith);
		if (ignoreCase) props.put("ignore case", "" + ignoreCase);
		if (alternativeOutputs != null) {
			for (int i = 0; i < alternativeOutputs.length; i++) {
				props.put("Alternative Output " + (i + 1) + System.lineSeparator(), alternativeOutputs[i].replaceAll("\r", "").replaceAll("\n", System.lineSeparator()));
			}
		}
		if (ignoreAllExcept != null) {
			for (int i = 0; i < ignoreAllExcept.length; i++) {
				props.put("ignoreAllExcept", Arrays.toString(ignoreAllExcept));
			}
		}
		props.put("Expected Output" + System.lineSeparator(), expectedOutput.replaceAll("\r", "").replaceAll("\n", System.lineSeparator()));
		return props;
	}
	
	protected String applyPropsToString(String str) {
		if (removeCarriageReturn) str = str.replaceAll("\r", "");
		if (trim) str = str.trim();
		if (removeTrailingWhiteSpace) str = StringUtil.removeTrailingWhiteSpace(str);
		if (regexReplacements != null) {
			for (Map.Entry<String, String> entry : regexReplacements.entrySet()) {
				str = str.replaceAll(entry.getKey(), entry.getValue());
			}
		}
		if (ignoreAllExcept != null) {
			str = removeNonMatches(str, ignoreAllExcept);
		}
		if (ignoreCase) str = str.toLowerCase();
		return str;
	}
	
	protected String removeNonMatches(String str, String[] targets) {
		String s = "";
		for (int i = 0; i <= str.length(); i++) {
			int matchLen = matchLengthAtIndex(str, targets, i);
			if (matchLen > 0) {
				s += str.substring(i, i + matchLen);
				i += matchLen - 1;
			}
		}
		return s;
	}
	
	private int matchLengthAtIndex(String str, String[] targets, int index) {
		for (String s : targets) {
			if (index + s.length() <= str.length() && str.substring(index, index + s.length()).equals(s)) {
				return s.length();
			}
		}
		return 0;
	}
	
	// could be overridden in subclasses that define matching in different ways, such as containing certain key words.
	protected boolean outputMatches(String expected, String output) {
		if (contains || startsWith || endsWith) {
			boolean passed = true;
			if (contains && !output.contains(expected)) {
				passed = false;
				getResult().addNote("output\n" + output + "\ndoes not contain\n" + expected);
			}
			if (startsWith && !output.startsWith(expected)) {
				passed = false;
				getResult().addNote("output\n" + output + "\ndoes not start with\n" + expected);
			}
			if (endsWith && !output.endsWith(expected)) {
				passed = false;
				getResult().addNote("output\n" + output + "\ndoes not end with\n" + expected);
			}
			return passed;
		}
		return output.equals(expected);
	}

	@Override
	protected void runCase() {
		try {
			Method method = getMethodWithCorrectSignature();
			if (method != null && getParameters() != null) {
				Object inst = !Modifier.isStatic(method.getModifiers()) ? getInstanceOfClass(getClassBeingTested()) : null;
				method.setAccessible(true);
				method.invoke(inst, getParameters());
				String output = applyPropsToString(getResult().getOutputString());
				String expected = applyPropsToString(expectedOutput);
				getResult().setPassed(outputMatches(expected, output));
				if (!getResult().isPassed()) {
					if (alternativeOutputs != null) {
						for (String alt : alternativeOutputs) {
							String altExp = applyPropsToString(alt);
							if (outputMatches(altExp, output)) {
								getResult().setPassed(true);
								getResult().addNote("Alternative Expected Output:" + System.lineSeparator() + altExp.replaceAll("\r", "").replaceAll("\n", System.lineSeparator()) + System.lineSeparator() + "matches:" + System.lineSeparator() + output.replaceAll("\r", "").replaceAll("\n", System.lineSeparator()));
								break;
							}
						}
					}
					if (!getResult().isPassed()) {
						getResult().addNote("Expected Output:" + System.lineSeparator() + expected.replaceAll("\r", "").replaceAll("\n", System.lineSeparator()) + System.lineSeparator() + "Does not match:" + System.lineSeparator() + output.replaceAll("\r", "").replaceAll("\n", System.lineSeparator()));
					}
				}
			}
		} catch (Throwable e) {
			getResult().addException(e);
		}
	}

	public String getExpectedOutput() {
		return expectedOutput;
	}

	public void setExpectedOutput(String expectedOutput) {
		this.expectedOutput = expectedOutput;
	}

	public boolean isRemoveCarriageReturn() {
		return removeCarriageReturn;
	}

	public void setRemoveCarriageReturn(boolean removeCarriageReturn) {
		this.removeCarriageReturn = removeCarriageReturn;
	}

	public boolean isRemoveTrailingWhiteSpace() {
		return removeTrailingWhiteSpace;
	}

	public void setRemoveTrailingWhiteSpace(boolean removeTrailingWhiteSpace) {
		this.removeTrailingWhiteSpace = removeTrailingWhiteSpace;
	}

	public boolean isTrim() {
		return trim;
	}

	public void setTrim(boolean trim) {
		this.trim = trim;
	}
	
	public Map<String, String> getRegexReplacements() {
		return regexReplacements;
	}

	public void setRegexReplacements(Map<String, String> regexReplacements) {
		this.regexReplacements = regexReplacements;
	}

	public boolean isIgnoreCase() {
		return ignoreCase;
	}

	public void setIgnoreCase(boolean ignoreCase) {
		this.ignoreCase = ignoreCase;
	}

	public String[] getIgnoreAllExcept() {
		return ignoreAllExcept;
	}

	public void setIgnoreAllExcept(String[] ignoreAllExcept) {
		this.ignoreAllExcept = ignoreAllExcept;
	}

	@Override
	protected String attributesDescription() {
		return super.attributesDescription() + ", expectedOutput=" + expectedOutput + ", expectedOutputIsFileName=" + expectedOutputIsFileName + ", removeCarriageReturn=" +
				removeCarriageReturn + ", removeTrailingWhiteSpace=" + removeTrailingWhiteSpace + ", trim=" + trim +
				", regexReplacements=" + regexReplacements + ", contains=" + contains + ", startsWith=" + startsWith +
				", endsWith=" + endsWith + ", ignoreCase=" + ignoreCase + ", ignoreAllExcept=" + ignoreAllExcept +
				", alternativeOutputs=" + alternativeOutputs;
	}
	
	@Override
	protected Set<String> validTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> validParams = super.validTestCaseParameters();
		if (validParams == null) {
			validParams = new HashSet<>();
		}
		validParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				EXPECTED_OUTPUT_KEY, EXPECTED_OUTPUT_IS_FILE_NAME_KEY, REMOVE_CARRIAGE_RETURN_KEY,
				REMOVE_TRAILING_WHITE_SPACE_KEY, TRIM_KEY, REGEX_REPLACEMENTS_KEY,
				ALTERNATIVE_OUTPUTS_KEY, CONTAINS_KEY, STARTS_WITH_KEY,
				ENDS_WITH_KEY, IGNORE_CASE_KEY, IGNORE_ALL_EXCEPT_KEY})));
		return validParams;
	}

	@Override
	protected Set<String> requiredTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> reqParams = super.requiredTestCaseParameters();
		if (reqParams == null) {
			reqParams = new HashSet<>();
		}
		reqParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				EXPECTED_OUTPUT_KEY})));
		return reqParams;
	}
}
