package com.tinocs.generalAutoGrader.testCase;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import com.google.gson.JsonObject;

/**
 * This test is passed if the class being tested throws the expected exception when running the test case
 * @author Ted_McLeod
 *
 */
public class ExceptionThrownTestCase extends ExecutionTestCase {
	public static final String EXPECTED_EXCEPTION_KEY = "expectedException"; // The class name of the exception that should be thrown (required)
	
	private String expectedException;

	public ExceptionThrownTestCase(JsonObject testCaseDef, String classBeingTested) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, MalformedURLException {
		super(testCaseDef, classBeingTested);
		
		// expectedException
		try {
			this.expectedException = testCaseDef.get(EXPECTED_EXCEPTION_KEY).getAsString();
		} catch (IllegalStateException err) {
			printInvalidFormatError(EXPECTED_EXCEPTION_KEY);
			throw err;
		}
	}
	
	@Override
	public LinkedHashMap<String, String> propertyDescriptions() {
		LinkedHashMap<String, String> props = super.propertyDescriptions();
		props.put("Expected Exception", expectedException);
		return props;
	}

	@Override
	protected void runCase() {
		try {
			if (getMethodName() != null) {
				// exception thrown in method
				Method method = getMethodWithCorrectSignature();
				if (method != null) {
					Object inst = !Modifier.isStatic(method.getModifiers()) ? getInstanceOfClass(getClassBeingTested()) : null;
					try {
						method.setAccessible(true);
						method.invoke(inst, getParameters());
					} catch(Exception error) {
						if (error.getCause().getClass().getName().equals(expectedException) || error.getCause().getClass().getSimpleName().equals(expectedException)) {
							getResult().setPassed(true);
						} else {
							getResult().addException(error);
						}
					}
				}
			} else {
				// exception thrown in constructor?
				getInstanceOfClass(getClassBeingTested());
			}
			
		} catch (Throwable e) {
			getResult().addException(e);
		}
	}

	public String getExpectedException() {
		return expectedException;
	}

	public void setExpectedException(String expectedException) {
		this.expectedException = expectedException;
	}

	@Override
	protected String attributesDescription() {
		return super.attributesDescription() + ", expectedException=" + expectedException;
	}
	
	@Override
	protected Set<String> validTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> validParams = super.validTestCaseParameters();
		if (validParams == null) {
			validParams = new HashSet<>();
		}
		validParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				EXPECTED_EXCEPTION_KEY})));
		return validParams;
	}

	@Override
	protected Set<String> requiredTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> reqParams = super.requiredTestCaseParameters();
		if (reqParams == null) {
			reqParams = new HashSet<>();
		}
		reqParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				EXPECTED_EXCEPTION_KEY})));
		return reqParams;
	}
}
