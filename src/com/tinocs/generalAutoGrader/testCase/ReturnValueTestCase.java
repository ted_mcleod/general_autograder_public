package com.tinocs.generalAutoGrader.testCase;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * This test is passed if the return value matches the expected return value (or one of the alternative return values if there are any)
 * @author Ted_McLeod
 *
 */
public class ReturnValueTestCase extends ExecutionTestCase {
	public static final String EXPECTED_RETURN_VALUE_KEY = "expectedReturnValue"; // The expected return value (Required)
	public static final String ORDER_MATTERS_KEY = "orderMatters"; // (Default true) If this is set to false, arrays will be considered equal if they contain the same elements regardless of order (optional)
	public static final String ALTERNATIVE_RETURN_VALUES_KEY = "alternativeReturnValues"; // (Default null) An array of alternative return values. If the return value matches either the expected return value or one of these return values, the test will be passed. (optional)
	
	private Object expectedReturnValue;
	private boolean orderMatters; // Does the order of elements in returned arrays/arraylists matter? Default is yes.
	private Object[] alternativeReturnValues; // allows alternative return values to be listed (i.e. test cases with alternative return values can be used to give partial credit)
	
	public ReturnValueTestCase(JsonObject testCaseDef, String classBeingTested) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, MalformedURLException {
		super(testCaseDef, classBeingTested);
		
		// expectedReturnValue
		try {
			this.expectedReturnValue = getObjectFromJsonElement(testCaseDef.get(EXPECTED_RETURN_VALUE_KEY), getReturnType(), getFullReturnType());
		} catch (IllegalStateException err) {
			printInvalidFormatError(EXPECTED_RETURN_VALUE_KEY);
			throw err;
		} catch (NoSuchMethodException | ClassNotFoundException | InvocationTargetException | InstantiationException cnfe) {
			getResult().addException(cnfe);
			this.expectedReturnValue = null;
		}
		
		// orderMatters
		try {
			this.orderMatters = testCaseDef.has(ORDER_MATTERS_KEY) ? testCaseDef.get(ORDER_MATTERS_KEY).getAsBoolean() : true;
		} catch (IllegalStateException err) {
			printInvalidFormatError(ORDER_MATTERS_KEY);
			throw err;
		}
		
		// alternativeReturnValues
		if (testCaseDef.has(ALTERNATIVE_RETURN_VALUES_KEY)) {
			try {
				JsonArray jsonArr = testCaseDef.get(ALTERNATIVE_RETURN_VALUES_KEY).getAsJsonArray();
				Class<?>[] typeArr = new Class<?>[jsonArr.size()];
				String[] fullTypesArr = new String[jsonArr.size()];
				for (int i = 0; i < typeArr.length; i++) {
					typeArr[i] = getReturnType();
					fullTypesArr[i] = getFullReturnType();
				}
				this.alternativeReturnValues = parameterArrayFromJsonArray(jsonArr, typeArr, fullTypesArr);
			} catch (IllegalStateException err) {
				printInvalidFormatError(ALTERNATIVE_RETURN_VALUES_KEY);
				throw err;
			} catch (NoSuchMethodException | ClassNotFoundException | InvocationTargetException | InstantiationException cnfe) {
				getResult().addException(cnfe);
				this.alternativeReturnValues = null;
			}
		} else {
			this.alternativeReturnValues = null;
		}
		
	}
	
	@Override
	public LinkedHashMap<String, String> propertyDescriptions() {
		LinkedHashMap<String, String> props = super.propertyDescriptions();
		if (!orderMatters && (Iterable.class.isAssignableFrom(getReturnType()) || getReturnType().isArray())) {
			props.put("Order Matters", "" + orderMatters);
		}
		if (alternativeReturnValues != null) {
			for (int i = 0; i < alternativeReturnValues.length; i++) {
				String key = (i + 1) + ". Alternative Return Value";
				String val = "";
				try {
					val = getReturnType().isArray() ? Arrays.deepToString(getObjectAsArray(alternativeReturnValues[i])) : alternativeReturnValues[i].toString();
				} catch (Throwable err) {
					val = "Error: Cannot print due to the following exception:\n" + err.toString();
				}
				props.put(key, val);
			}
			
		}
		String expectedDescrip = null;
		try {
			expectedDescrip = getReturnType().isArray() ? Arrays.deepToString(getObjectAsArray(expectedReturnValue)) : expectedReturnValue.toString();
		} catch (Throwable err) {
			expectedDescrip = "Error: Cannot print due to the following exception:\n" + err.toString();
		}
		props.put("Expected Return Value", expectedDescrip);
		return props;
	}

	@Override
	protected void runCase() {
		if (getReturnType() != null) {
			try {
				Method method = getMethodWithCorrectSignature();
				if (method != null && getParameters() != null) {
					Object inst = !Modifier.isStatic(method.getModifiers()) ? getInstanceOfClass(getClassBeingTested()) : null;
					if (method.getReturnType().equals(getReturnType())) {
						method.setAccessible(true);
						Object res = method.invoke(inst, getParameters());
						getResult().addNote("Returned: " + (res.getClass().isArray() ? Arrays.deepToString(getObjectAsArray(res)) : res));
						getResult().setPassed(resultMatches(expectedReturnValue, res, orderMatters));
						if (!getResult().isPassed() && alternativeReturnValues != null) {
							for (Object altRetVal: alternativeReturnValues) {
								if (resultMatches(altRetVal, res, orderMatters)) {
									getResult().setPassed(true);
									break;
								}
							}
						}
					} else {
						getResult().addNote("Incorrect Return Type: " + method.getReturnType().getName());
					}
				}
			} catch (Throwable e) {
				getResult().addException(e);
			}
		}
	}

	public Object getExpectedReturnValue() {
		return expectedReturnValue;
	}

	public void setExpectedReturnValue(Object expectedReturnValue) {
		this.expectedReturnValue = expectedReturnValue;
	}

	@Override
	protected String attributesDescription() {
		return super.attributesDescription() + ", expectedReturnValue=" + expectedReturnValue + ", orderMatters=" + orderMatters + ", alternativeReturnValues=" + alternativeReturnValues;
	}
	
	@Override
	protected Set<String> validTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> validParams = super.validTestCaseParameters();
		if (validParams == null) {
			validParams = new HashSet<>();
		}
		validParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				EXPECTED_RETURN_VALUE_KEY,
				ORDER_MATTERS_KEY, ALTERNATIVE_RETURN_VALUES_KEY})));
		return validParams;
	}

	@Override
	protected Set<String> requiredTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> reqParams = super.requiredTestCaseParameters();
		if (reqParams == null) {
			reqParams = new HashSet<>();
		}
		reqParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				EXPECTED_RETURN_VALUE_KEY})));
		return reqParams;
	}
}
