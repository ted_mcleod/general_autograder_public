package com.tinocs.generalAutoGrader.testCase;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.google.gson.JsonObject;

/**
 * This test is passed if the return type of the method being tested is correct
 * @author Ted_McLeod
 *
 */
public class ReturnTypeTestCase extends DeclarationTestCase {
	
	public ReturnTypeTestCase(JsonObject testCaseDef, String classBeingTested) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, MalformedURLException {
		super(testCaseDef, classBeingTested);
	}

	@Override
	protected void runCase() {
		try {
			Method m = getMethodWithCorrectSignature();
			if (m != null) {
				Type returnType = m.getGenericReturnType();
				String typeStr = returnType.toString().replaceAll("class ", "");
				if (typeStr.equals(getFullReturnType())) {
					getResult().setPassed(true);
				} else {
					getResult().addNote(typeStr + " not equal to " + getFullReturnType());
				}
			}
		} catch (Throwable e) {
			getResult().addException(e);
		}
	}

	@Override
	protected Set<String> requiredTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> reqParams = super.requiredTestCaseParameters();
		if (reqParams == null) {
			reqParams = new HashSet<>();
		}
		reqParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				METHOD_NAME_KEY, PARAMETER_TYPES_KEY, RETURN_TYPE_KEY})));
		return reqParams;
	}

	@Override
	protected Set<String> invalidTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> invalidParams = super.invalidTestCaseParameters();
		if (invalidParams == null) {
			invalidParams = new HashSet<>();
		}
		invalidParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				CONSTRUCTOR_PARAMETER_TYPES_KEY})));
		return invalidParams;
	}
}
