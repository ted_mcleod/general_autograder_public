package com.tinocs.generalAutoGrader.testCase;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import com.google.gson.JsonObject;

/**
 * This test passes if all the lines of the output match all the lines of the expected output according to the rules of the output test
 * @author Ted_McLeod
 *
 */
public class OutputLinesTestCase extends OutputTestCase {

	public static final String CONTAINS_LINES_KEY = "containsLines"; // (Default false) If this is true, then the output must contain the expected output to pass, but the expected output no longer has to match the entire output (optional)
	public static final String STARTS_WITH_LINES_KEY = "startsWithLines"; // (Default false) If this is true, then output must start with the expected output to pass, but the expected output no longer has to match the entire output (optional)
	public static final String ENDS_WITH_LINES_KEY = "endsWithLines"; // (Default false) If this is true, then the output must end with the expected output to pass, but the expected output no longer has to match the entire output (optional)

	private boolean containsLines;
	private boolean startsWithLines;
	private boolean endsWithLines;
	
	public OutputLinesTestCase(JsonObject testCaseDef, String classBeingTested) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException {
		super(testCaseDef, classBeingTested);

		// containsLines
		try {
			this.containsLines = testCaseDef.has(CONTAINS_LINES_KEY) ? testCaseDef.get(CONTAINS_LINES_KEY).getAsBoolean() : false;
		} catch (IllegalStateException err) {
			printInvalidFormatError(CONTAINS_LINES_KEY);
			throw err;
		}

		// startsWithLines
		try {
			this.startsWithLines = testCaseDef.has(STARTS_WITH_LINES_KEY) ? testCaseDef.get(STARTS_WITH_LINES_KEY).getAsBoolean() : false;
		} catch (IllegalStateException err) {
			printInvalidFormatError(STARTS_WITH_LINES_KEY);
			throw err;
		}

		// endsWith
		try {
			this.endsWithLines = testCaseDef.has(ENDS_WITH_LINES_KEY) ? testCaseDef.get(ENDS_WITH_LINES_KEY).getAsBoolean() : false;
		} catch (IllegalStateException err) {
			printInvalidFormatError(ENDS_WITH_LINES_KEY);
			throw err;
		}
	}
	
	@Override
	public LinkedHashMap<String, String> propertyDescriptions() {
		LinkedHashMap<String, String> props = super.propertyDescriptions();
		if (containsLines) props.put("contains lines", "" + containsLines);
		if (startsWithLines) props.put("starts with lines", "" + startsWithLines);
		if (endsWithLines) props.put("ends with lines", "" + endsWithLines);
		return props;
	}
	
	private String linesToString(String[] lines) {
		String str = "";
		for (String line : lines) {
			str += line + System.lineSeparator();
		}
		return str;
	}

	@Override
	protected boolean outputMatches(String expected, String output) {
		String[] expectedLines = expected.split("\n");
		String[] outputLines = output.split("\n");
		if (containsLines || startsWithLines || endsWithLines) {
			if (expectedLines.length > outputLines.length) {
				getResult().addNote("Expected lines:" + System.lineSeparator() + linesToString(expectedLines) + System.lineSeparator() + "longer than output lines:" + System.lineSeparator() + linesToString(outputLines));
				return false;
			}
			boolean matched = true;
			if (startsWithLines) {
				for (int i = 0; i < expectedLines.length; i++) {
					if (!super.outputMatches(expectedLines[i], outputLines[i])) {
						getResult().addNote("Expected line: " + expectedLines[i] + " does not match output line: " + outputLines[i]);
						matched = false;
					}
				}
			} else if (endsWithLines) {
				for (int i = outputLines.length - 1, j = expectedLines.length - 1; j >= 0; i--, j--) {
					if (!super.outputMatches(expectedLines[j], outputLines[i])) {
						getResult().addNote("Expected line:\n" + expectedLines[j] + "\ndoes not match output lines:\n" + outputLines[i]);
						matched = false;
					}
				}
			} else { // containsLines
				matched = false;
				for (int i = 0; i <= outputLines.length - expectedLines.length; i++) {
					boolean patternMatches = true;
					for (int j = 0; j < expectedLines.length; j++) {
						if (!super.outputMatches(expectedLines[j], outputLines[i + j])) {
							patternMatches = false;
							break;
						}
					}
					if (patternMatches) {
						matched = true;
						break;
					}
				}
				if (!matched) getResult().addNote("Did not find expected lines:\n" + Arrays.toString(expectedLines) + "\nin output lines:\n" + Arrays.toString(outputLines));
			}
			
			return matched;
		} else {
			if (expectedLines.length != outputLines.length) return false;
			boolean matched = true;
			for (int i = 0; i < expectedLines.length; i++) {
				if (!super.outputMatches(expectedLines[i], outputLines[i])) {
					getResult().addNote("Expected line: " + expectedLines[i] + " does not match output line: " + outputLines[i]);
					matched = false;
				}
			}
			return matched;
		}
	}
	
	@Override
	protected Set<String> validTestCaseParameters() {
		// For safety in case class hierarchy changes
		Set<String> validParams = super.validTestCaseParameters();
		if (validParams == null) {
			validParams = new HashSet<>();
		}
		validParams.addAll(new HashSet<>(Arrays.asList(new String[]{
				CONTAINS_LINES_KEY, STARTS_WITH_LINES_KEY, ENDS_WITH_LINES_KEY})));
		return validParams;
	}
}
