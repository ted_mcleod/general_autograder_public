package com.tinocs.generalAutoGrader;
import java.io.PrintStream;
import java.util.Arrays;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class TestsDef {
	public static final String TESTS_KEY = "tests";
	
	private TestDef[] tests;
	private Runnable callback;
	private boolean hasRunTests;
	
	public TestsDef(JsonObject testsDef, String className) {
		this.hasRunTests = false;
		this.callback = null;
		this.tests = testDefsArrayFromJsonArray(testsDef.get(TESTS_KEY).getAsJsonArray(), className);
	}
	
	public void runTests() {
		for (int i = 0; i < tests.length - 1; i++) {
			tests[i].setNextTestDef(tests[i + 1]);
		}
		hasRunTests = true;
		if (tests.length > 0) {
			tests[tests.length - 1].setCallback(callback);
			tests[0].runTests();
		} else if (callback != null) {
			callback.run();
		}
	}
	
	public boolean promptToExitProgram() {
		for (TestDef t : tests) {
			if (t.promptToExitProgram()) return true;
		}
		return false;
	}
	
	public TestDef[] getTests() {
		return tests;
	}

	public void setTests(TestDef[] tests) {
		this.tests = tests;
	}

	public boolean isHasRunTests() {
		return hasRunTests;
	}

	public void setHasRunTests(boolean hasRunTests) {
		this.hasRunTests = hasRunTests;
	}

	public Runnable getCallback() {
		return callback;
	}

	public void setCallback(Runnable callback) {
		this.callback = callback;
	}

	public static TestDef[] testDefsArrayFromJsonArray(JsonArray jsonArr, String className) {
		if (jsonArr == null) return null;
		TestDef[] arr = new TestDef[jsonArr.size()];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = new TestDef(jsonArr.get(i).getAsJsonObject(), className);
		}
		return arr;
	}
	
	public int getNumTestCasesPassed() throws IllegalStateException {
		if (!hasRunTests) {
			throw new IllegalStateException("Tests must be run before getting number of cases passed");
		}
		int numPassed = 0;
		for (TestDef test : tests) {
			numPassed += test.getNumTestCasesPassed();
		}
		return numPassed;
	}
	
	public int getNumTestCases() {
		int numCases = 0;
		for (TestDef test : tests) {
			numCases += test.getNumTestCases();
		}
		return numCases;
	}
	
	public double getRunTime() throws IllegalStateException {
		if (!hasRunTests) {
			throw new IllegalStateException("Tests must be run before getting run time");
		}
		double runTime = 0;
		for (TestDef test : tests) {
			runTime += test.getRunTime();
		}
		return runTime;
	}
	
	public double getNumPointsEarned() throws IllegalStateException {
		if (!hasRunTests) {
			throw new IllegalStateException("Tests must be run before getting number of points earned");
		}
		double numPoints = 0;
		for (TestDef test : tests) {
			numPoints += test.getNumPointsEarned();
		}
		return numPoints;
	}
	
	public double getNumPointsPossible() {
		double numPoints = 0;
		for (TestDef test : tests) {
			numPoints += test.getNumPointsPossible();
		}
		return numPoints;
	}
	
	public double getPointsPercent() throws IllegalStateException {
		if (!hasRunTests) {
			throw new IllegalStateException("Tests must be run before getting points percent");
		}
		return 100 * getNumPointsEarned() / getNumPointsPossible();
	}
	
	public void printReport(boolean verbose, boolean failedTestsOnly, PrintStream out) throws IllegalStateException {
		if (!hasRunTests) {
			throw new IllegalStateException("Tests must be run before printing report");
		}
		for (TestDef test : tests) {
			test.printTestResults(verbose, failedTestsOnly, out);
		}
		StringUtil.printTitleWithBorder("All Tests Results Summary", '*', ' ', 40, out);
		out.println("Passed " + getNumTestCasesPassed() + " / " + getNumTestCases());
		out.printf("Earned %.2f / %.2f = " + "%.2f%%", getNumPointsEarned(), getNumPointsPossible(), getNumPointsEarned() / getNumPointsPossible() * 100);
	}

	@Override
	public String toString() {
		return "TestsDef [tests=" + Arrays.toString(tests) + "]";
	}
	
	
}
