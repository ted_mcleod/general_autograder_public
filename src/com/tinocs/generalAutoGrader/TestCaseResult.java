package com.tinocs.generalAutoGrader;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TestCaseResult {
	private boolean passed;
	private ArrayList<Throwable> exceptions;
	private ArrayList<String> notes;
	private ArrayList<String> conditionalNotes; // only printed if test fails
	private ThreadSafePrintStream output;
	private ByteArrayOutputStream outputBytes;
	private ThreadSafePrintStream errOutput;
	private ByteArrayOutputStream errOutputBytes;
	private long runTime;
	
	public TestCaseResult(String threadID, long maxPrintCalls) {
		passed = false;
		exceptions = new ArrayList<>();
		notes = new ArrayList<>();
		conditionalNotes = new ArrayList<>();
		outputBytes = new ByteArrayOutputStream();
		output = new ThreadSafePrintStream(outputBytes, threadID, maxPrintCalls);
		errOutputBytes = new ByteArrayOutputStream();
		errOutput = new ThreadSafePrintStream(outputBytes, threadID, maxPrintCalls);
		runTime = 0;
	}
	
	public long getRunTime() {
		return runTime;
	}
	
	public void setRunTime(long runTime) {
		this.runTime = runTime;
	}
	
	public void addException(Throwable e) {
		exceptions.add(e);
	}
	
	public void addNote(String note) {
		notes.add(note);
	}
	
	public void addConditionalNote(String note) {
		conditionalNotes.add(note);
	}

	public boolean isPassed() {
		return passed;
	}

	public void setPassed(boolean passed) {
		this.passed = passed;
	}

	public List<Throwable> getExceptions() {
		return exceptions;
	}

	public List<String> getNotes() {
		return notes;
	}
	
	public ArrayList<String> getConditionalNotes() {
		return conditionalNotes;
	}

	public PrintStream getOutput() {
		return output;
	}

	public ByteArrayOutputStream getOutputBytes() {
		return outputBytes;
	}

	public PrintStream getErrOutput() {
		return errOutput;
	}

	public ByteArrayOutputStream getErrOutputBytes() {
		return errOutputBytes;
	}
	
	public String getOutputString() {
		return new String(getOutputBytes().toByteArray(), StandardCharsets.UTF_8);
	}

	public void printResult(boolean verbose, PrintStream out) {
		out.println(new String(outputBytes.toByteArray(), StandardCharsets.UTF_8));
		out.println(new String(errOutputBytes.toByteArray(), StandardCharsets.UTF_8));
		for (Throwable e : exceptions) {
			if (verbose) {
				if (e.getCause() != null && e.getCause().getClass().equals(StackOverflowError.class)) {
					ByteArrayOutputStream traceBytes = new ByteArrayOutputStream();
					PrintStream tracePrintStream = new PrintStream(traceBytes);
					e.printStackTrace(tracePrintStream);
					String traceStr = new String(traceBytes.toByteArray(), StandardCharsets.UTF_8);
					Scanner scan = new Scanner(traceStr);
					String trimmedTrace = "";
					int linesAfterStackOverflow = -1;
					while (scan.hasNextLine()) {
						String nextLine = scan.nextLine();
						if (linesAfterStackOverflow < 3) trimmedTrace += nextLine + System.lineSeparator();
						if (nextLine.trim().startsWith("Caused by: java.lang.StackOverflowError")) {
							linesAfterStackOverflow++;
						} else if (linesAfterStackOverflow >= 0) {
							linesAfterStackOverflow++;
						}
					}
					scan.close();
					out.print(trimmedTrace);
					if (linesAfterStackOverflow > 3) {
						out.println("\t..." + (linesAfterStackOverflow - 3) + " more calls in stack");
					}
				} else {
					e.printStackTrace(out);
				}
			}
			else out.println(e.getClass().getName() + ":" + e.getMessage());
		}
		if (!passed) {
			for (String note : conditionalNotes) {
				out.println(note);
			}
		}
		for (String note : notes) {
			out.println(note);
		}
		out.println("Run Time: " + runTime);
		out.println("Test Case " + (passed ? "PASSED" : "FAILED"));
	}

	@Override
	public String toString() {
		return "TestCaseResult [passed=" + passed + ", exceptions=" + exceptions + ", notes=" + notes + "]";
	}
	
	
}
