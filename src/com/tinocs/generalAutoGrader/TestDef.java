package com.tinocs.generalAutoGrader;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tinocs.generalAutoGrader.testCase.TestCase;

public class TestDef {
	public static final String TEST_NAME_KEY = "testName";
	public static final String POINTS_KEY = "points";
	public static final String TEST_CASES_KEY = "testCases";
	
	private static final PrintStream SYS_OUT = System.out; // original System.out
	private static final PrintStream SYS_ERR = System.err; // original System.err
	
	private String testName;
	private TestCase[] testCases;
	private String className;
	private TestDef nextTestDef;
	private Runnable callback;
	private boolean hasRunTests;

	public TestDef(JsonObject testDef, String className) throws IllegalArgumentException {
		this.testName = testDef.get(TEST_NAME_KEY).getAsString();
		this.testCases = testCaseArrayFromJsonArray(testDef.get(TEST_CASES_KEY).getAsJsonArray(), className);
		this.className = className;
		this.nextTestDef = null;
		this.callback = null;
		this.hasRunTests = false;
		try {
			if (testDef.has(POINTS_KEY)) {
				double points = testDef.get(POINTS_KEY).getAsDouble();
				if (points > 0) {
					for (TestCase tc : testCases) {
						tc.setPoints(points / testCases.length);
					}
				} else {
					throw new IllegalArgumentException("Invalid point value for test " + testName + ". Points should always be positive, but points was " + points);
				}
			}
		} catch (IllegalStateException err) {
			printInvalidFormatError(POINTS_KEY);
			throw err;
		}
	}
	
	public void runTests() {
		for (int i = 0; i < testCases.length - 1; i++) {
			testCases[i].setNextCase(testCases[i + 1]);
		}
		hasRunTests = true;
		if (testCases.length > 0) {
			testCases[testCases.length - 1].setCallback(new Runnable() {

				@Override
				public void run() {
					if (nextTestDef != null) {
						nextTestDef.runTests();
					} else if (callback != null) {
						callback.run();
					}
				}
				
			});
			testCases[0].runTestCase();
		} else if (callback != null) {
			callback.run();
		}
	}
	
	public boolean promptToExitProgram() {
		for (TestCase c : testCases) {
			if (c.promptForNextCase()) return true;
		}
		return false;
	}
	
	public void printTestResults(boolean verbose, boolean failedTestsOnly, PrintStream out) throws IllegalStateException {
		if (!hasRunTests) {
			throw new IllegalStateException("Test must be run before printing results");
		}
		if (failedTestsOnly) {
			if (getNumTestCasesPassed() < getNumTestCases()) {
				StringUtil.printTitleWithBorder(testName + " Failed Tests", '#', ' ', 40, out);
			}
		} else {
			StringUtil.printTitleWithBorder(testName + " Test Cases", '#', ' ', 40, out);
		}
		for (TestCase testCase : testCases) {
			if (!failedTestsOnly || !testCase.getResult().isPassed()) {
				testCase.printTestCaseDescription(verbose, out);
			}
		}
		
		StringUtil.printTitleWithBorder(testName + " Results Summary", '#', ' ', 40, out);
		out.println("Passed " + getNumTestCasesPassed() + " / " + getNumTestCases());
		out.printf("Earned %.2f / %.2f = %.2f%%", getNumPointsEarned(), getNumPointsPossible(), getNumPointsEarned() / getNumPointsPossible() * 100);
		out.println();
		out.println(StringUtil.getNChars(40, '#'));
	}
	
	public int getNumTestCasesPassed() throws IllegalStateException {
		if (!hasRunTests) {
			throw new IllegalStateException("Tests must be run before getting number of cases passed");
		}
		int numPassed = 0;
		for (TestCase testCase : testCases) {
			if (testCase.getResult().isPassed()) numPassed++;
		}
		return numPassed;
	}
	
	public int getNumTestCases() {
		return testCases.length;
	}
	
	public double getRunTime() throws IllegalStateException {
		if (!hasRunTests) {
			throw new IllegalStateException("Tests must be run before getting run time");
		}
		double runTime = 0;
		for (TestCase testCase : testCases) {
			runTime += testCase.getResult().getRunTime();
		}
		return runTime;
	}
	
	public double getNumPointsEarned() throws IllegalStateException {
		if (!hasRunTests) {
			throw new IllegalStateException("Tests must be run before getting number of points earned");
		}
		double numPoints = 0;
		for (TestCase testCase : testCases) {
			if (testCase.getResult().isPassed()) numPoints += testCase.getPoints();
		}
		return numPoints;
	}
	
	public double getNumPointsPossible() {
		double numPoints = 0;
		for (TestCase testCase : testCases) {
			numPoints += testCase.getPoints();
		}
		return numPoints;
	}
	
	public double getPointsPercent() throws IllegalStateException {
		if (!hasRunTests) {
			throw new IllegalStateException("Tests must be run before getting points percent");
		}
		return 100 * getNumPointsEarned() / getNumPointsPossible();
	}
	
	public static TestCase[] testCaseArrayFromJsonArray(JsonArray jsonArr, String className) {
		if (jsonArr == null) return null;
		TestCase[] arr = new TestCase[jsonArr.size()];
		for (int i = 0; i < arr.length; i++) {
			JsonObject testCaseDef = jsonArr.get(i).getAsJsonObject();
			String testClassName = TestCase.class.getPackage().getName() + "." + testCaseDef.get(TestCase.TEST_CASE_CLASS_KEY).getAsString();
			try {
				Class<?> cls = Class.forName(testClassName);
				Constructor<?> constr = cls.getConstructor(new Class<?>[]{JsonObject.class, String.class});
				TestCase testCase = (TestCase)constr.newInstance(new Object[]{testCaseDef, className});
				arr[i] = testCase;
			} catch(InvocationTargetException err) {
				SYS_OUT.println("Error instantiating test case:" + System.lineSeparator() + testCaseDef);
				SYS_OUT.println("Possible Causes:\n1. Make sure the test cases use full names for classes other than ArrayList and String (i.e. \"java.awt.Point\")\n2. Make sure the parameter types and parameters defined in the test case match\n3. make sure you are defining arrays in your tests where appropriate, for example alternativeOutputs should be an array of alternative outputs even if the test only has one alternative output");
				err.printStackTrace();
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		return arr;
	}
	
	public TestDef getNextTestDef() {
		return nextTestDef;
	}

	public void setNextTestDef(TestDef nextTestDef) {
		this.nextTestDef = nextTestDef;
	}

	public Runnable getCallback() {
		return callback;
	}

	public void setCallback(Runnable callback) {
		this.callback = callback;
	}

	public String getTestName() {
		return testName;
	}

	public String getClassName() {
		return className;
	}

	public boolean hasRunTests() {
		return hasRunTests;
	}
	
	protected static void printInvalidFormatError(String field) {
		SYS_ERR.println("Invalid format for field: " + field);
	}

	@Override
	public String toString() {
		return "TestDef [testName=" + testName + ", testCases=" + Arrays.toString(testCases) + "]";
	}
	
}
