package com.tinocs.generalAutoGrader;

public abstract class TimeoutThread implements Runnable {
	private volatile boolean timedOut = false;
	
	public void timeout() {
		timedOut = true;
	}
	
	protected boolean isTimedOut() {
		return timedOut;
	}
}

