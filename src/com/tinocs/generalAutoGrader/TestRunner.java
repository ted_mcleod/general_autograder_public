package com.tinocs.generalAutoGrader;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.google.gson.JsonElement;
import com.google.gson.JsonSyntaxException;
import com.tinocs.compression.ReportCompressor;

public class TestRunner {
	public static final Scanner SCANNER = new Scanner(System.in);
	private static final PrintStream SYS_OUT = System.out; // original System.out
	private static final PrintStream SYS_ERR = System.err; // original System.err
	public static final int DEFAULT_NUM_DECIMAL_PLACES = 2;
	
	public static void runTests(File testsFile, File classesDir, boolean verbose, boolean printFailedTestsOnly, boolean includeRunTimes) {
		runTests(testsFile, classesDir, null, null, null, verbose, printFailedTestsOnly, includeRunTimes);
	}
	
	public static void runTests(File testsFile, File classesDir, File fullReportsDir, File rostersDir, List<String> rosterFileNames, boolean verbose, boolean printFailedTestsOnly, boolean includeRunTimes) {
		ArrayList<Roster> rosters = new ArrayList<>();
		if (rostersDir != null && rostersDir.isDirectory()) {
			for (String rosterName : rosterFileNames) {
				try {
					rosters.add(new Roster(new File(rostersDir.toPath() + File.separator + rosterName)));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		SYS_OUT.println(rosters);
		
		ArrayList<String> ignoredFiles = new ArrayList<String>(Arrays.asList("AutoTesterDriver.java"));
		ByteArrayOutputStream summaryContent = new ByteArrayOutputStream();
		PrintStream summaryOut = new PrintStream(summaryContent);
		List<Class<?>> classes = null;
		try {
			classes = findClasses(classesDir, ignoredFiles);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		println("Classes found:" + classes, summaryOut);
		Map<String,ArrayList<Score>> scoresByTest = new HashMap<>();
		ArrayList<Score> scores = new ArrayList<Score>();
		ArrayList<Score> runTimes = new ArrayList<Score>();
		ArrayList<TestsDef> testsToRun = new ArrayList<>();
		JsonElement json = null;
		try {
			json = FileUtil.getObj(testsFile);
		} catch (JsonSyntaxException | IOException e) {
			e.printStackTrace();
		}
		final boolean promptToExit;
		if (classes.size() > 0 && json != null) {
			TestsDef testsDef = new TestsDef(json.getAsJsonObject(), classes.get(0).getName());
			promptToExit = testsDef.promptToExitProgram();
		} else {
			promptToExit = false;
		}
		
		ArrayList<String> errorMessages = new ArrayList<>();
		for (int i = 0; i < classes.size(); i++) {
			Class<?> cls = classes.get(i);
			Class<?> nextClass = (i < classes.size() - 1) ? classes.get(i + 1) : null;
			boolean namedCorrectly = true;
			String periodStr = "0";
			if (cls.getName().length() > 1 && Character.isDigit(cls.getName().charAt(1))) {
				periodStr = cls.getName().substring(1, 2);
			} else {
				namedCorrectly = false;
			}
			int per = 0;
			try {
				per = Integer.parseInt(periodStr);
			} catch (NumberFormatException err) {
				err.printStackTrace();
			}
			final int period = per;
			String[] splitStr = cls.getName().split("_");
			if (splitStr.length <= 1) namedCorrectly = false;
			String lastName = splitStr.length > 1 ? splitStr[1] : cls.getName();
			String firstName = splitStr.length > 2 ? splitStr[2] : cls.getName();
			if (!namedCorrectly) {
				errorMessages.add(cls.getName() + " is incorrectly named. Format should be PX_LastName_FirstName_MyClassName.");
			}
			ByteArrayOutputStream studentReportContent = new ByteArrayOutputStream();
			PrintStream studentReportOut = new PrintStream(studentReportContent);
			JsonElement jsonEl = null;
			try {
				jsonEl = FileUtil.getObj(testsFile);
			} catch (JsonSyntaxException | IOException e) {
				e.printStackTrace();
			}
			if (jsonEl != null) {
				TestsDef testsDef = new TestsDef(jsonEl.getAsJsonObject(), cls.getName());
				testsToRun.add(testsDef);
				testsDef.setCallback(new Runnable() {
					@Override
					public void run() {
						testsDef.printReport(verbose, printFailedTestsOnly, studentReportOut);
						String reportStr = new String(studentReportContent.toByteArray(), StandardCharsets.UTF_8);
						FileUtil.writeString(fullReportsDir.toPath() + File.separator + cls.getName() + ".txt", reportStr);
						scores.add(new Score(lastName, firstName, period, testsDef.getPointsPercent(), false, reportStr));
						for (TestDef td : testsDef.getTests()) {
							if (!scoresByTest.containsKey(td.getTestName())) {
								scoresByTest.put(td.getTestName(), new ArrayList<Score>());
							}
							scoresByTest.get(td.getTestName()).add(new Score(lastName, firstName, period, td.getPointsPercent(), false));
						}
						if (includeRunTimes) runTimes.add(new Score(lastName, firstName, period, testsDef.getRunTime(), false, reportStr));
						println(System.lineSeparator() + "***************************************" + System.lineSeparator(), studentReportOut);
						studentReportOut.close();
						if (testsToRun.isEmpty()) {
							ArrayList<String> warnings = new ArrayList<String>();
							for (Roster roster : rosters) {
								for (Student student : roster.getStudents()) {
									if (!studentHasScore(scores, student, roster.getPeriod())) {
										// is the student listed in another period? Perhaps they named their file with the wrong period
										for (Roster r : rosters) {
											if (studentHasScore(scores, student, r.getPeriod())) {
												warnings.add("Possible Incorrect Period in File Name for student: " + student);
											}
										}
										scores.add(new Score(student.getLastName(), student.getFirstName(), roster.getPeriod(), 0, true));
										if (includeRunTimes) runTimes.add(new Score(student.getLastName(), student.getFirstName(), roster.getPeriod(), 0, true));
									}
								}
							}
							println(System.lineSeparator() + System.lineSeparator(), summaryOut);
							println(System.lineSeparator() + "-------------------------", summaryOut);
							println("Scores by Period and Name:", summaryOut);
							println("-------------------------", summaryOut);
							printScoresReport(scores, true, DEFAULT_NUM_DECIMAL_PLACES, true, summaryOut);
							println(System.lineSeparator() + "-------------------------", summaryOut);
							println("Scores by Score:", summaryOut);
							println("-------------------------", summaryOut);
							printScoreOrderedBy(scores, new ScoreByScore(), true, true, true, false, DEFAULT_NUM_DECIMAL_PLACES, true, summaryOut);
							println(System.lineSeparator() + "-------------------------", summaryOut);
							println("Scores by Period and Score:", summaryOut);
							println("-------------------------", summaryOut);
							printScoreOrderedBy(scores, new ScoreByPeriodAndScore(), true, false, true, true, DEFAULT_NUM_DECIMAL_PLACES, true, summaryOut);
							
							println("-------------------------", summaryOut);
							println("Scores by Test", summaryOut);
							for (Map.Entry<String, ArrayList<Score>> entry : scoresByTest.entrySet()) {
								println("-------------------------", summaryOut);
								String testName = entry.getKey();
								ArrayList<Score> testScores = entry.getValue();
								println(testName + " Scores by Score", summaryOut);
								println("-------------------------", summaryOut);
								printScoreOrderedBy(testScores, new ScoreByScore(), true, true, true, false, DEFAULT_NUM_DECIMAL_PLACES, true, summaryOut);
								FileUtil.writeString(fullReportsDir.toPath() + File.separator + FileUtil.getNameWithoutExtension(testsFile) + "_" + testName + "_ScoresTabDelimited.txt", getTabDelimitedReport(testScores));
							}
							
							if (includeRunTimes) {
								println("-------------------------", summaryOut);
								println(System.lineSeparator() + System.lineSeparator(), summaryOut);
								println(System.lineSeparator() + "-------------------------", summaryOut);
								println("Run Times by Period and Name:", summaryOut);
								println("-------------------------", summaryOut);
								printScoresReport(runTimes, false, 0, false, summaryOut);
								println(System.lineSeparator() + "-------------------------", summaryOut);
								println("Run Times by Run Time:", summaryOut);
								println("-------------------------", summaryOut);
								printScoreOrderedBy(runTimes, new ScoreByScore(), false, true, false, false, 0, false, summaryOut);
								println(System.lineSeparator() + "-------------------------", summaryOut);
								println("Run Times by Period and Run Time:", summaryOut);
								println("-------------------------", summaryOut);
								printScoreOrderedBy(runTimes, new ScoreByPeriodAndScore(), false, false, false, true, 0, false, summaryOut);
							}
							String summaryStr = new String(summaryContent.toByteArray(), StandardCharsets.UTF_8);
							FileUtil.writeString(fullReportsDir.toPath() + File.separator + FileUtil.getNameWithoutExtension(testsFile) + "Summary.txt", summaryStr);
							FileUtil.writeString(fullReportsDir.toPath() + File.separator + FileUtil.getNameWithoutExtension(testsFile) + "SummaryTabDelimited.txt", getTabDelimitedReport(scores));
							FileUtil.writeString(fullReportsDir.toPath() + File.separator + FileUtil.getNameWithoutExtension(testsFile) + "fullReportsTabDelimited.txt", StringUtil.htmlEncodedString(getTabDelimitedFullReport(scores)));
							if (!errorMessages.isEmpty() || !warnings.isEmpty()) {
								SYS_ERR.println("WARNING! The following potential problems were encountered during testing:");
							}
							for (String w : warnings) {
								println(w, summaryOut);
								SYS_ERR.println(w);
							}
							for (String msg : errorMessages) {
								SYS_ERR.println(msg);
							}
							summaryOut.close();
							if (promptToExit) {
								SYS_OUT.println("Press Enter to exit program");
								SCANNER.nextLine();
							}
							System.exit(0); // necessary if any of the tests had to time out of an infinite loop and thread.stop() is not being used
						} else {
							TestsDef nextTest = testsToRun.remove(0);
							runTestsOnClass(nextClass, nextTest, fullReportsDir, verbose, scores);
						}
					}
				});
			}
		}
		if (testsToRun.size() > 0) {
			SYS_OUT.println();
			TestsDef nextTest = testsToRun.remove(0);
			runTestsOnClass(classes.get(0), nextTest, fullReportsDir, verbose, scores);
		}
	}
	
	private static boolean studentHasScore(ArrayList<Score> scores, Student student, int period) {
		for (Score score : scores) {
			if (score.getFirstName().toLowerCase().replaceAll(" ", "").equals(student.getFirstName().toLowerCase().replaceAll(" ", "")) && score.getLastName().toLowerCase().replaceAll(" ", "").equals(student.getLastName().toLowerCase().replaceAll(" ", "")) && period == score.getPeriod()) {
				return true;
			}
		}
		return false;
	}
	
	private static void runTestsOnClass(Class<?> cls, TestsDef testsDef, File fullReportsDir, boolean verbose, ArrayList<Score> scores) {
		SYS_OUT.println("Testing class: " + cls.getName());
		try {
			testsDef.runTests();
		} catch (Exception error) {
			error.printStackTrace();
		}
	}
	
	private static String getTabDelimitedReport(ArrayList<Score> scores, int decimalPlaces) {
		Collections.sort(scores);
		String str = "Period\tLast\tFirst\tScore";
		for (Score s : scores) {
			str += System.lineSeparator() + s.getPeriod() + "\t" + s.getLastName() + "\t" + s.getFirstName() + "\t" + StringUtil.roundNum(s.getScore(), decimalPlaces) + "\t" + (s.isMissing() ? "missing" : "");
		}
		return str;
	}
	
	private static String getTabDelimitedReport(ArrayList<Score> scores) {
		return getTabDelimitedReport(scores, DEFAULT_NUM_DECIMAL_PLACES);
	}
	
	private static String getTabDelimitedFullReport(ArrayList<Score> scores, int decimalPlaces) {
		Collections.sort(scores);
		String str = "Period\tLast\tFirst\tScore\tfullReport";
		for (Score s : scores) {
			String report = s.getFullReport().replaceAll("\r", "").replaceAll("\t", "" + "    ");
			int orignalLength = report.length();
			if (report.length() > 0) report = ReportCompressor.compress(report);
			//SYS_OUT.println("studentReport for " + s.getFirstName() + " " + s.getLastName() + " is " + report.length() + " characters after compressing from " + orignalLength + " characters");
			String studentReport = System.lineSeparator() + s.getPeriod() +
					"\t" + s.getLastName() + "\t" + s.getFirstName() +
					"\t" + StringUtil.roundNum(s.getScore(), decimalPlaces) +
					"\t" + (s.isMissing() ? "missing" : report
					.replaceAll(" ~ ", "_~_").replaceAll("\n", " ~ "));//.replaceAll("\n", "" + (char)7));
			str += studentReport;
		}
		return str;
	}
	
	private static String getTabDelimitedFullReport(ArrayList<Score> scores) {
		return getTabDelimitedFullReport(scores, DEFAULT_NUM_DECIMAL_PLACES);
	}
	
	private static void printScoresReport(ArrayList<Score> scores, boolean includePercent, int decimalPlaces, boolean includeAverage, PrintStream out) {
		Collections.sort(scores);
		int period = scores.get(0).getPeriod() - 1; // guaranteed not equal
		double total = 0;
		int numScoresInPeriod = 0;
		for (Score s : scores) {
			if (s.getPeriod() != period) {
				if (includeAverage && numScoresInPeriod > 0) {
					double avg = total / numScoresInPeriod;
					Score.printReportString(" ", "Average", avg, "", false, includePercent, decimalPlaces, out);
					println("", out);
				}
				total = 0;
				numScoresInPeriod = 0;
				period = s.getPeriod();
				println("*************************", out);
				println("Period " + period, out);
				println("*************************", out);
			}
			s.printReportString(false, includePercent, decimalPlaces, out);
			println("", out);
			total += s.getScore();
			numScoresInPeriod++;
		}
		if (includeAverage && numScoresInPeriod > 0) {
			double avg = total / numScoresInPeriod;
			Score.printReportString(" ", "Average", avg, "", false, includePercent, decimalPlaces, out);
			println("", out);
		}
	}
	
	private static void printScoreOrderedBy(ArrayList<Score> scores, Comparator<Score> comparator, boolean reversed, boolean includePeriod, boolean includePercent, boolean labelPeriodDividers, int decimalPlaces, boolean includeAverage, PrintStream out) {
		Collections.sort(scores, comparator);
		if (reversed) Collections.reverse(scores);
		int period = scores.get(0).getPeriod() - 1; // guaranteed not equal
		double total = 0;
		int numScoresInPeriod = 0;
		for (Score s : scores) {
			if (labelPeriodDividers && s.getPeriod() != period) {
				if (includeAverage && numScoresInPeriod > 0) {
					double avg = total / numScoresInPeriod;
					Score.printReportString(" ", "Average", avg, "", includePeriod, includePercent, decimalPlaces, out);
					println("", out);
				}
				total = 0;
				numScoresInPeriod = 0;
				period = s.getPeriod();
				println("*************************", out);
				println("Period " + period, out);
				println("*************************", out);
			}
			s.printReportString(includePeriod, includePercent, decimalPlaces, out);
			println("", out);
			total += s.getScore();
			numScoresInPeriod++;
		}
		if (includeAverage && numScoresInPeriod > 0) {
			double avg = total / numScoresInPeriod;
			Score.printReportString(" ", "Average", avg, "", includePeriod, includePercent, decimalPlaces, out);
			println("", out);
		}
	}
	
	private static List<Class<?>> findClasses(File directory, ArrayList<String> ignoredFiles) throws ClassNotFoundException {
	    List<Class<?>> classes = new ArrayList<Class<?>>();
	    if (!directory.exists()) {
	        return classes;
	    }
	    File[] files = directory.listFiles();
	    for (File file : files) {
	        if (file.getName().endsWith(".java")) {
				if (!ignoredFiles.contains(file.getName()))
		            classes.add(Class.forName(file.getName().substring(0, file.getName().length() - 5)));
	        }
	    }
	    return classes;
	}
	
	public static void println(String str, PrintStream out) {
		SYS_OUT.println(str);
		out.println(str);
	}
	
	public static void printf(PrintStream out, String format, Object...args) {
		SYS_OUT.printf(format, args);
		out.printf(format, args);
	}
}

class Roster {
	private int period;
	private ArrayList<Student> students;
	
	public Roster(File f) throws NumberFormatException, FileNotFoundException, IOException {
		students = new ArrayList<>();
		// file names for rosters must end with the period (Assumption: period is single digit)
		String fileName = f.getName();
		if (fileName.lastIndexOf('.') != -1) fileName = fileName.substring(0, fileName.lastIndexOf('.'));
		period = Integer.parseInt(fileName.substring(fileName.length() - 1));
		String[][] table = FileUtil.readTabDelimitedSpreadSheet(f);
		for (int row = 0; row < table.length; row++) {
			// Format should be last	first	id
			String lastName = table[row].length > 0 ? table[row][0].trim() : "";
			String firstName = table[row].length > 1 ? table[row][1].trim() : "";
			int id = table[row].length > 2 ? Integer.parseInt(table[row][2].trim()) : 0;
			students.add(new Student(lastName, firstName, id));
		}
	}
	
	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public ArrayList<Student> getStudents() {
		return students;
	}

	public void setStudents(ArrayList<Student> students) {
		this.students = students;
	}



	@Override
	public String toString() {
		return "Roster [period=" + period + ", students=" + students + "]";
	}
	
	
}

class Student implements Comparable<Student>{
	private String lastName;
	private String firstName;
	private int id;
	
	public Student(String lastName, String firstName, int id) {
		this.lastName = lastName;
		this.firstName = firstName;
		this.id = id;
	}

	@Override
	public int compareTo(Student other) {
		if (lastName.compareTo(other.lastName) != 0) {
			return lastName.compareTo(other.lastName);
		} else if (firstName.compareTo(other.firstName) != 0) {
			return firstName.compareTo(other.firstName);
		} else {
			return id - other.id;
		}
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return firstName + " " + lastName + " " + id;
	}
}

class Score implements Comparable <Score> {
	private String lastName;
	private String firstName;
	private int period;
	private double score;
	private boolean missing;
	private String fullReport;
	
	public Score(String lastName, String firstName, int period, double score, boolean missing) {
		this.lastName = lastName;
		this.firstName = firstName;
		this.period = period;
		this.score = score;
		this.missing = missing;
		this.fullReport = "";
	}
	
	public Score(String lastName, String firstName, int period, double score, boolean missing, String fullReport) {
		this(lastName, firstName, period, score, missing);
		this.fullReport = fullReport;
	}
	
	public String getFullReport() {
		return fullReport;
	}

	public void setFullReport(String fullReport) {
		this.fullReport = fullReport;
	}

	public void printReportString(boolean includePeriod, boolean includePercent, int decimalPlaces, PrintStream out) {
		printReportString("P" + getPeriod(), lastName + ", " + firstName, score, missing ? "missing" : "", includePeriod, includePercent, decimalPlaces, out);
	}
	
	public static void printReportString(String prefix, String name, double value, String comment, boolean includePrefix, boolean includePercent, int decimalPlaces, PrintStream out) throws IllegalArgumentException {
		if (decimalPlaces < 0) throw new IllegalArgumentException(decimalPlaces + " is not a valid number of decimal places. Number of decimal places must be >= 0.");
		if (includePrefix) {
			TestRunner.printf(out, "%-4s", prefix);
		}
		TestRunner.printf(out, "%-25s %5" + "." + decimalPlaces + "f" + (includePercent ? "%%" : "") + " %s", name, value, comment);
	}

	public boolean isMissing() {
		return missing;
	}

	public void setMissing(boolean missing) {
		this.missing = missing;
	}

	@Override
	public String toString() {
		return "Score [lastName=" + lastName + ", firstName=" + firstName
				+ ", period=" + period + ", score=" + score + "]";
	}
	
	public String getLastName() {
		return lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public int getPeriod() {
		return period;
	}

	public double getScore() {
		return score;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public void setScore(double score) {
		this.score = score;
	}

	@Override
	public int compareTo(Score other) {
		if (period != other.getPeriod()) return period - other.getPeriod();
		else if (!lastName.equals(other.getLastName())) return lastName.compareTo(other.lastName);
		return firstName.compareTo(other.getFirstName());
	}

}

class ScoreByScore implements Comparator<Score> {

	@Override
	public int compare(Score a, Score b) {
		if (a.getScore() > b.getScore()) {
			return 1;
		} else if (a.getScore() < b.getScore()) {
			return -1;
		} else {
			if (a.isMissing() && !b.isMissing()) return -1;
			else if (!a.isMissing() && b.isMissing()) return 1;
			return 0;
		}
	}
	
}

class ScoreByPeriodAndScore implements Comparator<Score> {

	@Override
	public int compare(Score a, Score b) {
		if (a.getPeriod() != b.getPeriod()) return a.getPeriod() - b.getPeriod();
		return new ScoreByScore().compare(a, b);
	}
	
}
