package com.tinocs.generalAutoGrader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

/**
 * This class only allows printing from a thread with a given threadID (thread name)
 * and also limits the number of times print methods can be called. This will prevent
 * test cases resulting in infinite loops from spamming the printstream of later test
 * cases and from spamming their own test results.
 * Note that this is not foolproof, as this assumes normal printing with
 * System.out.println, System.out.print, or System.out.printf rather than something
 * like System.out.write, but it is pretty safe to assume that students will not
 * do such a thing unless specifically directed to.
 * @author Ted_McLeod
 *
 */
public class ThreadSafePrintStream extends PrintStream {
	private static final PrintStream SYS_OUT = System.out;
	private static final PrintStream SYS_ERR = System.out;
	private static final boolean DEBUG = false; // print debug messages to SYS_OUT?
	private String threadID;
	private long numPrintCalls;
	private long maxPrintCalls;

	public ThreadSafePrintStream(File file, String csn, String threadID, long maxPrintCalls) throws FileNotFoundException, UnsupportedEncodingException {
		super(file, csn);
		this.threadID = threadID;
		this.numPrintCalls = 0;
		this.maxPrintCalls = maxPrintCalls;
	}

	public ThreadSafePrintStream(File file, String threadID, long maxPrintCalls) throws FileNotFoundException {
		super(file);
		this.threadID = threadID;
		this.numPrintCalls = 0;
		this.maxPrintCalls = maxPrintCalls;
	}

	public ThreadSafePrintStream(OutputStream out, boolean autoFlush, String encoding, String threadID, long maxPrintCalls) throws UnsupportedEncodingException {
		super(out, autoFlush, encoding);
		this.threadID = threadID;
		this.numPrintCalls = 0;
		this.maxPrintCalls = maxPrintCalls;
	}

	public ThreadSafePrintStream(OutputStream out, boolean autoFlush, String threadID, long maxPrintCalls) {
		super(out, autoFlush);
		this.threadID = threadID;
		this.numPrintCalls = 0;
		this.maxPrintCalls = maxPrintCalls;
	}

	public ThreadSafePrintStream(OutputStream out, String threadID, long maxPrintCalls) {
		super(out);
		this.threadID = threadID;
		this.numPrintCalls = 0;
		this.maxPrintCalls = maxPrintCalls;
	}

	public ThreadSafePrintStream(String fileName, String csn, String threadID, long maxPrintCalls) throws FileNotFoundException, UnsupportedEncodingException {
		super(fileName, csn);
		this.threadID = threadID;
		this.numPrintCalls = 0;
		this.maxPrintCalls = maxPrintCalls;
	}

	public ThreadSafePrintStream(String fileName, String threadID, long maxPrintCalls) throws FileNotFoundException {
		super(fileName);
		this.threadID = threadID;
		this.numPrintCalls = 0;
		this.maxPrintCalls = maxPrintCalls;
	}
	
	private boolean requestToPrint() {
		if (numPrintCalls > maxPrintCalls || !Thread.currentThread().getName().equals(threadID)) return false;
		numPrintCalls++;
		if (numPrintCalls == maxPrintCalls + 1) {
			String maxCallWarning = System.lineSeparator() + "...Maximum print calls (" + maxPrintCalls + ") exceeded. No more output will be shown." + System.lineSeparator();
			if (DEBUG) SYS_OUT.print(maxCallWarning);
			try {
				write(maxCallWarning.getBytes());
			} catch (IOException e) {
				e.printStackTrace(SYS_ERR);
			}
			return false;
		}
		return true;
	}

	@Override
	public void print(boolean b) {
		print(String.valueOf(b));
	}

	@Override
	public void print(char c) {
		print(String.valueOf(c));
	}

	@Override
	public void print(char[] s) {
		print(String.valueOf(s));
	}

	@Override
	public void print(double d) {
		print(String.valueOf(d));
	}

	@Override
	public void print(float f) {
		print(String.valueOf(f));
	}

	@Override
	public void print(int i) {
		print(String.valueOf(i));
	}

	@Override
	public void print(long l) {
		print(String.valueOf(l));
	}

	@Override
	public void print(Object obj) {
		print(String.valueOf(obj));
	}

	@Override
	public void print(String s) {
		if (requestToPrint()) {
			if (DEBUG) SYS_OUT.print(numPrintCalls + " calls: " + s);
			try {
				write(s.getBytes());
			} catch (IOException e) {
				setError();
			}
		}
	}

	@Override
	public PrintStream printf(Locale l, String format, Object... args) {
		if (requestToPrint()) {
			if (DEBUG)  {
				ArrayList<Object> ar = new ArrayList<>(Arrays.asList(args));
				ar.add(0, numPrintCalls + " calls: ");
				SYS_OUT.printf(l, "%s" + format, ar.toArray(new Object[ar.size()]));
			}
			return format(l, format, args);
		}
		return this;
	}

	@Override
	public PrintStream printf(String format, Object... args) {
		if (requestToPrint()) {
			if (DEBUG) {
				ArrayList<Object> ar = new ArrayList<>(Arrays.asList(args));
				ar.add(0, numPrintCalls + " calls: ");
				SYS_OUT.printf("%s" + format, ar.toArray(new Object[ar.size()]));
			}
			return format(format, args);
		}
		return this;
	}

	@Override
	public void println() {
		print(System.lineSeparator());
	}

	@Override
	public void println(boolean x) {
		print(x + System.lineSeparator());
	}

	@Override
	public void println(char x) {
		print(x + System.lineSeparator());
	}

	@Override
	public void println(char[] x) {
		print(String.valueOf(x) + System.lineSeparator());
	}

	@Override
	public void println(double x) {
		print(x + System.lineSeparator());
	}

	@Override
	public void println(float x) {
		print(x + System.lineSeparator());
	}

	@Override
	public void println(int x) {
		print(x + System.lineSeparator());
	}

	@Override
	public void println(long x) {
		print(x + System.lineSeparator());
	}

	@Override
	public void println(Object x) {
		print(x + System.lineSeparator());
	}

	@Override
	public void println(String x) {
		print(x + System.lineSeparator());
	}
}
