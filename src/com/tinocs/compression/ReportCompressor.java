package com.tinocs.compression;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

import com.tinocs.generalAutoGrader.FileUtil;

public class ReportCompressor {
	private static final PrintStream SYS_OUT = System.out;
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		String compressedFileName = "compressed4.txt";
		String decompressedFileName = "decompress4.txt";
		String testFileName = "P2_Chen_Eva_UserInput.txt";//"P2_Baqai_Ahmed_UserInput.txt";//"P2_Bobbala_Asritha_UserInput.txt";//"CompressionTestFile.txt";
		String s = FileUtil.read(new File(testFileName));
		Scanner scan = new Scanner(s);
		String strWithLS = "";
		// to prevent false mismatch due to \r\n vs \n
		while (scan.hasNextLine()) {
			strWithLS += scan.nextLine() + "\n";
		}
		scan.close();
		SYS_OUT.println("Length Before: " + s.length());
		String compressed = compress(s).replaceAll(" ~ ", "_~_").replaceAll("\n", " ~ ");
		FileUtil.writeString(compressedFileName, compressed);
		SYS_OUT.println("Length After: " + compressed.length());
		//SYS_OUT.println("Files the same? " + compressed.equals(FileUtil.read(new File("compressedWithCommonWords.txt"))));
		
		String compressedText = FileUtil.read(new File(compressedFileName));
		
		String decompressedText = decompress(compressedText.replaceAll(" ~ ", "\n").replaceAll("_~_", " ~ "));
		FileUtil.writeString(decompressedFileName, decompressedText);
		//String beforeSpace = FileUtil.read(new File("compressedNoSpaceApplied.txt"));
		SYS_OUT.println("Files the same? " + strWithLS.equals(decompressedText));
	}
	
	public static String compress(String str) {
		LinesMap linesMap = new LinesMap(str);
		str = linesMap.removeLines(str);
		return linesMap.toString() + str;
	}
	
	public static String decompress(String str) {
		//SYS_OUT.println("decompressing:\n" + str);
		int endOfSpaceMap = LinesMap.indexOfEndOfMap(str);
		LinesMap linesMap = LinesMap.parse(str.substring(0, endOfSpaceMap + 1));
		String s = str.substring(endOfSpaceMap + 1);
		s = linesMap.insertLines(s);
		return s;
	}
}
